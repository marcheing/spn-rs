use clap::{load_yaml, App};
use spn_rs::actions::parser::parse_command_matches;
use std::io;

fn main() -> io::Result<()> {
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from(yaml).get_matches();
    parse_command_matches(matches)?;
    Ok(())
}
