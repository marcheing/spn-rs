use rand::seq::SliceRandom;

pub struct MIS {
    pub n_vertices: usize,
    pub n_edges: usize,
    pub data: Vec<Vec<bool>>,
}

impl MIS {
    pub fn new(n: usize, alpha: f64, r: f64, p: f64) -> Self {
        let mut rng = &mut rand::thread_rng();
        let vertices_per_clique = (n as f64).powf(alpha) as usize;
        let n_vertices = n * vertices_per_clique;
        let mut bool_matrix: Vec<Vec<bool>> = vec![vec![false; n_vertices]; n_vertices];
        let mut last_vertice = 0;
        let mut n_edges = n * (vertices_per_clique * (vertices_per_clique - 1) / 2);
        for _ in 0..n {
            for j in 0..vertices_per_clique {
                for k in 0..vertices_per_clique {
                    bool_matrix[last_vertice + j][last_vertice + k] = true;
                }
            }
            last_vertice += vertices_per_clique;
        }

        let clique_index_pool: Vec<usize> = (0..n).collect();

        for _ in 0..((r * (n as f64) * (n as f64).ln()) as usize - 1) {
            let selected_cliques: Vec<usize> = clique_index_pool
                .choose_multiple(&mut rng, 2)
                .copied()
                .collect();
            for _ in 0..(p * (n as f64).powf(2f64 * alpha)) as usize {
                let vertices_1: Vec<usize> =
                    (selected_cliques[0]..(selected_cliques[0] + vertices_per_clique)).collect();
                let vertices_2: Vec<usize> =
                    (selected_cliques[1]..(selected_cliques[1] + vertices_per_clique)).collect();
                let choice1 = vertices_1.choose(&mut rng).unwrap();
                let choice2 = vertices_2.choose(&mut rng).unwrap();
                if !bool_matrix[*choice1][*choice2] {
                    bool_matrix[*choice1][*choice2] = true;
                    bool_matrix[*choice2][*choice1] = true;
                    n_edges += 1;
                }
            }
        }

        MIS {
            n_vertices,
            n_edges,
            data: bool_matrix,
        }
    }
}
