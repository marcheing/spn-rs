use crate::spn::SPN;
use crate::utils::vector::powerset;
use crate::variable::Variable;
use itertools::Itertools;
use std::collections::hash_map::{Iter, Keys, Values};
use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::hash::Hash;
use std::io::{self, BufRead};
use std::ops::{Index, IndexMut};

#[derive(Clone, Default, Debug, PartialEq, Eq)]
pub struct Evidence {
    varset: HashMap<usize, Vec<usize>>,
}

impl Evidence {
    pub fn new(varset: HashMap<usize, Vec<usize>>) -> Self {
        let mut new_varset = HashMap::default();
        for (var_id, values) in varset {
            new_varset.insert(var_id, values);
        }
        Evidence { varset: new_varset }
    }

    pub fn all_set(&self) -> bool {
        self.values().all(|values| values.len() == 1)
    }

    pub fn is_empty(&self) -> bool {
        self.values().all(|values| values.is_empty())
    }

    pub fn complete(variables: &[Variable]) -> Self {
        let mut varset = HashMap::default();
        for variable in variables {
            varset.insert(variable.id, (0..variable.n_categories).collect());
        }
        Evidence { varset }
    }

    pub fn univariate_subevidences(&self) -> Vec<Self> {
        let mut subevidences = Vec::default();
        let first_var = self.keys().next().unwrap();
        for value in &self[first_var] {
            let mut proto_evidence = Evidence::default();
            proto_evidence.insert(*first_var, vec![*value]);
            subevidences.push(proto_evidence);
        }
        for var_id in self.keys().skip(1) {
            let mut new_full_list = Vec::default();
            for value in &self[var_id] {
                for evidence in &subevidences {
                    let mut new_evidence = evidence.clone();
                    new_evidence.insert(*var_id, vec![*value]);
                    new_full_list.push(new_evidence);
                }
            }
            subevidences = new_full_list;
        }

        subevidences
    }

    pub fn all_subevidences(&self) -> impl Iterator<Item = Self> + '_ {
        let var_ids = self.keys();
        let values_as_vecs: Vec<Vec<usize>> = self.values().map(|v| v.to_vec()).collect();
        let powersets = values_as_vecs.iter().map(|v| powerset(v));

        powersets.multi_cartesian_product().map(move |power_value| {
            let mut evidence = Evidence::default();
            for (var_id, values) in var_ids.clone().zip(power_value.iter()) {
                evidence.insert(*var_id, values.to_vec());
            }
            evidence
        })
    }

    pub fn insert(&mut self, var_id: usize, values: Vec<usize>) {
        if !values.is_empty() {
            self.varset.insert(var_id, values);
        }
    }

    pub fn insert_set(&mut self, var_id: usize, set: Vec<usize>) {
        self.varset.insert(var_id, set);
    }

    pub fn has(&self, var_id: usize) -> bool {
        self.varset.contains_key(&var_id)
    }

    pub fn get(&self, var_id: usize) -> Option<&Vec<usize>> {
        self.varset.get(&var_id)
    }

    pub fn keys(&self) -> Keys<usize, Vec<usize>> {
        self.varset.keys()
    }

    pub fn values(&self) -> Values<usize, Vec<usize>> {
        self.varset.values()
    }

    pub fn iter(&self) -> Iter<usize, Vec<usize>> {
        self.varset.iter()
    }

    pub fn merge_mut(&mut self, other: &Self) {
        for (var_id, values) in other.iter() {
            if self.has(*var_id) {
                for value in values {
                    if !self[var_id].contains(value) {
                        self[var_id].push(*value);
                    }
                }
            } else {
                self.varset.insert(*var_id, other[var_id].clone());
            }
        }
    }

    pub fn has_subevidence(&self, subevidence: &Self) -> bool {
        for (var_id, values) in subevidence.iter() {
            for value in values {
                if !self[var_id].contains(value) {
                    return false;
                }
            }
        }
        true
    }

    pub fn merge(&self, other: &Evidence) -> Evidence {
        let mut evidence = Evidence::default();
        evidence.merge_mut(self);
        evidence.merge_mut(other);
        evidence
    }
}

pub fn merge_slices<T: Eq + Clone + Hash>(vec1: &[T], vec2: &[T]) -> Vec<T> {
    let mut new_set: Vec<T> = Vec::default();
    for elem in vec1.iter().chain(vec2.iter()) {
        new_set.push(elem.clone());
    }

    new_set.into_iter().collect()
}

impl Index<&usize> for Evidence {
    type Output = Vec<usize>;

    fn index(&self, var_id: &usize) -> &Self::Output {
        self.varset.get(var_id).unwrap()
    }
}

impl IndexMut<&usize> for Evidence {
    fn index_mut<'a>(&'a mut self, var_id: &usize) -> &'a mut Self::Output {
        self.varset.get_mut(var_id).unwrap()
    }
}

impl fmt::Display for Evidence {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut string = "{".to_string();
        let mut var_ids: Vec<&usize> = self.keys().collect();
        var_ids.sort();
        for var_id in var_ids {
            string.push_str(&format![" {:?}: {:?}", var_id, self[var_id]]);
        }
        string.push_str(" }");
        write!(f, "{}", string)
    }
}

pub fn evidences_from_file(spn: &SPN, filepath: &str) -> io::Result<Vec<Evidence>> {
    Ok(io::BufReader::new(File::open(filepath)?)
        .lines()
        .filter(|line| !line.as_ref().unwrap().starts_with("var"))
        .map(|line| {
            let values = line
                .unwrap()
                .split(|c| c == ',' || c == ' ')
                .map(|value_string| value_string.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            let mut evidence_map = HashMap::default();
            for variable_ref in spn.variables.iter() {
                evidence_map.insert(variable_ref.id, vec![values[variable_ref.id]]);
            }
            Evidence::new(evidence_map)
        })
        .collect())
}
