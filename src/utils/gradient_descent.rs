use crate::evidence::Evidence;
use crate::spn::node::Node;
use crate::spn::SPN;
use std::collections::HashMap;
use std::f64;

pub fn simple_gd(
    spn: &mut SPN,
    training_evidences: &[Evidence],
    test_evidences: &[Evidence],
    iterations: usize,
    learning_rate: f64,
) {
    let mut train_ll: f64 = training_evidences
        .iter()
        .map(|train_ev| spn.log_value(train_ev))
        .sum();
    let mut test_ll: f64 = test_evidences
        .iter()
        .map(|test_ev| spn.log_value(test_ev))
        .sum();
    println!["Iteration,train_ll,test_ll"];

    for iteration in 0..iterations {
        println!["{},{},{}", iteration, train_ll, test_ll];
        let mut changes_map: HashMap<usize, Vec<f64>> = HashMap::default();
        for training_evidence in training_evidences.iter() {
            let eval_values = spn.eval(training_evidence);
            let derivatives = spn.derivatives(training_evidence, Some(eval_values.clone()));

            for node_id in spn.topological_order() {
                if let Node::Sum(child_ids, _) = &spn.nodes[node_id] {
                    if !changes_map.contains_key(&node_id) {
                        changes_map.insert(node_id, vec![0f64; child_ids.len()]);
                    }
                    let values_for_weights = changes_map.get_mut(&node_id).unwrap();
                    for (acc_value, child_id) in values_for_weights.iter_mut().zip(child_ids.iter())
                    {
                        *acc_value += derivatives[node_id] * eval_values[*child_id]
                            / eval_values[spn.root_id];
                    }
                }
            }
        }
        for (node_id, values_to_add) in changes_map.iter() {
            if let Node::Sum(_, weights) = &mut spn.nodes[*node_id] {
                for (value_to_add, weight_ref) in values_to_add.iter().zip(weights.iter_mut()) {
                    *weight_ref += learning_rate * value_to_add;
                }
            }
        }
        spn.locally_normalize();
        train_ll = training_evidences
            .iter()
            .map(|train_ev| spn.log_value(train_ev))
            .sum();
        test_ll = test_evidences
            .iter()
            .map(|test_ev| spn.log_value(test_ev))
            .sum();
    }
}

pub fn stochastic_gd(
    spn: &mut SPN,
    training_evidences: &[Evidence],
    test_evidences: &[Evidence],
    iterations: usize,
    learning_rate: f64,
) {
    let mut train_ll: f64 = training_evidences
        .iter()
        .map(|train_ev| spn.log_value(train_ev))
        .sum();
    let mut test_ll: f64 = test_evidences
        .iter()
        .map(|test_ev| spn.log_value(test_ev))
        .sum();
    println!["Iteration,train_ll,test_ll"];

    for iteration in 0..iterations {
        println!["{},{},{}", iteration, train_ll, test_ll];
        for training_evidence in training_evidences.iter() {
            let mut changes_map: HashMap<usize, Vec<f64>> = HashMap::default();
            let eval_values = spn.eval(training_evidence);
            let derivatives = spn.derivatives(training_evidence, Some(eval_values.clone()));

            for node_id in spn.topological_order() {
                if let Node::Sum(child_ids, _) = &spn.nodes[node_id] {
                    if !changes_map.contains_key(&node_id) {
                        changes_map.insert(node_id, vec![0f64; child_ids.len()]);
                    }
                    let values_for_weights = changes_map.get_mut(&node_id).unwrap();
                    for (acc_value, child_id) in values_for_weights.iter_mut().zip(child_ids.iter())
                    {
                        *acc_value += derivatives[node_id] * eval_values[*child_id]
                            / eval_values[spn.root_id];
                    }
                }
            }
            for (node_id, values_to_add) in changes_map.iter() {
                if let Node::Sum(_, weights) = &mut spn.nodes[*node_id] {
                    for (value_to_add, weight_ref) in values_to_add.iter().zip(weights.iter_mut()) {
                        *weight_ref += learning_rate * value_to_add;
                    }
                }
            }
        }
        train_ll = training_evidences
            .iter()
            .map(|train_ev| spn.log_value(train_ev))
            .sum();
        test_ll = test_evidences
            .iter()
            .map(|test_ev| spn.log_value(test_ev))
            .sum();
    }
    spn.locally_normalize();
}

pub fn projected_gd(
    spn: &mut SPN,
    training_evidences: &[Evidence],
    test_evidences: &[Evidence],
    iterations: usize,
    proj_eps: f64,
    stop_epsilon: f64,
    mut learning_rate: f64,
    shrink_weight: f64,
    prior_scale: f64,
    map_prior: bool,
) {
    let mut optimal_valid_logp = f64::NEG_INFINITY;
    let mut train_logps: f64;
    let mut valid_logps: f64;
    let num_trains = training_evidences.len();
    let num_valids = test_evidences.len();
    let mut sufficient_stats: HashMap<usize, Vec<f64>> = HashMap::default();
    let mut optimization: HashMap<usize, Vec<f64>> = HashMap::default();
    let mut alphas: HashMap<usize, Vec<f64>> = HashMap::default();
    let mut train_funcs = vec![];
    let mut valid_funcs = vec![];
    let all_marginalized = spn.all_marginalized();

    for node_id in spn.topological_order() {
        if let Node::Sum(child_ids, weights) = &spn.nodes[node_id] {
            sufficient_stats.insert(node_id, vec![0f64; child_ids.len()]);
            optimization.insert(node_id, vec![0f64; child_ids.len()]);
            let mut prior_weights = weights.clone();
            for weight in prior_weights.iter_mut() {
                *weight *= prior_scale;
            }
            alphas.insert(node_id, prior_weights);
        }
    }

    println!["#iteration,train-lld,valid-lld"];

    for iteration in 0..iterations {
        if iteration > 1 && train_funcs[iteration - 1] < train_funcs[iteration - 2] {
            learning_rate *= shrink_weight;
        }

        train_logps = 0f64;
        valid_logps = 0f64;
        for (_, stats) in sufficient_stats.iter_mut() {
            for stat in stats.iter_mut() {
                *stat = 0f64;
            }
        }

        for training_evidence in training_evidences.iter() {
            let eval_values = spn.eval(training_evidence);
            let root_value = eval_values[spn.root_id];
            train_logps += root_value.ln();
            let derivatives = spn.derivatives(training_evidence, Some(eval_values.clone()));
            for node_id in spn.topological_order() {
                if let Node::Sum(child_ids, _) = &spn.nodes[node_id] {
                    for (child_index, child_id) in child_ids.iter().enumerate() {
                        let value_to_add =
                            derivatives[node_id] * eval_values[*child_id] / root_value;
                        sufficient_stats.get_mut(&node_id).unwrap()[child_index] += value_to_add;
                    }
                }
            }
        }

        for test_evidence in test_evidences.iter() {
            valid_logps += spn.log_value(test_evidence);
        }

        let eval_values = spn.eval(&all_marginalized);
        let root_value = eval_values[spn.root_id];
        let derivatives = spn.derivatives(&all_marginalized, Some(eval_values.clone()));
        for node_id in spn.topological_order() {
            if let Node::Sum(child_ids, _) = &spn.nodes[node_id] {
                for (child_index, child_id) in child_ids.iter().enumerate() {
                    let value_to_subtract =
                        derivatives[node_id] * eval_values[*child_id] / root_value;
                    sufficient_stats.get_mut(&node_id).unwrap()[child_index] -=
                        training_evidences.len() as f64 * value_to_subtract;
                }
            }
        }

        train_logps -= (num_trains as f64) * root_value.ln();
        valid_logps -= (num_valids as f64) * root_value.ln();
        train_logps /= num_trains as f64;
        valid_logps /= num_valids as f64;

        train_funcs.push(train_logps);
        valid_funcs.push(valid_logps);

        if valid_logps > optimal_valid_logp {
            optimal_valid_logp = valid_logps;
            for node_id in spn.topological_order() {
                if let Node::Sum(_, weights) = &spn.nodes[node_id] {
                    for (child_index, weight) in weights.iter().enumerate() {
                        optimization.get_mut(&node_id).unwrap()[child_index] = *weight;
                    }
                }
            }
        }

        println![
            "{},{},{}",
            iteration, train_funcs[iteration], valid_funcs[iteration]
        ];

        for node_id in spn.topological_order() {
            if let Node::Sum(child_ids, weights) = &mut spn.nodes[node_id] {
                for (child_index, (_, weight)) in
                    child_ids.iter().zip(weights.iter_mut()).enumerate()
                {
                    if map_prior {
                        sufficient_stats.get_mut(&node_id).unwrap()[child_index] +=
                            (alphas[&node_id][child_index] - 1f64) / *weight;
                    }
                    sufficient_stats.get_mut(&node_id).unwrap()[child_index] /= num_trains as f64;
                    let possible_new_weight =
                        *weight + learning_rate * sufficient_stats[&node_id][child_index];
                    if possible_new_weight > 0f64 {
                        *weight = possible_new_weight;
                    } else {
                        *weight = proj_eps;
                    }
                }
            }
        }

        if iteration > 1 && train_funcs[iteration] - train_funcs[iteration - 1] < stop_epsilon {
            break;
        }
    }

    for node_id in spn.topological_order() {
        if let Node::Sum(_, weights) = &mut spn.nodes[node_id] {
            for (child_index, weight) in weights.iter_mut().enumerate() {
                *weight = optimization[&node_id][child_index];
            }
        }
    }

    spn.weight_projection(1e-3);
}
