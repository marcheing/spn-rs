use crate::spn::SPN;
use crate::utils::anytime::AnytimeEnv;

pub fn curry_usize_arg<F>(arg: usize, function: F) -> impl FnOnce(&SPN, AnytimeEnv)
where
    F: FnOnce(&SPN, usize, AnytimeEnv),
{
    move |spn: &SPN, anytime_env: AnytimeEnv| function(spn, arg, anytime_env)
}
