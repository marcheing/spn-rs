use crate::evidence::Evidence;
use crate::spn::node::Node;
use crate::spn::SPN;
use std::collections::HashMap;
use std::f64;

pub fn do_em(
    spn: &mut SPN,
    training_evidences: &[Evidence],
    test_evidences: &[Evidence],
    iterations: usize,
    stop_epsilon: f64,
) {
    let num_trains = training_evidences.len();
    let num_valids = test_evidences.len();
    let mut optimal_valid_logp = f64::NEG_INFINITY;
    let mut train_logps: f64;
    let mut valid_logps: f64;
    let mut train_funcs = vec![];
    let mut valid_funcs = vec![];
    let mut ssz: f64;
    let epsilon = f64::EPSILON;

    let mut sufficient_stats: HashMap<usize, Vec<f64>> = HashMap::default();
    let mut optimization: HashMap<usize, Vec<f64>> = HashMap::default();
    let sum_nodes = spn.sum_nodes();
    let sum_node_ids: Vec<usize> = sum_nodes.iter().map(|(node_id, (_, _))| *node_id).collect();

    for (node_id, (children, _)) in sum_nodes.iter() {
        sufficient_stats.insert(*node_id, vec![0f64; children.len()]);
        optimization.insert(*node_id, vec![0f64; children.len()]);
    }

    println!["#iteration,train-lld,valid-lld"];

    for iteration in 0..iterations {
        train_logps = 0f64;

        for (_, stats) in sufficient_stats.iter_mut() {
            *stats = vec![0f64; stats.len()];
        }

        for training_evidence in training_evidences.iter() {
            let eval_values = spn.eval(training_evidence);
            let root_value = eval_values[spn.root_id];
            train_logps += root_value.ln();
            let derivatives = spn.derivatives(training_evidence, Some(eval_values.clone()));
            for (node_id, (child_ids, weights)) in sum_nodes.iter() {
                for (child_index, (child_id, weight)) in
                    child_ids.iter().zip(weights.iter()).enumerate()
                {
                    sufficient_stats.get_mut(&node_id).unwrap()[child_index] +=
                        weight * (derivatives[*node_id] * eval_values[*child_id] / root_value);
                }
            }
        }

        valid_logps = test_evidences
            .iter()
            .map(|test_evidence| spn.log_value(test_evidence))
            .sum();

        train_logps /= num_trains as f64;
        valid_logps /= num_valids as f64;

        train_funcs.push(train_logps);
        valid_funcs.push(valid_logps);

        if valid_logps > optimal_valid_logp {
            optimal_valid_logp = valid_logps;
            for (node_id, (_, weights)) in sum_nodes.iter() {
                for (child_index, weight) in weights.iter().enumerate() {
                    optimization.get_mut(&node_id).unwrap()[child_index] = *weight;
                }
            }
        }
        println![
            "{},{},{}",
            iteration, train_funcs[iteration], valid_funcs[iteration]
        ];

        for node_id in sum_node_ids.iter() {
            if let Node::Sum(child_ids, weights) = &mut spn.nodes[*node_id] {
                ssz = 0f64;
                for child_index in 0..child_ids.len() {
                    ssz += sufficient_stats[&node_id][child_index] + epsilon;
                }
                for (child_index, weight) in weights.iter_mut().enumerate() {
                    *weight = (sufficient_stats[&node_id][child_index] + epsilon) / ssz;
                }
                //assert![weights.iter().sum::<f64>() - 1f64 < epsilon];
            }
        }
        if iteration > 0 && train_funcs[iteration] - train_funcs[iteration - 1] < stop_epsilon {
            break;
        }
    }
    for node_id in sum_node_ids {
        if let Node::Sum(_, weights) = &mut spn.nodes[node_id] {
            for child_index in 0..weights.len() {
                weights[child_index] = optimization[&node_id][child_index];
            }
        }
    }
    spn.normalize();
}
