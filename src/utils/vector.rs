use itertools::Itertools;
use rand::Rng;

pub fn vector_from_indices<T: Clone>(vector: &[T], indices: &[usize]) -> Vec<T> {
    indices.iter().map(|index| vector[*index].clone()).collect()
}

pub fn vector_difference(vector1: &[f64], vector2: &[f64]) -> Vec<f64> {
    vector1
        .iter()
        .zip(vector2.iter())
        .map(|(v1, v2)| v1 - v2)
        .collect()
}

pub fn scalar_vec_product(scalar: f64, vec: &[f64]) -> Vec<f64> {
    vec.iter().map(|x| x * scalar).collect()
}

pub fn dot(vector1: &[f64], vector2: &[f64]) -> f64 {
    vector1
        .iter()
        .zip(vector2.iter())
        .map(|(v1, v2)| *v1 * *v2)
        .sum()
}

pub fn powerset<T: Clone>(vector: &[T]) -> Vec<Vec<T>> {
    (0..=vector.len())
        .map(|i| vector.iter().cloned().combinations(i))
        .flatten()
        .collect()
}

pub fn vector_product<T: Clone>(vector: Vec<Vec<T>>) -> Vec<Vec<T>> {
    let mut result = vec![vec![]];
    for list in vector {
        let mut new_result = Vec::default();
        for element in list.iter() {
            for mut current_results in result.clone() {
                current_results.push(element.clone());
                new_result.push(current_results);
            }
        }
        result = new_result;
    }
    result
}

pub fn remove_item<T: Clone + Eq>(vector: &mut Vec<T>, item: T) {
    if let Some(position) = vector.iter().position(|x| *x == item) {
        vector.remove(position);
    }
}

pub fn hamming(p1: &[usize], p2: &[usize]) -> usize {
    p1.iter()
        .zip(p2.iter())
        .filter(|(p1_elem, p2_elem)| *p1_elem != *p2_elem)
        .count()
}

pub fn sample<T: Clone, R: Rng + ?Sized>(data: &[T], rng: &mut R) -> Vec<T> {
    let data_size = data.len();
    (0..data_size)
        .map(|_| {
            let random_index = rng.gen_range(0, data_size);
            data[random_index].clone()
        })
        .collect()
}
