pub mod algorithm;
pub mod anytime;
pub mod em;
pub mod f64;
pub mod functional;
pub mod gradient_descent;
pub mod random;
pub mod vector;
