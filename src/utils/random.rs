use rand::rngs::StdRng;
use rand::SeedableRng;

pub fn get_rng(matches: &clap::ArgMatches) -> StdRng {
    SeedableRng::seed_from_u64(match matches.value_of("seed") {
        Some(seed_string) => seed_string.parse::<u64>().unwrap(),
        None => 1337,
    })
}
