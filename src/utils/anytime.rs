use crate::evidence::Evidence;
use crate::spn::SPN;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::time::{Duration, Instant};

// I want AnytimeResult to hold the best evidence found, the lower bound, and the upper bound, in
// this order
pub type AnytimeResult = (Evidence, f64, f64);

pub struct AnytimeEnv {
    sender: Sender<AnytimeResult>,
    receiver: Receiver<()>,
    finished: bool,
}

impl AnytimeEnv {
    pub fn new(sender: Sender<AnytimeResult>, receiver: Receiver<()>) -> Self {
        AnytimeEnv {
            sender,
            receiver,
            finished: false,
        }
    }

    pub fn update_best_result(&self, anytime_result: AnytimeResult) {
        if let Err(e) = self.sender.send(anytime_result) {
            println!("{:?}", e);
        }
    }

    pub fn check_timeout(&mut self) -> bool {
        if self.finished {
            true
        } else if let Ok(()) = self.receiver.try_recv() {
            self.finished = true;
            true
        } else {
            false
        }
    }
}

pub fn run<F: FnOnce(&SPN, AnytimeEnv) + Send + 'static>(
    spn: SPN,
    time: Duration,
    alg: F,
) -> (thread::JoinHandle<Duration>, Receiver<AnytimeResult>) {
    let (sender_result, receiver_result) = channel();
    let (sender_timeout, receiver_timeout) = channel();
    let anytime_env = AnytimeEnv::new(sender_result, receiver_timeout);
    let algorithm_handle = thread::spawn(move || {
        let now = Instant::now();
        alg(&spn, anytime_env);
        now.elapsed()
    });
    thread::spawn(move || {
        thread::sleep(time);
        sender_timeout.send(()).unwrap();
    });
    (algorithm_handle, receiver_result)
}

pub fn run_and_check_time<F: FnOnce(&SPN) -> Evidence>(spn: &SPN, alg: F) -> (Duration, Evidence) {
    let now = Instant::now();
    let evidence = alg(spn);
    (now.elapsed(), evidence)
}
