use crate::utils::vector::hamming;
use rand::Rng;
use std::collections::HashMap;

#[derive(PartialEq)]
pub struct Cluster {
    pub all_points: Vec<usize>,
    pub mean_index: usize, // Should this be an index to the all_points field?
}

impl Cluster {
    pub fn insert(&mut self, data: &[Vec<usize>], new_data_index: usize) {
        self.all_points.push(new_data_index);
        let new_data_instance = &data[new_data_index];
        let mean_data_instance = &data[self.mean_index];
        let mut distance_from_mean_to_all_points = 0;
        let mut distance_from_new_index_to_all_points = 0;
        for data_index in self.all_points.iter() {
            let data_instance = &data[*data_index];
            distance_from_mean_to_all_points += hamming(mean_data_instance, data_instance);
            distance_from_new_index_to_all_points += hamming(new_data_instance, data_instance);
        }
        if distance_from_new_index_to_all_points < distance_from_mean_to_all_points {
            self.mean_index = new_data_index;
        }
    }

    pub fn remove(&mut self, data: &[Vec<usize>], index_to_remove: usize) {
        // TODO: Should assert that the index exists
        self.all_points.remove(
            self.all_points
                .iter()
                .position(|x| *x == index_to_remove)
                .unwrap(),
        );
        if self.mean_index == index_to_remove {
            let mut best = std::usize::MAX;
            for data_index_1 in self.all_points.iter() {
                let data_instance_1 = &data[*data_index_1];
                let distance_to_all_other_points = self
                    .all_points
                    .iter()
                    .map(|data_index_2| hamming(data_instance_1, &data[*data_index_2]))
                    .sum();
                if distance_to_all_other_points < best {
                    best = distance_to_all_other_points;
                    self.mean_index = *data_index_1;
                }
            }
        }
    }
}

pub fn kmedoid<R: Rng + ?Sized>(
    n_clusters: usize,
    data: &[Vec<usize>],
    rng: &mut R,
) -> Vec<Cluster> {
    let n = data.len();
    let mut clusters: Vec<Cluster> = vec![];
    let mut data_index_to_cluster_index = HashMap::<usize, usize>::default();

    let mut mean_points_to_choose: Vec<usize> = (0..n).collect();
    let mut mean_points_chosen = vec![];

    for _ in 0..n_clusters {
        let mut r: usize = 0;
        let mut ok = true;
        while ok && mean_points_chosen.len() < n {
            r = rng.gen_range(0, mean_points_to_choose.len());
            mean_points_to_choose.remove(r);
            ok = false;
            mean_points_chosen.push(r);
            let mean_data_instance = &data[r];
            for cluster_ref in clusters.iter() {
                if ok {
                    break;
                }
                let mean_cluster_data_instance = &data[cluster_ref.mean_index];
                let mean_cluster_data_instance_is_mean_data_instance = mean_data_instance
                    .iter()
                    .zip(mean_cluster_data_instance)
                    .all(|(data_point, mean_cluster_data_point)| {
                        data_point == mean_cluster_data_point
                    });
                if mean_cluster_data_instance_is_mean_data_instance {
                    ok = true;
                }
            }
        }
        if ok {
            break;
        }
        let mut cluster = Cluster {
            all_points: vec![],
            mean_index: r,
        };
        data_index_to_cluster_index.insert(r, clusters.len());
        cluster.insert(data, r);
        clusters.push(cluster);
    }

    println!["Starting K-means until convergence..."];

    let mut nochange = 0;
    for i in (0..n).cycle() {
        let mut min = std::usize::MAX;
        let mut which = None;

        if let Some(cluster_index_ref) = data_index_to_cluster_index.get(&i) {
            which = Some(*cluster_index_ref);
            min = hamming(&data[clusters[*cluster_index_ref].mean_index], &data[i]);
        }
        for (cluster_index, cluster_ref) in clusters.iter().enumerate() {
            if which.is_none() || cluster_index != which.unwrap() {
                let t = hamming(&data[cluster_ref.mean_index], &data[i]);
                if t < min {
                    min = t;
                    which = Some(cluster_index);
                }
            }
        }
        let which_cluster_index = which.unwrap();
        match data_index_to_cluster_index.get_mut(&i) {
            Some(cluster_index) => {
                if *cluster_index != which_cluster_index {
                    clusters[*cluster_index].remove(data, i);
                    data_index_to_cluster_index.insert(i, which_cluster_index);
                    clusters[which_cluster_index].insert(data, i);
                    nochange = 0;
                } else {
                    nochange += 1;
                }
            }
            None => {
                data_index_to_cluster_index.insert(i, which_cluster_index);
                clusters[which_cluster_index].insert(data, i);
                nochange = 0;
            }
        }
        if nochange >= n {
            break;
        }
    }

    clusters
}
