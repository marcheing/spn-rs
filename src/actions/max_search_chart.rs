use crate::map::heuristics::*;
use crate::map::map_scenario::MapScenario;
use crate::map::max_search::*;

pub fn max_search_chart(
    map_scenarios: Vec<MapScenario>,
    time_limit_opt: Option<u64>,
    heuristic: &str,
    order: &str,
) -> std::io::Result<()> {
    let map_scenario = map_scenarios.first().unwrap();
    let checking_heuristic: &CheckingFunction = match heuristic {
        "forward_checking" => &forward_checking,
        _ => &marginal_checking,
    };
    let order_heuristic: &BranchingHeuristic = match order {
        "first_variable" => &first_variable,
        "lowest_marginal" => &lowest_marginal,
        "largest_marginal" => &largest_marginal,
        "lowest_entropy" => &lowest_entropy,
        "largest_entropy" => &largest_entropy,
        _ => &ordering,
    };
    let anytime_info = max_search_for_chart(
        &map_scenario,
        checking_heuristic,
        time_limit_opt,
        order_heuristic,
    );
    for (val, val_ln, time) in anytime_info.chart_order() {
        println!["{},{},{}", val, val_ln, time.as_millis()];
    }
    println!["---"];
    Ok(())
}
