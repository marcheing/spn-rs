use crate::evidence::evidences_from_file;
use crate::spn::io::file::from_file;
use crate::spn::SPN;
use std::io;
use std::path::Path;

pub fn ll_wrapper(ll_params: &clap::ArgMatches) -> io::Result<()> {
    let spn_filepath = ll_params.value_of("spn_filepath").unwrap();
    let data_filepath = ll_params.value_of("data_filepath").unwrap();
    log_likelihood(&spn_filepath, &data_filepath)?;
    Ok(())
}

fn ll_from_file(spn: &SPN, data_filepath: &str) -> io::Result<f64> {
    Ok(evidences_from_file(&spn, data_filepath)?
        .iter()
        .map(|evidence| spn.log_value(evidence))
        .sum())
}

fn log_likelihood(spn_filepath: &str, data_filepath: &str) -> io::Result<()> {
    let spn = from_file(Path::new(spn_filepath))?;
    println!["Total LL: {:?}", ll_from_file(&spn, data_filepath)];
    Ok(())
}
