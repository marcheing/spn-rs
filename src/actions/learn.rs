use crate::classification::classification_test;
use crate::data::datafile::load_data;
use crate::evidence::Evidence;
use crate::learn::gens;
use crate::spn::io::file::to_file;
use crate::spn::SPN;
use rand::rngs::StdRng;
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::{self, BufRead};
use std::path::Path;

pub enum ParamCheckingMode {
    LL,
    Classification,
}

pub enum ParamComparisonType {
    Float(f64),
    Int(usize),
}

impl ParamComparisonType {
    pub fn larger(&self, other: &Self) -> bool {
        match self {
            ParamComparisonType::Float(f1) => match other {
                ParamComparisonType::Float(f2) => f1 > f2,
                ParamComparisonType::Int(_) => false,
            },
            ParamComparisonType::Int(i1) => match other {
                ParamComparisonType::Float(_) => false,
                ParamComparisonType::Int(i2) => i1 > i2,
            },
        }
    }
}

pub fn learn(
    dataset_path: &str,
    spn_path: &str,
    kclusters: usize,
    pval: f64,
    rng: &mut StdRng,
) -> io::Result<()> {
    let (data, variables) = load_data(dataset_path)?;
    let spn = SPN::minimize(gens(&variables, &data, kclusters, pval, rng, None, 0));
    to_file(Path::new(spn_path), &spn)?;
    Ok(())
}

pub fn learn_params(
    dataset_path: &str,
    spn_path: &str,
    test_path: &str,
    kclusters: &[usize],
    pvals: &[f64],
    rng: &mut StdRng,
    param_checking_mode: ParamCheckingMode,
) -> io::Result<()> {
    let (data, variables) = load_data(dataset_path)?;
    let mut best_kcluster = 0;
    let mut best_pval = 0.0;
    let mut best_spn = None;
    let mut best_value = None;
    let test_file = File::open(test_path)?;
    let test_data_instances: Vec<Vec<usize>> = io::BufReader::new(test_file)
        .lines()
        .map(|line| {
            let line_string = line.unwrap();
            if line_string.starts_with("var") {
                None
            } else {
                Some(
                    line_string
                        .split(|c| c == ',' || c == ' ')
                        .map(|value_string| value_string.parse::<usize>().unwrap())
                        .collect::<Vec<usize>>(),
                )
            }
        })
        .filter(|line| line.is_some())
        .map(|line_opt| line_opt.unwrap())
        .collect();
    let test_evidences = test_data_instances
        .iter()
        .map(|values| {
            let mut evidence_map = HashMap::default();
            for variable_ref in variables.iter() {
                evidence_map.insert(variable_ref.id, vec![values[variable_ref.id]]);
            }
            Evidence::new(evidence_map)
        })
        .collect::<Vec<Evidence>>();

    for kcluster in kclusters {
        for pval in pvals {
            let spn = SPN::minimize(gens(&variables, &data, *kcluster, *pval, rng, None, 0));
            let comparison = match param_checking_mode {
                ParamCheckingMode::LL => ParamComparisonType::Float({
                    let sum_value = test_evidences
                        .iter()
                        .map(|evidence| spn.log_value(evidence))
                        .sum::<f64>();
                    if sum_value.is_nan() {
                        panic!["Param checking ended in nan"];
                    }
                    sum_value
                }),
                ParamCheckingMode::Classification => {
                    ParamComparisonType::Int(classification_test(&spn, &test_data_instances))
                }
            };
            if best_value.is_none() || comparison.larger(best_value.as_ref().unwrap()) {
                best_value = Some(comparison);
                best_spn = Some(spn);
                best_kcluster = *kcluster;
                best_pval = *pval;
            }
        }
    }
    println!["Best Kclusters: {}", best_kcluster];
    println!["Best pval: {}", best_pval];
    to_file(Path::new(spn_path), &best_spn.unwrap())?;
    Ok(())
}

pub fn learn_spn_on_path(path: &str, rng: &mut StdRng) -> io::Result<()> {
    for file_in_dataset_folder in fs::read_dir(path)? {
        let new_path = file_in_dataset_folder.unwrap().path();
        let path_string = new_path.to_str().unwrap();
        if path_string.ends_with(".train.data") {
            let filepath_string = new_path.file_stem().unwrap().to_str().unwrap();
            let filepath_string_splitted = filepath_string.split('.').collect::<Vec<&str>>();
            let spn_name = filepath_string_splitted.first().unwrap();
            let spn_path_string = format!["{}/{}.spn", path, spn_name];
            let spn_filepath = Path::new(&spn_path_string);
            if spn_filepath.exists() {
                continue;
            }
            println!["Seeing {}", spn_name];
            let (data, variables) = load_data(path_string)?;
            let spn = SPN::minimize(gens(&variables, &data, 3, 0.5, rng, None, 0));
            to_file(spn_filepath, &spn)?;
        }
    }
    Ok(())
}
