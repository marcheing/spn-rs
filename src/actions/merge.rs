use crate::spn::io::file::{from_file, to_file};
use crate::spn::node::Node;
use crate::spn::SPN;
use std::io;
use std::path::Path;

pub fn merge_spns(spns_to_merge_paths: &[String], final_spn_filepath: &str) -> io::Result<()> {
    let mut spns = vec![];
    for spn_to_merge_path in spns_to_merge_paths {
        spns.push(from_file(Path::new(spn_to_merge_path))?);
    }
    let variables = spns.first().unwrap().variables.clone();
    let n_spns = spns.len();
    let uniform_weight = 1f64 / (n_spns as f64);
    let mut main_spn_nodes = vec![];
    let mut subspn_roots = vec![];
    let mut last_index = 0;
    let mut total_number_of_nodes = 0;
    for spn in spns {
        total_number_of_nodes += spn.nodes.len();
        for node in spn.nodes {
            match node {
                Node::Indicator(var_id, assignment) => {
                    main_spn_nodes.push(Node::Indicator(var_id, assignment))
                }
                Node::Sum(children, weights) => {
                    let new_children = children
                        .into_iter()
                        .map(|child_id| child_id + last_index)
                        .collect();
                    main_spn_nodes.push(Node::Sum(new_children, weights))
                }
                Node::Product(children) => {
                    let new_children = children
                        .into_iter()
                        .map(|child_id| child_id + last_index)
                        .collect();
                    main_spn_nodes.push(Node::Product(new_children))
                }
                _ => {}
            };
        }
        subspn_roots.push(spn.root_id + last_index);
        last_index = main_spn_nodes.len();
    }
    let root_node = Node::Sum(subspn_roots, vec![uniform_weight; n_spns]);
    total_number_of_nodes += 1;
    let root_id = main_spn_nodes.len();
    main_spn_nodes.push(root_node);
    let spn = SPN::new(main_spn_nodes, variables, root_id);
    assert_eq![total_number_of_nodes, spn.nodes.len()];
    to_file(Path::new(final_spn_filepath), &SPN::minimize(spn))?;

    Ok(())
}
