use crate::actions::bag::bag_wrapper;
use crate::actions::best_limited::best_limited;
use crate::actions::classification::classification;
use crate::actions::create_scenarios::*;
use crate::actions::em::em;
use crate::actions::gd::gd;
use crate::actions::learn::*;
use crate::actions::ll::ll_wrapper;
use crate::actions::map::map;
use crate::actions::max_search_chart::max_search_chart;
use crate::actions::max_table::max_table_action;
use crate::actions::merge::merge_spns;
use crate::actions::plain_autocomplete::plain_autocomplete_action;
use crate::actions::table::table_action;
use crate::actions::time_measure::time_measure;
use crate::actions::value::value;
use crate::map::map_scenario::map_scenarios_from_file;
use crate::spn::io::file::from_file;
use rand::rngs::StdRng;
use rand::SeedableRng;
use std::io;
use std::path::Path;
use std::sync::Arc;

fn get_rng(matches: &clap::ArgMatches) -> StdRng {
    SeedableRng::seed_from_u64(match matches.value_of("seed") {
        Some(seed_string) => seed_string.parse::<u64>().unwrap(),
        None => 1337,
    })
}

pub fn parse_command_matches(matches: clap::ArgMatches) -> io::Result<()> {
    match matches.subcommand() {
        ("learn-path", Some(learn_path_matches)) => {
            learn_path_wrapper(learn_path_matches)?;
        }
        ("bag", Some(bag_params)) => {
            bag_wrapper(bag_params)?;
        }
        ("ll", Some(ll_params)) => {
            ll_wrapper(ll_params)?;
        }
        ("learn", Some(learn_params)) => {
            let data_filepath = learn_params.value_of("data_filepath").unwrap();
            let spn_filepath = learn_params.value_of("spn_filepath").unwrap();
            let kclusters = learn_params
                .value_of("kclusters")
                .unwrap()
                .parse::<usize>()
                .unwrap();
            let pval = learn_params
                .value_of("pval")
                .unwrap()
                .parse::<f64>()
                .unwrap();
            let mut rng = get_rng(learn_params);
            learn(&data_filepath, &spn_filepath, kclusters, pval, &mut rng)?;
        }
        ("learn-params", Some(params)) => {
            let data_filepath = params.value_of("data_filepath").unwrap();
            let spn_filepath = params.value_of("spn_filepath").unwrap();
            let test_filepath = params.value_of("test_filepath").unwrap();
            let kclusters = params
                .values_of("kclusters")
                .unwrap()
                .map(|k| k.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            let pval = params
                .values_of("pval")
                .unwrap()
                .map(|p| p.parse::<f64>().unwrap())
                .collect::<Vec<f64>>();
            let mut rng = get_rng(params);
            let comparison_check = match params.value_of("checking_mode").unwrap() {
                "acc" | "accuracy" | "classification" | "class" => {
                    ParamCheckingMode::Classification
                }

                _ => ParamCheckingMode::LL,
            };
            learn_params(
                &data_filepath,
                &spn_filepath,
                &test_filepath,
                &kclusters,
                &pval,
                &mut rng,
                comparison_check,
            )?;
        }
        ("merge", Some(merge_params)) => {
            let spn_filepaths = merge_params
                .values_of("spn_filepaths")
                .unwrap()
                .map(String::from)
                .collect::<Vec<String>>();
            let final_spn_filepath = merge_params.value_of("final_spn_filepath").unwrap();
            merge_spns(&spn_filepaths, &final_spn_filepath)?;
        }
        ("max_search_chart", Some(max_search_chart_params)) => {
            let spn = Arc::new(from_file(Path::new(
                max_search_chart_params.value_of("spn").unwrap(),
            ))?);
            let map_scenarios = map_scenarios_from_file(
                max_search_chart_params.value_of("map_scenarios").unwrap(),
                spn,
            )?;
            let time_limit_opt = max_search_chart_params
                .value_of("time_limit")
                .map(|time_limit_str| time_limit_str.parse::<u64>().unwrap());
            let heuristic = max_search_chart_params
                .value_of("heuristc")
                .unwrap_or("forward_checking");
            let order = max_search_chart_params
                .value_of("order")
                .unwrap_or("ordering");
            max_search_chart(map_scenarios, time_limit_opt, heuristic, order)?;
        }
        ("table", Some(table_params)) => {
            let spn = Arc::new(from_file(Path::new(table_params.value_of("spn").unwrap()))?);
            let map_scenarios =
                map_scenarios_from_file(table_params.value_of("map_scenarios").unwrap(), spn)?;
            let evidences_filename = table_params.value_of("evidences");
            let times_filename = table_params.value_of("times");
            let count = match table_params.value_of("count") {
                Some(count_as_str) => count_as_str.parse::<usize>().unwrap_or(1),
                None => 1,
            };
            let time_limit = match table_params.value_of("time_limit") {
                Some(time_limit_as_str) => time_limit_as_str.parse::<u64>().unwrap_or(600),
                None => 600,
            };
            table_action(
                map_scenarios,
                evidences_filename,
                times_filename,
                count,
                time_limit,
            );
        }
        ("max_table", Some(table_params)) => {
            let spn = Arc::new(from_file(Path::new(table_params.value_of("spn").unwrap()))?);
            let map_scenarios =
                map_scenarios_from_file(table_params.value_of("map_scenarios").unwrap(), spn)?;
            let evidences_filename = table_params.value_of("evidences");
            let times_filename = table_params.value_of("times");
            let time_limit = match table_params.value_of("time_limit") {
                Some(time_limit_as_str) => time_limit_as_str.parse::<u64>().unwrap_or(600),
                None => 600,
            };
            max_table_action(
                map_scenarios,
                evidences_filename,
                times_filename,
                time_limit,
            );
        }
        ("plain_autocomplete", Some(plain_autocomplete_params)) => {
            let spn = Arc::new(from_file(Path::new(
                plain_autocomplete_params.value_of("spn").unwrap(),
            ))?);
            let map_scenarios = map_scenarios_from_file(
                plain_autocomplete_params.value_of("map_scenarios").unwrap(),
                spn,
            )?;
            plain_autocomplete_action(map_scenarios)?;
        }
        ("scenarios", Some(scenarios_params)) => {
            let spn = Arc::new(from_file(Path::new(
                scenarios_params.value_of("spn").unwrap(),
            ))?);
            let output_path = scenarios_params.value_of("output").unwrap();
            let test_filepath = scenarios_params.value_of("test_filepath").unwrap();
            let n = scenarios_params
                .value_of("number")
                .map(|n| n.parse::<usize>().unwrap())
                .unwrap();
            create_scenarios(spn, output_path, test_filepath, n)?;
        }
        ("image_scenarios", Some(image_scenarios_params)) => {
            let spn = Arc::new(from_file(Path::new(
                image_scenarios_params.value_of("spn").unwrap(),
            ))?);
            let output_path = image_scenarios_params.value_of("output").unwrap();
            let test_filepath = image_scenarios_params.value_of("test_filepath").unwrap();
            let n = image_scenarios_params
                .value_of("number")
                .map(|n| n.parse::<usize>().unwrap())
                .unwrap();
            let format = usize_to_image_scenario_format(
                image_scenarios_params
                    .value_of("format")
                    .unwrap()
                    .parse::<usize>()
                    .unwrap(),
            )
            .unwrap();
            let classification = usize_to_classification_placement(
                image_scenarios_params
                    .value_of("classification")
                    .unwrap()
                    .parse::<usize>()
                    .unwrap(),
            )
            .unwrap();
            create_image_scenarios(spn, output_path, test_filepath, n, format, classification)?;
        }
        ("em", Some(em_params)) => {
            let spn = from_file(Path::new(em_params.value_of("spn").unwrap()))?;
            let output_path = em_params.value_of("output").unwrap();
            let training_data_path = em_params.value_of("training_data").unwrap();
            let test_data_path = em_params.value_of("test_data").unwrap();
            let iterations = match em_params.value_of("iterations") {
                Some(iterations_str) => iterations_str.parse::<usize>().unwrap(),
                None => 30,
            };
            let stop_epsilon = match em_params.value_of("stop_epsilon") {
                Some(stop_epsilon_str) => stop_epsilon_str.parse::<f64>().unwrap(),
                None => f64::EPSILON,
            };
            em(
                spn,
                output_path,
                training_data_path,
                test_data_path,
                iterations,
                stop_epsilon,
            )?;
        }
        ("gd", Some(gd_params)) => {
            let spn = from_file(Path::new(gd_params.value_of("spn").unwrap()))?;
            let output_path = gd_params.value_of("output").unwrap();
            let training_data_path = gd_params.value_of("training_data").unwrap();
            let test_data_path = gd_params.value_of("test_data").unwrap();
            let iterations = match gd_params.value_of("iterations") {
                Some(iterations_str) => iterations_str.parse::<usize>().unwrap(),
                None => 30,
            };
            let stop_epsilon = match gd_params.value_of("stop_epsilon") {
                Some(stop_epsilon_str) => stop_epsilon_str.parse::<f64>().unwrap(),
                None => f64::EPSILON,
            };
            gd(
                spn,
                output_path,
                training_data_path,
                test_data_path,
                iterations,
                stop_epsilon,
            )?;
        }
        ("map", Some(map_params)) => {
            let spn = from_file(Path::new(map_params.value_of("spn").unwrap()))?;
            let algorithm_name = map_params.value_of("algorithm").unwrap();
            let scenarios_file = map_params.value_of("map_scenarios").unwrap();
            let evidences_file = map_params.value_of("evidences_file");
            map(spn, algorithm_name, scenarios_file, evidences_file)?;
        }
        ("time_measure", Some(time_measure_params)) => {
            let spn = from_file(Path::new(time_measure_params.value_of("spn").unwrap()))?;
            let scenarios_file = time_measure_params.value_of("map_scenarios").unwrap();
            time_measure(spn, scenarios_file)?;
        }
        ("best_limited", Some(best_limited_params)) => {
            let spn = Arc::new(from_file(Path::new(
                best_limited_params.value_of("spn").unwrap(),
            ))?);
            let map_scenarios = map_scenarios_from_file(
                best_limited_params.value_of("map_scenarios").unwrap(),
                spn,
            )?;
            let time_limit = best_limited_params
                .value_of("time_limit")
                .map(|time_limit_as_str| time_limit_as_str.parse::<u64>().unwrap());
            best_limited(map_scenarios, time_limit)?;
        }
        ("value", Some(value_params)) => {
            let spn = from_file(Path::new(value_params.value_of("spn").unwrap()))?;
            let evidences_file = value_params.value_of("evidences").unwrap();
            value(spn, evidences_file)?;
        }
        ("classification", Some(classification_params)) => {
            let spn = from_file(Path::new(classification_params.value_of("spn").unwrap()))?;
            let evidences_file = classification_params.value_of("evidences").unwrap();
            let classification_var_index = classification_params
                .value_of("classification_var")
                .unwrap()
                .parse::<usize>()
                .unwrap();
            classification(spn, evidences_file, classification_var_index)?;
        }
        (other_command, _) => {
            println!["Command {} not implemented", other_command];
        }
    };
    Ok(())
}

fn learn_path_wrapper(learn_path_matches: &clap::ArgMatches) -> io::Result<()> {
    let mut rng = get_rng(learn_path_matches);
    if let Some(paths) = learn_path_matches.values_of("paths") {
        for path in paths.collect::<Vec<&str>>().iter() {
            learn_spn_on_path(&path, &mut rng)?;
        }
    }
    Ok(())
}
