use super::table::{experiment_evidence, experiment_time, experiment_value, ExperimentResult};
use crate::evidence::Evidence;
use crate::map::map_scenario::MapScenario;
use crate::map::max_search::max_search_with_forward_checking;
use std::fs::File;
use std::io::Write;
use std::sync::Arc;
use std::thread;
use std::time::Instant;

fn run_algorithm_and_check_time(
    algorithm: impl Fn(&MapScenario, Option<u64>, Option<Evidence>) -> Evidence,
    map_scenario: Arc<MapScenario>,
    time_limit: u64,
) -> ExperimentResult {
    let now = Instant::now();
    let evidence = algorithm(&map_scenario, Some(time_limit), None);
    let value = map_scenario.spn.value(&evidence);
    Some((evidence, now.elapsed().as_millis() as usize, value))
}

fn max_search_thread(
    map_scenario_arc: &Arc<MapScenario>,
    time_limit: u64,
) -> thread::JoinHandle<ExperimentResult> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    thread::spawn(move || {
        run_algorithm_and_check_time(
            max_search_with_forward_checking,
            map_scenario_arc_clone,
            time_limit,
        )
    })
}

pub fn max_table_action(
    map_scenarios: Vec<MapScenario>,
    evidences_filename: Option<&str>,
    times_filename: Option<&str>,
    time_limit: u64,
) {
    let mut maybe_evidences_file =
        evidences_filename.map(|filename| File::create(filename).unwrap());
    let mut maybe_times_file = times_filename.map(|filename| File::create(filename).unwrap());
    for map_scenario in map_scenarios {
        let map_scenario_arc = Arc::new(map_scenario);
        let maxsearch_handle = max_search_thread(&map_scenario_arc, time_limit);
        let max_search_result = maxsearch_handle.join().unwrap();
        println!["{}", experiment_value(&max_search_result),];
        if let Some(file) = &mut maybe_evidences_file {
            file.write_all(format!["{}\n", experiment_evidence(&max_search_result),].as_bytes())
                .unwrap();
        }
        if let Some(file) = &mut maybe_times_file {
            file.write_all(
                format!["{}\n", experiment_time(&max_search_result, time_limit),].as_bytes(),
            )
            .unwrap();
        }
    }
}
