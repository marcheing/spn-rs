use crate::evidence::evidences_from_file;
use crate::spn::io::file::to_file;
use crate::spn::SPN;
use crate::utils::em::do_em;
use std::io;
use std::path::Path;

pub fn em(
    mut spn: SPN,
    output_path: &str,
    training_data_path: &str,
    test_data_path: &str,
    iterations: usize,
    stop_epsilon: f64,
) -> io::Result<()> {
    let training_evidences = evidences_from_file(&spn, training_data_path)?;
    let test_evidences = evidences_from_file(&spn, test_data_path)?;
    let training_ll_value: f64 = training_evidences
        .iter()
        .map(|evidence| spn.log_value(evidence))
        .sum();
    let test_ll_value: f64 = test_evidences
        .iter()
        .map(|evidence| spn.log_value(evidence))
        .sum();

    println!["Before EM:"];
    println!["  Train LL: {}", training_ll_value];
    println!["  Test LL: {}", test_ll_value];

    do_em(
        &mut spn,
        &training_evidences,
        &test_evidences,
        iterations,
        stop_epsilon,
    );

    let training_ll_value: f64 = training_evidences
        .iter()
        .map(|evidence| spn.log_value(evidence))
        .sum();
    let test_ll_value: f64 = test_evidences
        .iter()
        .map(|evidence| spn.log_value(evidence))
        .sum();

    println!["After EM:"];
    println!["  Train LL: {}", training_ll_value];
    println!["  Test LL: {}", test_ll_value];

    to_file(Path::new(output_path), &spn)?;

    Ok(())
}
