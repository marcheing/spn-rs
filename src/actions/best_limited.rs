use crate::evidence::Evidence;
use crate::map::argmax_product::argmax_product_wrapper;
use crate::map::beam_search::beam_search_wrapper;
use crate::map::kbt::kbt_wrapper;
use crate::map::map_scenario::MapScenario;
use crate::map::max_product::max_product_wrapper;
use crate::map::max_search::max_search_with_forward_checking;
use ordered_float::OrderedFloat;
use std::sync::Arc;
use std::thread;

fn run_algorithm(
    algorithm: impl Fn(&MapScenario, Option<u64>) -> Option<Evidence>,
    map_scenario: Arc<MapScenario>,
    time_limit: Option<u64>,
) -> Option<(Evidence, f64)> {
    match algorithm(&map_scenario, time_limit) {
        Some(evidence) => {
            let value = map_scenario.spn.value(&evidence);
            Some((evidence, value))
        }
        None => None,
    }
}

fn maxprod_thread(
    map_scenario_arc: &Arc<MapScenario>,
    time_limit: Option<u64>,
) -> thread::JoinHandle<Option<(Evidence, f64)>> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    thread::spawn(move || run_algorithm(max_product_wrapper, map_scenario_arc_clone, time_limit))
}

fn argmaxprod_thread(
    map_scenario_arc: &Arc<MapScenario>,
    time_limit: Option<u64>,
) -> thread::JoinHandle<Option<(Evidence, f64)>> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    thread::spawn(move || run_algorithm(argmax_product_wrapper, map_scenario_arc_clone, time_limit))
}

fn beam_search_3_thread(
    map_scenario_arc: &Arc<MapScenario>,
    time_limit: Option<u64>,
) -> thread::JoinHandle<Option<(Evidence, f64)>> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    let beam_search_3_function_wrapper =
        |ms: &MapScenario, tl: Option<u64>| -> Option<Evidence> { beam_search_wrapper(&ms, 3, tl) };
    thread::spawn(move || {
        run_algorithm(
            beam_search_3_function_wrapper,
            map_scenario_arc_clone,
            time_limit,
        )
    })
}

fn kbt3_thread(
    map_scenario_arc: &Arc<MapScenario>,
    time_limit: Option<u64>,
) -> thread::JoinHandle<Option<(Evidence, f64)>> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    let kbt_function_wrapper =
        |ms: &MapScenario, tl: Option<u64>| -> Option<Evidence> { kbt_wrapper(&ms, 3, tl) };
    thread::spawn(move || run_algorithm(kbt_function_wrapper, map_scenario_arc_clone, time_limit))
}

fn max_search_thread(
    map_scenario_arc: &Arc<MapScenario>,
    time_limit: Option<u64>,
) -> thread::JoinHandle<Option<(Evidence, f64)>> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    let max_search_function_wrapper = |ms: &MapScenario, tl: Option<u64>| -> Option<Evidence> {
        Some(max_search_with_forward_checking(ms, tl, None))
    };
    thread::spawn(move || {
        run_algorithm(
            max_search_function_wrapper,
            map_scenario_arc_clone,
            time_limit,
        )
    })
}

pub fn best_limited(
    map_scenarios: Vec<MapScenario>,
    time_limit: Option<u64>,
) -> std::io::Result<()> {
    for scenario in map_scenarios {
        let map_scenario_arc = Arc::new(scenario);
        let maxprod_handle = maxprod_thread(&map_scenario_arc, time_limit);
        let argmaxprod_handle = argmaxprod_thread(&map_scenario_arc, time_limit);
        let beam_search_3_handle = beam_search_3_thread(&map_scenario_arc, time_limit);
        let kbt_3_handle = kbt3_thread(&map_scenario_arc, time_limit);
        let max_search_handle = max_search_thread(&map_scenario_arc, time_limit);
        let mut results = vec![
            maxprod_handle,
            argmaxprod_handle,
            beam_search_3_handle,
            kbt_3_handle,
            max_search_handle,
        ]
        .into_iter()
        .map(|handle| handle.join().unwrap())
        .collect::<Vec<Option<(Evidence, f64)>>>();
        results.retain(|opt| opt.is_some());
        let best_result = results
            .into_iter()
            .map(|opt| opt.unwrap())
            .max_by_key(|(_, value)| OrderedFloat::<f64>(*value))
            .unwrap();
        for (var, values) in best_result.0.iter() {
            print!["{}", var];
            for value in values {
                print![" {}", value];
            }
            println![];
        }
        println!["{}", best_result.1];
        println!["---"];
    }
    Ok(())
}
