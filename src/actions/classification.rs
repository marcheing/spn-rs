use super::value::evidences_file_to_vec;
use crate::spn::SPN;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

pub fn classification(
    spn: SPN,
    evidences_filename: &str,
    classification_var_id: usize,
) -> std::io::Result<()> {
    let mut file = File::open(evidences_filename)?;
    let mut file_string = String::new();
    let var = &spn.variables[classification_var_id];
    file.read_to_string(&mut file_string)?;

    let incomplete_evidences = evidences_file_to_vec(Path::new(evidences_filename))?;
    for incomplete_evidence in incomplete_evidences {
        let mut chosen_category = None;
        let mut best_value = None;
        for category in 0..var.n_categories {
            let mut evidence = incomplete_evidence.clone();
            evidence.insert(var.id, vec![category]);
            let value = spn.log_value(&evidence);

            if best_value.is_none() || value > best_value.unwrap() {
                best_value = Some(value);
                chosen_category = Some(category);
            }
        }
        println!["{}", chosen_category.unwrap()];
    }

    Ok(())
}
