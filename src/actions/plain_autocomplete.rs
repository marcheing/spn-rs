use crate::evidence::Evidence;
use crate::map::map_scenario::MapScenario;
use ordered_float::OrderedFloat;
use std::collections::HashMap;

pub fn plain_autocomplete_action(scenarios: Vec<MapScenario>) -> std::io::Result<()> {
    for scenario in scenarios {
        let spn_ref = scenario.spn;
        let mut evidence = if let Some(ev) = scenario.evidence {
            ev.clone()
        } else {
            Evidence::new(HashMap::default())
        };
        if let Some(marginalized) = scenario.marginalized {
            for marg_variable in marginalized {
                evidence.insert(marg_variable.id, (0..marg_variable.n_categories).collect());
            }
        }
        for variable in scenario.query {
            let chosen_value = (0..variable.n_categories)
                .max_by_key(|value| {
                    let mut ev = evidence.clone();
                    ev.insert(variable.id, vec![*value]);
                    OrderedFloat::<f64>(spn_ref.value(&ev))
                })
                .unwrap();
            println!["{} {}", variable.id, chosen_value];
        }
        println!["---"];
    }
    Ok(())
}
