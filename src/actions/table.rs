use crate::evidence::Evidence;
use crate::map::argmax_product::argmax_product_wrapper;
use crate::map::beam_search::beam_search_wrapper;
use crate::map::kbt::kbt_wrapper;
use crate::map::local_search::local_search_wrapper;
use crate::map::map_scenario::MapScenario;
use crate::map::max_product::max_product_wrapper;
use crate::map::max_search::max_search_with_forward_checking;
use std::fs::File;
use std::io::Write;
use std::sync::Arc;
use std::thread;
use std::time::Instant;

pub type ExperimentResult = Option<(Evidence, usize, f64)>;

pub fn experiment_evidence(experiment_result: &ExperimentResult) -> Evidence {
    experiment_result
        .as_ref()
        .map_or(Evidence::default(), |(ev, _, _)| ev.clone())
}

pub fn experiment_time(experiment_result: &ExperimentResult, time_limit: u64) -> usize {
    experiment_result
        .as_ref()
        .map_or(time_limit as usize, |(_, time, _)| *time)
}

pub fn experiment_value(experiment_result: &ExperimentResult) -> f64 {
    experiment_result
        .as_ref()
        .map_or(0f64, |(_, _, value)| *value)
}

fn run_algorithm_and_check_time(
    algorithm: impl Fn(&MapScenario, Option<u64>) -> Option<Evidence>,
    map_scenario: Arc<MapScenario>,
    count: usize,
    time_limit: u64,
) -> ExperimentResult {
    let now = Instant::now();
    let mut evidence_opt: Option<Evidence> = None;
    for _ in 0..count {
        evidence_opt = algorithm(&map_scenario, Some(time_limit));
    }
    match evidence_opt {
        Some(evidence) => {
            let value = map_scenario.spn.value(&evidence);
            Some((evidence, now.elapsed().as_millis() as usize / count, value))
        }
        None => None,
    }
}

fn maxprod_thread(
    map_scenario_arc: &Arc<MapScenario>,
    count: usize,
    time_limit: u64,
) -> thread::JoinHandle<ExperimentResult> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    thread::spawn(move || {
        run_algorithm_and_check_time(
            max_product_wrapper,
            map_scenario_arc_clone,
            count,
            time_limit,
        )
    })
}

fn local_search_thread(
    map_scenario_arc: &Arc<MapScenario>,
    evidence: Evidence,
    count: usize,
    time_limit: u64,
) -> thread::JoinHandle<ExperimentResult> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    let local_search_function_wrapper =
        move |ms: &MapScenario, tl: Option<u64>| -> Option<Evidence> {
            Some(local_search_wrapper(&ms, Some(evidence.clone()), tl))
        };
    thread::spawn(move || {
        run_algorithm_and_check_time(
            local_search_function_wrapper,
            map_scenario_arc_clone,
            count,
            time_limit,
        )
    })
}

fn argmaxprod_thread(
    map_scenario_arc: &Arc<MapScenario>,
    count: usize,
    time_limit: u64,
) -> thread::JoinHandle<ExperimentResult> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    thread::spawn(move || {
        run_algorithm_and_check_time(
            argmax_product_wrapper,
            map_scenario_arc_clone,
            count,
            time_limit,
        )
    })
}

fn beam_search_10_thread(
    map_scenario_arc: &Arc<MapScenario>,
    count: usize,
    time_limit: u64,
) -> thread::JoinHandle<ExperimentResult> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    let beam_search_3_function_wrapper =
        |ms: &MapScenario, tl: Option<u64>| -> Option<Evidence> { beam_search_wrapper(&ms, 3, tl) };
    thread::spawn(move || {
        run_algorithm_and_check_time(
            beam_search_3_function_wrapper,
            map_scenario_arc_clone,
            count,
            time_limit,
        )
    })
}

fn kbt10_thread(
    map_scenario_arc: &Arc<MapScenario>,
    count: usize,
    time_limit: u64,
) -> thread::JoinHandle<ExperimentResult> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    let kbt_function_wrapper =
        |ms: &MapScenario, tl: Option<u64>| -> Option<Evidence> { kbt_wrapper(&ms, 3, tl) };
    thread::spawn(move || {
        run_algorithm_and_check_time(
            kbt_function_wrapper,
            map_scenario_arc_clone,
            count,
            time_limit,
        )
    })
}

fn max_search_thread(
    map_scenario_arc: &Arc<MapScenario>,
    count: usize,
    time_limit: u64,
    evidence: Evidence,
) -> thread::JoinHandle<ExperimentResult> {
    let map_scenario_arc_clone = Arc::clone(map_scenario_arc);
    let max_search_function_wrapper =
        move |ms: &MapScenario, tl: Option<u64>| -> Option<Evidence> {
            Some(max_search_with_forward_checking(
                &ms,
                tl,
                Some(evidence.clone()),
            ))
        };
    thread::spawn(move || {
        run_algorithm_and_check_time(
            max_search_function_wrapper,
            map_scenario_arc_clone,
            count,
            time_limit,
        )
    })
}

pub fn table_action(
    map_scenarios: Vec<MapScenario>,
    evidences_filename: Option<&str>,
    times_filename: Option<&str>,
    count: usize,
    time_limit: u64,
) {
    println!["MaxProd,Local Search,ArgmaxProd,Local Search Argmax,BS,KBT,MaxSearch"];
    let mut maybe_evidences_file =
        evidences_filename.map(|filename| File::create(filename).unwrap());
    let mut maybe_times_file = times_filename.map(|filename| File::create(filename).unwrap());
    for map_scenario in map_scenarios {
        let map_scenario_arc = Arc::new(map_scenario);
        let maxprod_handle = maxprod_thread(&map_scenario_arc, count, time_limit);
        let argmaxprod_handle = argmaxprod_thread(&map_scenario_arc, count, time_limit);
        let beam_search_10_handle = beam_search_10_thread(&map_scenario_arc, count, time_limit);
        let kbt_10_handle = kbt10_thread(&map_scenario_arc, count, time_limit);
        let max_prod_result = maxprod_handle.join().unwrap();
        let max_search_handle = max_search_thread(
            &map_scenario_arc,
            count,
            time_limit,
            max_prod_result.as_ref().unwrap().0.clone(),
        );
        let local_search_handle_opt = max_prod_result.as_ref().map(|(maxprod_evidence, _, _)| {
            local_search_thread(
                &map_scenario_arc,
                maxprod_evidence.clone(),
                count,
                time_limit,
            )
        });
        let argmax_prod_result = argmaxprod_handle.join().unwrap();
        let local_search_argmaxp_thread_opt =
            argmax_prod_result
                .as_ref()
                .map(|(argmaxprod_evidence, _, _)| {
                    local_search_thread(
                        &map_scenario_arc,
                        argmaxprod_evidence.clone(),
                        count,
                        time_limit,
                    )
                });
        let local_search_result = local_search_handle_opt
            .map(|handle| handle.join().unwrap())
            .flatten();
        let local_search_argmaxp_result = local_search_argmaxp_thread_opt
            .map(|handle| handle.join().unwrap())
            .flatten();
        let beam_search_result = beam_search_10_handle.join().unwrap();
        let kbt_result = kbt_10_handle.join().unwrap();
        let max_search_result = max_search_handle.join().unwrap();
        println![
            "{},{},{},{},{},{},{}",
            experiment_value(&max_prod_result),
            experiment_value(&local_search_result),
            experiment_value(&argmax_prod_result),
            experiment_value(&local_search_argmaxp_result),
            experiment_value(&beam_search_result),
            experiment_value(&kbt_result),
            experiment_value(&max_search_result),
        ];
        if let Some(file) = &mut maybe_evidences_file {
            file.write_all(
                format![
                    "{},{},{},{},{},{},{}\n",
                    experiment_evidence(&max_prod_result),
                    experiment_evidence(&local_search_result),
                    experiment_evidence(&argmax_prod_result),
                    experiment_evidence(&local_search_argmaxp_result),
                    experiment_evidence(&beam_search_result),
                    experiment_evidence(&kbt_result),
                    experiment_evidence(&max_search_result),
                ]
                .as_bytes(),
            )
            .unwrap();
        }
        if let Some(file) = &mut maybe_times_file {
            file.write_all(
                format![
                    "{},{},{},{},{},{},{}\n",
                    experiment_time(&max_prod_result, time_limit),
                    experiment_time(&local_search_result, time_limit),
                    experiment_time(&argmax_prod_result, time_limit),
                    experiment_time(&local_search_argmaxp_result, time_limit),
                    experiment_time(&beam_search_result, time_limit),
                    experiment_time(&kbt_result, time_limit),
                    experiment_time(&max_search_result, time_limit),
                ]
                .as_bytes(),
            )
            .unwrap();
        }
    }
}

pub fn verify_evidence_on_map_scenario(evidence: &Evidence, map_scenario: &MapScenario) {
    for var in &map_scenario.query {
        assert_eq![evidence[&var.id].len(), 1];
    }
    for var in (&map_scenario.marginalized).as_ref().unwrap() {
        assert![evidence[&var.id].len() > 1];
    }
    for (var, values) in (&map_scenario.evidence).as_ref().unwrap().iter() {
        for x in values {
            assert![evidence[&var].contains(x)];
        }
        for x in evidence[&var].iter() {
            assert![values.contains(&x)];
        }
    }
}
