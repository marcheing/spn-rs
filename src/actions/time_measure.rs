use crate::map::map_scenario::map_scenarios_from_file;
use crate::map::max_product::max_product_wrapper;
use crate::map::max_search::max_search_with_forward_checking;
use crate::spn::SPN;
use std::sync::Arc;
use std::time::Instant;

pub fn time_measure(spn: SPN, scenarios_file: &str) -> std::io::Result<()> {
    let spn_arc = Arc::new(spn);
    let time = Instant::now();
    for map_scenario in map_scenarios_from_file(scenarios_file, spn_arc)? {
        max_search_with_forward_checking(
            &map_scenario,
            Some(7200),
            max_product_wrapper(&map_scenario, None),
        );
    }
    let elapsed_time = time.elapsed();
    println!["{}", elapsed_time.as_millis()];

    Ok(())
}
