use crate::evidence::Evidence;
use crate::map::argmax_product::argmax_product_wrapper;
use crate::map::local_search::local_search_wrapper;
use crate::map::map_scenario::map_scenarios_to_file;
use crate::map::map_scenario::MapScenario;
use crate::map::max_product::max_product_wrapper;
use crate::spn::SPN;
use crate::variable::Variable;
use std::cmp::min;
use std::fs::File;
use std::io::{self, BufRead};
use std::sync::Arc;

pub enum ImageScenarioFormat {
    QFirstMMiddle,
    QFirstMLast,
    QMiddleMFirst,
    QMiddleMLast,
    QLastMFirst,
    QLastMMiddle,
}

pub fn usize_to_image_scenario_format(number: usize) -> Option<ImageScenarioFormat> {
    match number {
        0 => Some(ImageScenarioFormat::QFirstMMiddle),
        1 => Some(ImageScenarioFormat::QFirstMLast),
        2 => Some(ImageScenarioFormat::QMiddleMFirst),
        3 => Some(ImageScenarioFormat::QMiddleMLast),
        4 => Some(ImageScenarioFormat::QLastMFirst),
        5 => Some(ImageScenarioFormat::QLastMMiddle),
        _ => None,
    }
}

pub enum ClassificationPlacement {
    QUERY,
    MARGINAL,
    EVIDENCE,
}

pub fn usize_to_classification_placement(number: usize) -> Option<ClassificationPlacement> {
    match number {
        0 => Some(ClassificationPlacement::QUERY),
        1 => Some(ClassificationPlacement::MARGINAL),
        2 => Some(ClassificationPlacement::EVIDENCE),
        _ => None,
    }
}

fn instance_pair_to_vecs(pairs: Vec<(Variable, Vec<usize>)>) -> Vec<Variable> {
    pairs.into_iter().map(|(var, _)| var).collect()
}

fn add_instance_to_evidence(pairs: Vec<(Variable, Vec<usize>)>, evidence: &mut Evidence) {
    for (var, values) in pairs {
        evidence.insert(var.id, values);
    }
}

fn distance(value1: usize, value2: usize) -> usize {
    if value1 > value2 {
        value1 - value2
    } else {
        value2 - value1
    }
}

pub fn create_scenarios(
    spn: Arc<SPN>,
    output_path: &str,
    test_filepath: &str,
    n: usize,
) -> io::Result<()> {
    let test_file = File::open(test_filepath)?;
    let mut all_value_lines: Vec<Vec<usize>> = vec![];
    for line in io::BufReader::new(test_file).lines() {
        let line_str = line.unwrap();
        if line_str.starts_with("var") {
            continue;
        }
        all_value_lines.push(
            line_str
                .split(|c| c == ',' || c == ' ')
                .map(|value_string| value_string.parse::<usize>().unwrap())
                .collect::<Vec<usize>>(),
        );
    }
    all_value_lines.sort();
    all_value_lines.dedup();
    let mut scenarios = all_value_lines
        .into_iter()
        .map(|values| {
            let one_third_of_values_size = values.len() / 3;
            let mut query_vars = vec![];
            let mut marginal_vars = vec![];
            let mut evidence = Evidence::default();
            for variable_ref in spn.variables.iter() {
                let var_id = variable_ref.id;
                if var_id > one_third_of_values_size {
                    if var_id > 2 * one_third_of_values_size {
                        evidence.insert(var_id, vec![values[var_id]]);
                    } else {
                        marginal_vars.push(variable_ref.clone());
                    }
                } else {
                    query_vars.push(variable_ref.clone());
                }
            }
            let map_scenario = MapScenario::new(
                Arc::clone(&spn),
                query_vars,
                Some(marginal_vars),
                Some(evidence),
            );
            let max_prod_evidence = max_product_wrapper(&map_scenario, None).unwrap();
            let max_prod_local_search_evidence =
                local_search_wrapper(&map_scenario, Some(max_prod_evidence), None);
            let argmax_prod_evidence = argmax_product_wrapper(&map_scenario, None).unwrap();
            let argmax_prod_local_search_evidence =
                local_search_wrapper(&map_scenario, Some(argmax_prod_evidence), None);
            (
                map_scenario,
                spn.log_value(&max_prod_local_search_evidence),
                spn.log_value(&argmax_prod_local_search_evidence),
            )
        })
        .collect::<Vec<(MapScenario, f64, f64)>>();
    scenarios.sort_by(|(scenario1, _, _), (scenario2, _, _)| {
        format!["{}", scenario1]
            .partial_cmp(&format!["{}", scenario2])
            .unwrap()
    });
    scenarios.dedup();

    let scenarios_size = scenarios.len();
    let map_scenarios = scenarios
        .into_iter()
        .take(min(n, scenarios_size))
        .map(|(scenario, _, _)| scenario)
        .collect::<Vec<MapScenario>>();
    map_scenarios_to_file(output_path, &map_scenarios)?;

    Ok(())
}

pub fn create_image_scenarios(
    spn: Arc<SPN>,
    output_path: &str,
    test_filepath: &str,
    n: usize,
    format: ImageScenarioFormat,
    classification: ClassificationPlacement,
) -> io::Result<()> {
    let test_file = File::open(test_filepath)?;
    let mut scenarios = io::BufReader::new(test_file)
        .lines()
        .map(|line| {
            let mut values = line
                .unwrap()
                .split(',')
                .map(|value_string| value_string.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            let mut query_vars = vec![];
            let mut marginal_vars = vec![];
            let mut evidence = Evidence::default();
            let classification_value = values.pop().unwrap();
            let mut variables = spn.variables.clone();
            let last_var_id = variables.iter().max_by_key(|var| var.id).unwrap().id;
            let last_var_index = variables
                .iter()
                .position(|var| var.id == last_var_id)
                .unwrap();
            let classification_var = variables.remove(last_var_index);
            match classification {
                ClassificationPlacement::QUERY => {
                    query_vars.push(classification_var);
                }
                ClassificationPlacement::MARGINAL => {
                    marginal_vars.push(classification_var);
                }
                ClassificationPlacement::EVIDENCE => {
                    evidence.insert(classification_var.id, vec![classification_value]);
                }
            }
            let one_third_of_values_size = values.len() / 3;
            let mut first_values = vec![];
            let mut middle_values = vec![];
            let mut last_values = vec![];
            for variable_ref in variables.iter() {
                let to_insert = (variable_ref.clone(), vec![values[variable_ref.id]]);
                let var_id = variable_ref.id;
                if var_id > one_third_of_values_size {
                    if var_id > 2 * one_third_of_values_size {
                        last_values.push(to_insert);
                    } else {
                        middle_values.push(to_insert);
                    }
                } else {
                    first_values.push(to_insert);
                }
            }
            let (query_pairs, marginal_pairs, evidence_pairs) = match format {
                ImageScenarioFormat::QFirstMMiddle => (first_values, middle_values, last_values),
                ImageScenarioFormat::QFirstMLast => (first_values, last_values, middle_values),
                ImageScenarioFormat::QMiddleMFirst => (middle_values, first_values, last_values),
                ImageScenarioFormat::QMiddleMLast => (middle_values, last_values, first_values),
                ImageScenarioFormat::QLastMFirst => (last_values, first_values, middle_values),
                ImageScenarioFormat::QLastMMiddle => (last_values, middle_values, first_values),
            };
            let query_vars = instance_pair_to_vecs(query_pairs);
            let marginal_vars = instance_pair_to_vecs(marginal_pairs);
            add_instance_to_evidence(evidence_pairs, &mut evidence);
            let map_scenario = MapScenario::new(
                Arc::clone(&spn),
                query_vars,
                Some(marginal_vars),
                Some(evidence),
            );
            let max_prod_evidence = max_product_wrapper(&map_scenario, None).unwrap();
            let max_prod_local_search_evidence =
                local_search_wrapper(&map_scenario, Some(max_prod_evidence), None);
            let argmax_prod_evidence = argmax_product_wrapper(&map_scenario, None).unwrap();
            let argmax_prod_local_search_evidence =
                local_search_wrapper(&map_scenario, Some(argmax_prod_evidence), None);
            let max_prod_local_search_error: usize = map_scenario
                .query
                .iter()
                .map(|query_var| {
                    distance(
                        *max_prod_local_search_evidence[&query_var.id]
                            .first()
                            .unwrap(),
                        values[query_var.id],
                    )
                })
                .sum();
            let argmax_prod_local_search_error: usize = map_scenario
                .query
                .iter()
                .map(|query_var| {
                    distance(
                        *argmax_prod_local_search_evidence[&query_var.id]
                            .first()
                            .unwrap(),
                        values[query_var.id],
                    )
                })
                .sum();
            (
                map_scenario,
                max_prod_local_search_error,
                argmax_prod_local_search_error,
            )
        })
        .collect::<Vec<(MapScenario, usize, usize)>>();
    scenarios.sort_by(|(scenario1, _, _), (scenario2, _, _)| {
        format!["{}", scenario1]
            .partial_cmp(&format!["{}", scenario2])
            .unwrap()
    });
    scenarios.dedup();

    let scenarios_size = scenarios.len();
    let map_scenarios = scenarios
        .into_iter()
        .take(min(n, scenarios_size))
        .map(|(scenario, _, _)| scenario)
        .collect::<Vec<MapScenario>>();
    map_scenarios_to_file(output_path, &map_scenarios)?;

    Ok(())
}
