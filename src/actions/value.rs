use crate::evidence::Evidence;
use crate::spn::SPN;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

pub fn value(spn: SPN, evidences_filename: &str) -> std::io::Result<()> {
    for evidence in evidences_file_to_vec(&Path::new(evidences_filename))? {
        println!["{}", spn.value(&evidence)];
    }
    Ok(())
}

pub fn evidences_file_to_vec(evidences_filename: &Path) -> std::io::Result<Vec<Evidence>> {
    let mut file = File::open(evidences_filename)?;
    let mut file_string = String::new();
    file.read_to_string(&mut file_string)?;

    let mut evidences = vec![];
    let mut evidence = Evidence::default();
    for line in file_string.split('\n') {
        if line.is_empty() {
            continue;
        }
        if line.trim() == "-" {
            evidences.push(evidence);
            evidence = Evidence::default();
            continue;
        }
        let evidence_info: Vec<usize> = line
            .trim()
            .split(|c| c == ' ' || c == ',')
            .map(|number_str| number_str.parse::<usize>().unwrap())
            .collect();
        evidence.insert(*evidence_info.first().unwrap(), evidence_info[1..].to_vec());
    }

    Ok(evidences)
}

pub fn evidences_vec_to_file(evidences_vec: &[Evidence], filename: &str) -> std::io::Result<()> {
    let mut file = File::create(filename)?;
    for evidence in evidences_vec {
        for (var_id, values) in evidence.iter() {
            file.write_fmt(format_args!["{}", var_id])?;
            for value in values {
                file.write_fmt(format_args![" {}", value])?
            }
            file.write_fmt(format_args!["\n"])?;
        }
        file.write_fmt(format_args!["-\n"])?;
    }

    Ok(())
}
