use crate::data::datafile::load_data;
use crate::learn::gens;
use crate::spn::io::file::to_file;
use crate::spn::node::Node;
use crate::spn::SPN;
use crate::utils::random::get_rng;
use crate::utils::vector::sample;
use rand::rngs::StdRng;
use std::io;
use std::path::Path;

pub fn bag_wrapper(bag_params: &clap::ArgMatches) -> io::Result<()> {
    let n_bags = bag_params
        .value_of("bags")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let path = bag_params.value_of("path").unwrap();
    let spn_filepath = bag_params.value_of("spn_filepath").unwrap();
    let kclusters = bag_params
        .value_of("kclusters")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let pval = bag_params.value_of("pval").unwrap().parse::<f64>().unwrap();
    let mut rng = get_rng(bag_params);
    learn_bagging_on_path(n_bags, &path, &spn_filepath, kclusters, pval, &mut rng)?;
    Ok(())
}

fn learn_bagging_on_path(
    n_bags: usize,
    path: &str,
    spn_filepath: &str,
    kclusters: usize,
    pval: f64,
    rng: &mut StdRng,
) -> io::Result<()> {
    let (data, variables) = load_data(path)?;
    let mut main_spn_nodes = vec![];
    let mut subspn_roots = vec![];
    let mut last_index = 0;
    let uniform_weight = 1f64 / (n_bags as f64);
    let mut total_number_of_nodes = 0;
    (0..n_bags).for_each(|_| {
        let new_data = sample(&data, rng);
        let spn = gens(&variables, &new_data, kclusters, pval, rng, None, 0);
        total_number_of_nodes += spn.nodes.len();
        for node in spn.nodes {
            match node {
                Node::Indicator(var_id, assignment) => {
                    main_spn_nodes.push(Node::Indicator(var_id, assignment));
                }
                Node::Sum(children, weights) => {
                    let new_children = children
                        .into_iter()
                        .map(|child_id| child_id + last_index)
                        .collect();
                    main_spn_nodes.push(Node::Sum(new_children, weights));
                }
                Node::Product(children) => {
                    let new_children = children
                        .into_iter()
                        .map(|child_id| child_id + last_index)
                        .collect();
                    main_spn_nodes.push(Node::Product(new_children));
                }
                _ => {}
            }
        }
        subspn_roots.push(spn.root_id + last_index);
        last_index = main_spn_nodes.len();
    });
    let root_node = Node::Sum(subspn_roots, vec![uniform_weight; n_bags]);
    total_number_of_nodes += 1;
    let root_id = main_spn_nodes.len();
    main_spn_nodes.push(root_node);
    let spn = SPN::new(main_spn_nodes, variables, root_id);
    assert_eq![total_number_of_nodes, spn.nodes.len()];
    let minimized_spn = SPN::minimize(spn);
    to_file(Path::new(spn_filepath), &minimized_spn)?;

    Ok(())
}
