use super::value::evidences_vec_to_file;
use crate::evidence::Evidence;
use crate::map::argmax_product::argmax_product_wrapper;
use crate::map::beam_search::beam_search_wrapper;
use crate::map::kbt::kbt_wrapper;
use crate::map::local_search::local_search_wrapper;
use crate::map::map_scenario::map_scenarios_from_file;
use crate::map::map_scenario::MapScenario;
use crate::map::max_product::max_product_wrapper;
use crate::map::max_search::{forward_checking, max_search};
use crate::spn::SPN;
use std::sync::Arc;

pub fn map(
    spn: SPN,
    algorithm_name: &str,
    scenarios_file: &str,
    evidences_filename: Option<&str>,
) -> std::io::Result<()> {
    let mut variables = spn.variables.clone();
    let spn_arc = Arc::new(spn);
    variables.sort_by_key(|var| var.id);
    let mut evidences = vec![];
    for map_scenario in map_scenarios_from_file(scenarios_file, spn_arc)? {
        let maybe_evidence = run_algorithm_from_name(algorithm_name, &map_scenario);
        match maybe_evidence {
            Some(evidence) => {
                for (variable, values) in evidence.iter() {
                    print!["{}", variable];
                    for value in values {
                        print![" {}", value];
                    }
                    println![];
                }
                let value = map_scenario.spn.value(&evidence);
                println!["{}", value];
                evidences.push(evidence);
            }
            None => {
                println![]
            }
        }
        println!["---"];
    }
    if let Some(evidences_filename_str) = evidences_filename {
        evidences_vec_to_file(&evidences, evidences_filename_str)?;
    }

    Ok(())
}

fn run_algorithm_from_name(name: &str, scenario: &MapScenario) -> Option<Evidence> {
    match name {
        "max_product" | "max-product" | "bt" | "best-tree" | "best_tree" | "mp" => {
            max_product_wrapper(scenario, None)
        }
        "argmax_product" | "argmax-product" | "amp" => argmax_product_wrapper(scenario, None),
        "beam_search" | "beam-search" | "bs" | "beam_search_3" | "beam-search-3" | "bs3" => {
            beam_search_wrapper(scenario, 3, None)
        }
        "k_best_tree" | "k-best-tree" | "kbt" | "k_best_tree_3" | "k-best-tree-3" | "kbt3" => {
            kbt_wrapper(scenario, 3, None)
        }
        "max_search" | "max-search" | "ms" => {
            Some(max_search(scenario, &forward_checking, None, Some(600)))
        }
        "local_search_argmax" | "local-search-argmax" => {
            let evidence = argmax_product_wrapper(scenario, None);
            Some(local_search_wrapper(scenario, evidence, None))
        }
        _ => {
            println!["Unknown algorithm name: {}", name];
            None
        }
    }
}
