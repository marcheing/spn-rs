use crate::independence::union_find::{UnionFindNode, UnionFindTree};
use crate::variable::Variable;
use rgsl::randist::chi_squared::chisq_P;
use rgsl::statistics::correlation;
use std::collections::HashMap;

pub struct IndependencyGraph {
    adjacency_list: HashMap<usize, Vec<usize>>,
    pub kset: Vec<Vec<usize>>,
}

impl IndependencyGraph {
    pub fn new(var_datas: Vec<(Variable, Vec<usize>)>, pval: f64) -> IndependencyGraph {
        let mut independency_graph = IndependencyGraph {
            adjacency_list: HashMap::default(),
            kset: vec![],
        };

        let size = var_datas.len();
        let mut ids = vec![0; size];
        let mut reverse_ids: HashMap<usize, usize> = HashMap::default();

        for i in 0..size {
            ids[i] = var_datas[i].0.id;
            reverse_ids.insert(ids[i], i);
            independency_graph.adjacency_list.insert(ids[i], vec![]);
        }

        let sets = (0..size).map(|i| UnionFindNode::new(ids[i], i)).collect();
        let mut tree = UnionFindTree::new(sets);

        println!["Constructing Independcy Graph..."];

        for i in 0..size {
            for j in (i + 1)..size {
                let v1 = ids[i];
                let v2 = ids[j];

                if tree.find(i) == tree.find(j) {
                    continue;
                }

                /*let p = var_datas[i].0.n_categories;
                let q = var_datas[j].0.n_categories;

                let mut contingency_matrix: Vec<Vec<usize>> =
                    (0..(p + 1)).map(|_| vec![0; q + 1]).collect();
                let m = var_datas[i].1.len();
                for k in 0..m {
                    contingency_matrix[var_datas[i].1[k]][var_datas[j].1[k]] += 1;
                }

                let mut total_x_axis = vec![0; q];
                let mut total_y_axis: usize;
                let mut total_x_plus_y = 0;

                for contingency_matrix_line in contingency_matrix.iter_mut().take(p) {
                    total_y_axis = 0;
                    for (y, total_x_axis_elem) in total_x_axis.iter_mut().enumerate().take(q) {
                        total_y_axis += contingency_matrix_line[y];
                        *total_x_axis_elem += contingency_matrix_line[y];
                    }
                    contingency_matrix_line[q] = total_y_axis;
                }

                for (y, total_x_axis_elem) in total_x_axis.iter().enumerate().take(q) {
                    contingency_matrix[p][y] = *total_x_axis_elem;
                    total_x_plus_y += total_x_axis[y];
                }

                contingency_matrix[p][q] = total_x_plus_y;
                let indep = gtest(p, q, &contingency_matrix, pval);*/
                let indep = pearson(&var_datas[i].1, &var_datas[j].1, pval);

                if !indep {
                    independency_graph
                        .adjacency_list
                        .get_mut(&v1)
                        .unwrap()
                        .push(v2);
                    independency_graph
                        .adjacency_list
                        .get_mut(&v2)
                        .unwrap()
                        .push(v1);
                    tree.node_union(i, j);
                }
            }
        }

        independency_graph.kset = vec![];

        for i in 0..size {
            if i == tree.nodes[i].parent {
                independency_graph.kset.push(tree.var_ids(i));
            }
        }

        independency_graph
    }
}

fn gtest(p: usize, q: usize, data: &[Vec<usize>], sigval: f64) -> bool {
    //println!["Performing gtest..."];
    let mut expect = vec![vec![0f64; q]; p];
    //println!["Data: {:?}", data];
    for i in 0..p {
        for j in 0..q {
            expect[i][j] = (data[p][j] * data[i][q]) as f64 / data[p][q] as f64;
        }
    }

    let df = ((p - 1) * (q - 1)) as f64;
    let mut sum = 0f64;

    for i in 0..p {
        for j in 0..q {
            if expect[i][j] == 0f64 {
                continue;
            }
            let o = data[i][j] as f64;
            if o == 0f64 {
                continue;
            }
            sum += o * (o / expect[i][j]).ln();
        }
    }

    sum *= 2f64;
    let c = chisq_P(sum, df);
    if c > 0f64 {
        println!["Sum: {}, df: {}, chisq: {}", sum, df, c];
    }
    let cmp = 1f64 - chisq_P(sum, df);
    //if cmp < 1f64 {
    //    println!["{}", cmp];
    // }
    cmp >= sigval
}

fn not_all_zero(vec: &[f64]) -> bool {
    for x in vec {
        if *x != 0f64 {
            return true;
        }
    }
    false
}

fn pearson(data_var_1: &[usize], data_var_2: &[usize], pval: f64) -> bool {
    let n = data_var_1.len();
    let data_var_1_as_float = data_var_1
        .iter()
        .map(|val| *val as f64)
        .collect::<Vec<f64>>();
    let data_var_2_as_float = data_var_2
        .iter()
        .map(|val| *val as f64)
        .collect::<Vec<f64>>();
    let mut pearson_r = correlation(&data_var_1_as_float, 1, &data_var_2_as_float, 1, n);

    //if not_all_zero(&data_var_1_as_float) {
    //    println!["Pearson: {}\nData: {:?}", pearson_r, data_var_1_as_float];
    //}

    if pearson_r.is_nan() {
        return true;
    }

    if pearson_r < 0f64 {
        pearson_r = -pearson_r;
    }

    pearson_r < pval
}
