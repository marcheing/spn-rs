#[derive(Debug)]
pub struct UnionFindNode {
    pub rank: usize,
    pub parent: usize,
    pub var_id: usize,
    pub children: Vec<usize>,
}

#[derive(Default)]
pub struct UnionFindTree {
    pub nodes: Vec<UnionFindNode>,
}

impl UnionFindNode {
    pub fn new(var_id: usize, parent: usize) -> Self {
        UnionFindNode {
            rank: 0,
            parent,
            var_id,
            children: vec![],
        }
    }
}

impl UnionFindTree {
    pub fn new(nodes: Vec<UnionFindNode>) -> Self {
        UnionFindTree { nodes }
    }

    pub fn find(&mut self, index: usize) -> usize {
        let mut node = &self.nodes[index];
        let mut indexes_to_change = vec![];
        let mut current_node_index = index;
        while node.parent != current_node_index {
            indexes_to_change.push(current_node_index);
            current_node_index = node.parent;
            node = &self.nodes[node.parent];
        }
        indexes_to_change
            .into_iter()
            .for_each(|index| self.nodes[index].parent = current_node_index);
        current_node_index
    }

    pub fn var_ids(&self, index: usize) -> Vec<usize> {
        let node = &self.nodes[index];
        let mut children = vec![node.var_id];
        for child in &node.children {
            children.extend_from_slice(&self.var_ids(*child));
        }
        children
    }

    pub fn node_union(&mut self, x: usize, y: usize) -> Option<usize> {
        let node_x = self.find(x);
        let node_y = self.find(y);
        if node_x == node_y {
            return None;
        }

        if self.nodes[node_x].rank > self.nodes[node_y].rank {
            self.nodes[node_y].parent = node_x;
            self.nodes[node_x].children.push(node_y);
            return Some(node_x);
        }
        self.nodes[node_x].parent = node_y;
        self.nodes[node_y].children.push(node_x);
        if self.nodes[node_x].rank == self.nodes[node_y].rank {
            self.nodes[node_y].rank += 1;
        }
        Some(node_y)
    }
}
