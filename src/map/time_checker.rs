use std::sync::mpsc::Receiver;

pub struct TimeChecker {
    receiver: Receiver<bool>,
    done: bool,
}

impl TimeChecker {
    pub fn new(receiver: Receiver<bool>) -> Self {
        TimeChecker {
            receiver,
            done: false,
        }
    }

    pub fn check(&mut self) -> bool {
        if self.done || self.receiver.try_recv().is_ok() {
            self.done = true;
        }
        self.done
    }
}
