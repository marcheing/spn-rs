use crate::evidence::Evidence;
use crate::spn::node::Node;
use crate::spn::SPN;
use crate::utils::f64::partial_ordering;
use std::collections::HashMap;

type MarginalDict = HashMap<usize, HashMap<usize, f64>>;

pub struct HybridMaxProductResult {
    pub evidence: Option<Evidence>,
    pub max_product_value: Option<f64>,
    pub sum_product_value: Option<f64>,
    pub marginal_dict: MarginalDict,
}

impl HybridMaxProductResult {
    pub fn extract(self) -> (Option<Evidence>, MarginalDict) {
        (self.evidence, self.marginal_dict)
    }
}

pub fn hybrid_max_product(
    spn: &SPN,
    node_id: usize,
    evidence: &Evidence,
) -> HybridMaxProductResult {
    let root = &spn.nodes[node_id];
    match root {
        Node::Indicator(var_id, assign) => hybrid_max_product_indicator(*var_id, *assign, evidence),
        Node::Product(children_ids) => hybrid_max_product_prod(spn, children_ids, evidence),
        Node::Sum(children_ids, weights) => {
            hybrid_max_product_sum(spn, children_ids, weights, evidence)
        }
        _ => panic![],
    }
}

fn hybrid_max_product_indicator(
    var_id: usize,
    assign: usize,
    evidence: &Evidence,
) -> HybridMaxProductResult {
    let mut marginal_dict = MarginalDict::default();
    for category in evidence[&var_id].iter() {
        let mut value_hash = HashMap::default();
        value_hash.insert(*category, 0f64);
        marginal_dict.insert(var_id, value_hash);
    }
    if evidence[&var_id].contains(&assign) {
        let mut varset = HashMap::default();
        let mut values = Vec::default();
        values.push(assign);
        marginal_dict.get_mut(&var_id).unwrap().insert(assign, 1f64);
        varset.insert(var_id, values);
        return HybridMaxProductResult {
            evidence: Some(Evidence::new(varset)),
            max_product_value: Some(1f64),
            sum_product_value: Some(1f64),
            marginal_dict,
        };
    }
    HybridMaxProductResult {
        evidence: None,
        max_product_value: None,
        sum_product_value: None,
        marginal_dict,
    }
}

fn hybrid_max_product_prod(
    spn: &SPN,
    children_ids: &[usize],
    evidence: &Evidence,
) -> HybridMaxProductResult {
    let mut new_evidence = Evidence::default();
    let mut value = 1f64;
    let mut marginal_dict = MarginalDict::default();
    for (var, values) in evidence.iter() {
        let mut value_hash = HashMap::default();
        for assign in values {
            value_hash.insert(*assign, 1f64);
        }
        marginal_dict.insert(*var, value_hash);
    }
    for child_id in children_ids {
        let mut varset = HashMap::default();
        for (var, values) in evidence.iter() {
            if spn.scope_subspn(*child_id).contains(var) {
                varset.insert(*var, values.clone());
            }
        }
        let child_evidence = Evidence::new(varset);
        let result = hybrid_max_product(spn, *child_id, &child_evidence);
        if let Some(result_evidence) = result.evidence {
            new_evidence.merge_mut(&result_evidence);
        }
        if let Some(max_product_value) = result.max_product_value {
            value *= max_product_value;
        }
        for (var, values_hash) in result.marginal_dict {
            for (assign, the_value) in values_hash {
                let prev_value = marginal_dict[&var][&assign];
                marginal_dict
                    .get_mut(&var)
                    .unwrap()
                    .insert(assign, prev_value * the_value);
            }
        }
    }
    HybridMaxProductResult {
        evidence: Some(new_evidence),
        max_product_value: Some(value),
        sum_product_value: Some(value),
        marginal_dict,
    }
}

fn hybrid_max_product_sum(
    spn: &SPN,
    children_ids: &[usize],
    weights: &[f64],
    evidence: &Evidence,
) -> HybridMaxProductResult {
    let child_map_results = children_ids
        .iter()
        .map(|child_id| hybrid_max_product(spn, *child_id, evidence));
    let mut results = Vec::default();
    let mut acc_val = 0f64;
    let mut marginal_dict = MarginalDict::default();
    for (var, values) in evidence.iter() {
        let mut value_hash = HashMap::default();
        for value in values {
            value_hash.insert(*value, 0f64);
        }
        marginal_dict.insert(*var, value_hash);
    }
    for (map_result, weight) in child_map_results.zip(weights) {
        let value = weight
            * match map_result.max_product_value {
                Some(val) => val,
                None => 0f64,
            };
        results.push((
            match map_result.evidence {
                Some(ev) => Some(ev),
                None => None,
            },
            value,
        ));
        acc_val += value;
        for (var, values_dict) in map_result.marginal_dict {
            for (assign, the_value) in values_dict {
                let prev_value = marginal_dict[&var][&assign];
                marginal_dict
                    .get_mut(&var)
                    .unwrap()
                    .insert(assign, prev_value + weight * the_value);
            }
        }
    }
    let (max_ev, max_value) = results
        .into_iter()
        .max_by(|x, y| partial_ordering(x.1, y.1))
        .unwrap();
    HybridMaxProductResult {
        evidence: max_ev,
        max_product_value: Some(max_value),
        sum_product_value: Some(acc_val),
        marginal_dict,
    }
}
