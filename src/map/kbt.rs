use super::time_checker::TimeChecker;
use crate::evidence::Evidence;
use crate::map::map_scenario::*;
use crate::spn::node::Node;
use crate::utils::f64::partial_ordering;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::f64;
use std::sync::mpsc::channel;
use std::{thread, time};

#[derive(Debug, Clone)]
struct KBTReturn {
    pub evidence: Evidence,
    pub value: f64,
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct KBTReturnPair {
    pub kbt_return_1: KBTReturn,
    pub index_1: usize,
    pub kbt_return_2: KBTReturn,
    pub index_2: usize,
}

struct KBTProductHeap {
    heap: BinaryHeap<KBTReturnPair>,
    kbts_list_1: Vec<KBTReturn>,
    kbts_list_2: Vec<KBTReturn>,
}

impl KBTReturnPair {
    pub fn evidence(&self) -> Evidence {
        self.kbt_return_1
            .evidence
            .merge(&self.kbt_return_2.evidence)
    }

    pub fn value(&self) -> f64 {
        self.kbt_return_1.value * self.kbt_return_2.value
    }
}

impl Ord for KBTReturnPair {
    fn cmp(&self, other: &Self) -> Ordering {
        self.value().partial_cmp(&other.value()).unwrap()
    }
}

impl PartialOrd for KBTReturnPair {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.value().partial_cmp(&other.value())
    }
}

impl PartialEq for KBTReturn {
    fn eq(&self, other: &Self) -> bool {
        self.evidence == other.evidence && self.value == other.value
    }
}

impl Eq for KBTReturn {}

impl Ord for KBTReturn {
    fn cmp(&self, other: &Self) -> Ordering {
        self.value.partial_cmp(&other.value).unwrap()
    }
}

impl PartialOrd for KBTReturn {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.value.partial_cmp(&other.value)
    }
}

impl KBTProductHeap {
    pub fn new(kbts_list_1: Vec<KBTReturn>, kbts_list_2: Vec<KBTReturn>) -> Self {
        let mut heap = BinaryHeap::new();
        let best_from_list_1 = kbts_list_1.first().unwrap();
        let best_from_list_2 = kbts_list_2.first().unwrap();
        heap.push(KBTReturnPair {
            kbt_return_1: best_from_list_1.clone(),
            index_1: 0,
            kbt_return_2: best_from_list_2.clone(),
            index_2: 0,
        });
        KBTProductHeap {
            heap,
            kbts_list_1,
            kbts_list_2,
        }
    }

    pub fn pop(&mut self) -> KBTReturn {
        let current_solution = self.heap.pop().unwrap();
        let first_from_current_solution = &current_solution.kbt_return_1;
        let second_from_current_solution = &current_solution.kbt_return_2;
        let index_of_first = current_solution.index_1;
        let index_of_second = current_solution.index_2;
        let next_index_first = index_of_first + 1;
        let next_index_second = index_of_second + 1;
        let next_of_first_option = self.kbts_list_1.get(next_index_first);
        let next_of_second_option = self.kbts_list_2.get(next_index_second);
        if let Some(next_of_first) = next_of_first_option {
            let first_next_to_push = KBTReturnPair {
                kbt_return_1: next_of_first.clone(),
                index_1: next_index_first,
                kbt_return_2: second_from_current_solution.clone(),
                index_2: index_of_second,
            };
            self.heap.push(first_next_to_push);
        }
        if let Some(next_of_second) = next_of_second_option {
            let second_next_to_push = KBTReturnPair {
                kbt_return_1: first_from_current_solution.clone(),
                index_1: index_of_first,
                kbt_return_2: next_of_second.clone(),
                index_2: next_index_second,
            };
            self.heap.push(second_next_to_push);
        }
        KBTReturn {
            evidence: current_solution.evidence(),
            value: current_solution.value(),
        }
    }
}

pub fn kbt_wrapper(
    map_scenario: &MapScenario,
    k: usize,
    time_limit_opt: Option<u64>,
) -> Option<Evidence> {
    match time_limit_opt {
        Some(time_limit) => {
            let (sender, receiver) = channel();
            thread::spawn(move || {
                thread::sleep(time::Duration::from_secs(time_limit));
                sender.send(true).is_ok()
            });
            let time_checker = TimeChecker::new(receiver);
            let best_trees_opt = kbt(
                map_scenario,
                map_scenario.spn.root_id,
                k,
                &mut Some(time_checker),
            );
            match best_trees_opt {
                Some(best_trees) => {
                    let spn = &map_scenario.spn;
                    let mut evidences_and_values: Vec<(Evidence, f64)> = best_trees
                        .into_iter()
                        .map(|(evidence, _)| {
                            let value = spn.value(&evidence);
                            (evidence, value)
                        })
                        .collect();
                    Some(evidences_and_values.remove(0).0)
                }
                None => None,
            }
        }
        None => {
            let best_trees = kbt(map_scenario, map_scenario.spn.root_id, k, &mut None).unwrap();
            let spn = &map_scenario.spn;
            let mut evidences_and_values: Vec<(Evidence, f64)> = best_trees
                .into_iter()
                .map(|(evidence, _)| {
                    let value = spn.value(&evidence);
                    (evidence, value)
                })
                .collect();
            Some(evidences_and_values.remove(0).0)
        }
    }
}

fn best_k(items: impl Iterator<Item = (Evidence, f64)>, k: usize) -> Vec<(Evidence, f64)> {
    let mut result_vec = Vec::with_capacity(k);
    for (ev, value) in items {
        if result_vec.len() >= k {
            if let Some(index) = result_vec
                .iter()
                .position(|(_, vec_value)| value > *vec_value)
            {
                result_vec[index] = (ev, value);
            }
        } else {
            result_vec.push((ev, value));
        }
    }
    result_vec.sort_by(|(_, current_value), (_, next_value)| {
        partial_ordering(*current_value, *next_value).reverse()
    });
    result_vec
}

fn kbt(
    map_scenario: &MapScenario,
    node_id: usize,
    k: usize,
    time_checker_opt: &mut Option<TimeChecker>,
) -> Option<Vec<(Evidence, f64)>> {
    if let Some(time_checker) = time_checker_opt {
        if time_checker.check() {
            return None;
        }
    }
    match &map_scenario.spn.nodes[node_id] {
        Node::Sum(child_ids, weights) => {
            kbt_sum(&map_scenario, child_ids, weights, k, time_checker_opt)
        }
        Node::Product(child_ids) => kbt_product(&map_scenario, child_ids, k, time_checker_opt),
        Node::Indicator(var_id, assign) => Some(kbt_indicator(&map_scenario, *var_id, *assign)),
        _ => panic![],
    }
}

fn kbt_sum(
    map_scenario: &MapScenario,
    child_ids: &[usize],
    weights: &[f64],
    k: usize,
    time_checker_opt: &mut Option<TimeChecker>,
) -> Option<Vec<(Evidence, f64)>> {
    let mut best_trees = Vec::with_capacity(child_ids.len());
    for (child_id, weight) in child_ids.iter().zip(weights) {
        let child_kbt_result_opt = kbt(map_scenario, *child_id, k, time_checker_opt);
        if let Some(child_kbt_result) = child_kbt_result_opt {
            best_trees.push(
                child_kbt_result
                    .into_iter()
                    .map(move |(evidence, val)| (evidence, weight * val))
                    .collect::<Vec<(Evidence, f64)>>(),
            );
        } else {
            return None;
        }
    }
    Some(best_k(best_trees.into_iter().flatten(), k))
}

fn kbt_product(
    map_scenario: &MapScenario,
    child_ids: &[usize],
    k: usize,
    time_checker_opt: &mut Option<TimeChecker>,
) -> Option<Vec<(Evidence, f64)>> {
    let mut child_kbts = Vec::with_capacity(child_ids.len());
    for child_id in child_ids.iter() {
        if let Some(child_kbt) = kbt(map_scenario, *child_id, k, time_checker_opt) {
            child_kbts.push(
                child_kbt
                    .into_iter()
                    .map(|(evidence, value)| KBTReturn { evidence, value })
                    .collect::<Vec<KBTReturn>>(),
            );
        } else {
            return None;
        }
    }
    let mut first_child_kbt = child_kbts.pop().unwrap();
    while let Some(next_child_kbt) = child_kbts.pop() {
        let mut kbt_product_heap = KBTProductHeap::new(first_child_kbt.clone(), next_child_kbt);
        first_child_kbt = (0..k)
            .into_iter()
            .map(|_| kbt_product_heap.pop())
            .collect::<Vec<KBTReturn>>();
    }
    Some(
        first_child_kbt
            .into_iter()
            .map(|kbt_return| (kbt_return.evidence, kbt_return.value))
            .collect(),
    )
}

fn kbt_indicator(map_scenario: &MapScenario, var_id: usize, assign: usize) -> Vec<(Evidence, f64)> {
    let mut evidence = Evidence::default();
    if let Some(marginalized) = &map_scenario.marginalized {
        if let Some(variable) = marginalized.iter().find(|var| var.id == var_id) {
            evidence.insert(var_id, (0..variable.n_categories).collect());
            return vec![(evidence, 1f64)];
        }
    }
    if let Some(scenario_evidence) = &map_scenario.evidence {
        if scenario_evidence.has(var_id) {
            let ev_values = &scenario_evidence[&var_id];
            evidence.insert(var_id, ev_values.clone());
            let value = if ev_values.contains(&assign) {
                1f64
            } else {
                0f64
            };
            return vec![(evidence, value)];
        }
    }
    evidence.insert(var_id, vec![assign]);
    vec![(evidence, 1f64)]
}
