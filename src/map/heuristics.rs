use crate::evidence::Evidence;
use crate::spn::SPN;
use crate::variable::Variable;
use ordered_float::OrderedFloat;
use std::collections::HashMap;

pub fn first_variable(
    _: &SPN,
    partial_evidences: &Evidence,
    marginalized_opt: &Option<Vec<Variable>>,
    _: &Option<HashMap<usize, Vec<f64>>>,
) -> Option<(usize, Vec<usize>)> {
    partial_evidences
        .iter()
        .find(|(var_id, values)| {
            let has_more_than_one_value = values.len() > 1;
            if let Some(marginal_vars) = marginalized_opt {
                marginal_vars
                    .iter()
                    .find(|var| var.id == **var_id)
                    .is_none()
                    && has_more_than_one_value
            } else {
                has_more_than_one_value
            }
        })
        .map(|(var_id_ref, values_ref)| (*var_id_ref, values_ref.clone()))
}

pub fn lowest_marginal(
    spn: &SPN,
    partial_evidences: &Evidence,
    marginalized_opt: &Option<Vec<Variable>>,
    derivatives_opt: &Option<HashMap<usize, Vec<f64>>>,
) -> Option<(usize, Vec<usize>)> {
    let derivative_of_assignment = match derivatives_opt {
        Some(ders) => ders.clone(),
        None => spn.derivative_of_assignment(partial_evidences),
    };
    let blocked_ids: Vec<usize> = match marginalized_opt {
        Some(marginal_vars) => marginal_vars.iter().map(|var| var.id).collect(),
        None => vec![],
    };
    let mut chosen_var_and_values = None;
    let mut lowest_value = std::f64::INFINITY;
    for (variable, values) in partial_evidences.iter() {
        if values.len() <= 1 || blocked_ids.contains(variable) {
            continue;
        }
        let sum_of_marginals = derivative_of_assignment[variable]
            .iter()
            .map(|number| OrderedFloat::<f64>(*number))
            .min()
            .unwrap()
            .into_inner();
        if sum_of_marginals < lowest_value {
            chosen_var_and_values = Some((*variable, values.clone()));
            lowest_value = sum_of_marginals;
        }
    }
    chosen_var_and_values
}

pub fn largest_marginal(
    spn: &SPN,
    partial_evidences: &Evidence,
    marginalized_opt: &Option<Vec<Variable>>,
    derivatives_opt: &Option<HashMap<usize, Vec<f64>>>,
) -> Option<(usize, Vec<usize>)> {
    let derivative_of_assignment = match derivatives_opt {
        Some(ders) => ders.clone(),
        None => spn.derivative_of_assignment(partial_evidences),
    };
    let blocked_ids: Vec<usize> = match marginalized_opt {
        Some(marginal_vars) => marginal_vars.iter().map(|var| var.id).collect(),
        None => vec![],
    };
    let mut chosen_var_and_values = None;
    let mut largest_value = -std::f64::INFINITY;
    for (variable, values) in partial_evidences.iter() {
        if values.len() <= 1 || blocked_ids.contains(variable) {
            continue;
        }
        let sum_of_marginals = derivative_of_assignment[variable]
            .iter()
            .map(|number| OrderedFloat::<f64>(*number))
            .max()
            .unwrap()
            .into_inner();
        if sum_of_marginals > largest_value {
            chosen_var_and_values = Some((*variable, values.clone()));
            largest_value = sum_of_marginals;
        }
    }
    chosen_var_and_values
}

pub fn lowest_entropy(
    spn: &SPN,
    partial_evidences: &Evidence,
    marginalized_opt: &Option<Vec<Variable>>,
    derivatives_opt: &Option<HashMap<usize, Vec<f64>>>,
) -> Option<(usize, Vec<usize>)> {
    let derivative_of_assignment = match derivatives_opt {
        Some(ders) => ders.clone(),
        None => spn.derivative_of_assignment(partial_evidences),
    };
    let blocked_ids: Vec<usize> = match marginalized_opt {
        Some(marginal_vars) => marginal_vars.iter().map(|var| var.id).collect(),
        None => vec![],
    };
    let mut chosen_var_and_values = None;
    let mut lowest_value = std::f64::INFINITY;
    for (variable, values) in partial_evidences.iter() {
        if values.len() <= 1 || blocked_ids.contains(variable) {
            continue;
        }
        let log_base = values.len();
        let entropy = -derivative_of_assignment[variable]
            .iter()
            .map(|value| value * value.log(log_base as f64))
            .sum::<f64>();
        if entropy < lowest_value {
            chosen_var_and_values = Some((*variable, values.clone()));
            lowest_value = entropy;
        }
    }
    chosen_var_and_values
}

pub fn largest_entropy(
    spn: &SPN,
    partial_evidences: &Evidence,
    marginalized_opt: &Option<Vec<Variable>>,
    derivatives_opt: &Option<HashMap<usize, Vec<f64>>>,
) -> Option<(usize, Vec<usize>)> {
    let derivative_of_assignment = match derivatives_opt {
        Some(ders) => ders.clone(),
        None => spn.derivative_of_assignment(partial_evidences),
    };
    let blocked_ids: Vec<usize> = match marginalized_opt {
        Some(marginal_vars) => marginal_vars.iter().map(|var| var.id).collect(),
        None => vec![],
    };
    let mut chosen_var_and_values = None;
    let mut largest_value = -std::f64::INFINITY;
    for (variable, values) in partial_evidences.iter() {
        if values.len() <= 1 || blocked_ids.contains(variable) {
            continue;
        }
        let log_base = values.len();
        let entropy = -derivative_of_assignment[variable]
            .iter()
            .map(|value| value * value.log(log_base as f64))
            .sum::<f64>();
        if entropy > largest_value {
            chosen_var_and_values = Some((*variable, values.clone()));
            largest_value = entropy;
        }
    }
    chosen_var_and_values
}

pub fn ordering(
    spn: &SPN,
    partial_evidences: &Evidence,
    marginalized_opt: &Option<Vec<Variable>>,
    derivatives_opt: &Option<HashMap<usize, Vec<f64>>>,
) -> Option<(usize, Vec<usize>)> {
    let mut vars_and_values: Vec<(usize, Vec<usize>)> = partial_evidences
        .iter()
        .map(|(variable_ref, values_ref)| (*variable_ref, values_ref.clone()))
        .collect();
    vars_and_values.sort_by_key(|(_, values)| values.len());

    vars_and_values
        .into_iter()
        .find(|(var_id, values)| {
            let has_more_than_one_value = values.len() > 1;
            if let Some(marginal_vars) = marginalized_opt {
                marginal_vars.iter().find(|var| var.id == *var_id).is_none()
                    && has_more_than_one_value
            } else {
                has_more_than_one_value
            }
        })
        .map(|(chosen_variable, mut values_for_chosen_variable)| {
            let derivatives = match derivatives_opt {
                Some(ders) => ders.clone(),
                None => spn.derivative_of_assignment(partial_evidences),
            };
            values_for_chosen_variable
                .sort_by_key(|value| OrderedFloat::<f64>(derivatives[&chosen_variable][*value]));
            (chosen_variable, values_for_chosen_variable)
        })
}
