use super::time_checker::TimeChecker;
use crate::evidence::Evidence;
use crate::map::map_scenario::*;
use crate::variable::Variable;
use std::cmp::min;
use std::sync::mpsc::channel;
use std::{thread, time};

pub fn beam_search_wrapper(
    map_scenario: &MapScenario,
    beam_size: usize,
    time_limit_opt: Option<u64>,
) -> Option<Evidence> {
    match time_limit_opt {
        Some(time_limit) => {
            let (sender, receiver) = channel();
            thread::spawn(move || {
                thread::sleep(time::Duration::from_secs(time_limit));
                sender.send(true).is_ok()
            });
            let time_checker = TimeChecker::new(receiver);
            beam_search(map_scenario, beam_size, &mut Some(time_checker))
        }
        None => beam_search(map_scenario, beam_size, &mut None),
    }
}

fn beam_search(
    map_scenario: &MapScenario,
    beam_size: usize,
    time_checker_opt: &mut Option<TimeChecker>,
) -> Option<Evidence> {
    let mut initial_evidence = match &map_scenario.evidence {
        Some(evidence) => evidence.clone(),
        None => Evidence::default(),
    };
    let query_vars = &map_scenario.query;
    if let Some(marginal_vars) = &map_scenario.marginalized {
        for marginal_var in marginal_vars {
            initial_evidence.insert(marginal_var.id, (0..marginal_var.n_categories).collect());
        }
    }
    for query_var in query_vars.iter() {
        initial_evidence.insert(query_var.id, (0..query_var.n_categories).collect());
    }
    let initial_value = map_scenario.spn.value(&initial_evidence);
    let mut partial_evidences = vec![(initial_evidence, initial_value)];
    loop {
        if let Some(time_checker) = time_checker_opt {
            if time_checker.check() {
                return None;
            }
        }
        let all_partial_evidences_have_query_values_set =
            partial_evidences.iter().all(|(partial_evidence, _)| {
                query_vars
                    .iter()
                    .all(|query_var| partial_evidence[&query_var.id].len() == 1)
            });
        if all_partial_evidences_have_query_values_set {
            break;
        }
        let mut new_partial_evidences = partial_evidences
            .into_iter()
            .map(|(partial_evidence, value)| {
                let vars_to_search: Vec<Variable> = query_vars
                    .clone()
                    .into_iter()
                    .filter(|var| partial_evidence[&var.id].len() > 1)
                    .collect();
                if vars_to_search.is_empty() {
                    vec![(partial_evidence, value)]
                } else {
                    let derivatives = map_scenario.spn.derivative_of_assignment(&partial_evidence);
                    vars_to_search
                        .into_iter()
                        .map(|query_var| {
                            let query_var_id = query_var.id;
                            let query_var_categories = query_var.n_categories;
                            derivatives
                                .get(&query_var_id)
                                .unwrap()
                                .iter()
                                .zip(0..query_var_categories)
                                .map(|(derivative_value, var_value)| {
                                    let mut new_evidence = partial_evidence.clone();
                                    new_evidence.insert(query_var_id, vec![var_value]);
                                    (new_evidence, *derivative_value)
                                })
                                .collect::<Vec<(Evidence, f64)>>()
                        })
                        .flatten()
                        .collect()
                }
            })
            .flatten()
            .collect::<Vec<(Evidence, f64)>>();
        new_partial_evidences.sort_by(|(_, v1), (_, v2)| v2.partial_cmp(&v1).unwrap());
        new_partial_evidences.dedup();
        partial_evidences =
            new_partial_evidences[0..min(beam_size, new_partial_evidences.len())].to_vec();
    }

    Some(
        partial_evidences
            .into_iter()
            .max_by(|(_, v1), (_, v2)| v1.partial_cmp(&v2).unwrap())
            .unwrap()
            .0,
    )
}
