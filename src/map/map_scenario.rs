use crate::evidence::Evidence;
use crate::spn::SPN;
use crate::variable::Variable;
use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufRead, BufReader};
use std::sync::Arc;

#[derive(Clone, Debug, PartialEq)]
pub struct MapScenario {
    pub spn: Arc<SPN>,
    pub query: Vec<Variable>,
    pub marginalized: Option<Vec<Variable>>,
    pub evidence: Option<Evidence>,
}

impl fmt::Display for MapScenario {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut string = "q".to_string();
        for var_id in &self.query {
            string.push_str(&format![" {:?}", var_id]);
        }
        if let Some(marginal_vars) = self.marginalized.as_ref() {
            string.push_str("\nm");
            for var_id in marginal_vars {
                string.push_str(&format![" {:?}", var_id]);
            }
        }
        if let Some(evidence_obj) = self.evidence.as_ref() {
            string.push_str("\ne ");
            string.push_str(&format!["{}", evidence_obj]);
        }
        write!(f, "{}", string)
    }
}

impl MapScenario {
    pub fn new(
        spn: Arc<SPN>,
        query: Vec<Variable>,
        marginalized: Option<Vec<Variable>>,
        evidence: Option<Evidence>,
    ) -> Self {
        MapScenario {
            spn,
            query,
            marginalized,
            evidence,
        }
    }

    pub fn is_valid(&self) -> bool {
        for variable_ref in self.spn.variables.iter() {
            if !self.query.contains(variable_ref) {
                match &self.marginalized {
                    Some(marginal_vars) => {
                        if !marginal_vars.contains(variable_ref) {
                            match &self.evidence {
                                Some(ev) => {
                                    if !ev.has(variable_ref.id) {
                                        return false;
                                    }
                                }
                                None => {
                                    return false;
                                }
                            }
                        }
                    }
                    None => match &self.evidence {
                        Some(ev) => {
                            if !ev.has(variable_ref.id) {
                                return false;
                            }
                        }
                        None => {
                            return false;
                        }
                    },
                }
            }
        }
        for variable_ref in self.query.iter() {
            if !self.spn.variables.contains(variable_ref) {
                return false;
            }
        }
        if let Some(marginal_vars) = &self.marginalized {
            for variable_ref in marginal_vars.iter() {
                if !self.spn.variables.contains(variable_ref) {
                    return false;
                }
            }
        }
        if let Some(evidence) = &self.evidence {
            let var_ids: Vec<usize> = self.spn.variables.iter().map(|var| var.id).collect();
            for var_id in evidence.keys() {
                if !var_ids.contains(var_id) {
                    return false;
                }
            }
        }
        true
    }
}

pub fn map_scenarios_from_file(filepath: &str, spn: Arc<SPN>) -> std::io::Result<Vec<MapScenario>> {
    let file = File::open(filepath)?;
    let mut buf_reader = BufReader::new(file);
    let mut line = String::new();
    let mut scenarios = vec![];
    let mut scenario = None;

    loop {
        if buf_reader.read_line(&mut line).is_err() {
            break;
        }
        let trimmed_line = line.trim();
        if trimmed_line.is_empty() {
            break;
        }
        if trimmed_line.starts_with('q') {
            let query = trimmed_line
                .split(' ')
                .skip(1)
                .map(|var_id| spn.variables[var_id.parse::<usize>().unwrap()].clone())
                .collect();
            if let Some(complete_scenario) = scenario {
                scenarios.push(complete_scenario);
            }
            scenario = Some(MapScenario::new(Arc::clone(&spn), query, None, None));
        } else if trimmed_line.starts_with('m') {
            let marginalized = trimmed_line
                .split(' ')
                .skip(1)
                .map(|var_id| spn.variables[var_id.parse::<usize>().unwrap()].clone())
                .collect();
            if let Some(current_scenario) = scenario.as_mut() {
                current_scenario.marginalized = Some(marginalized);
            }
        } else if trimmed_line.starts_with('e') {
            let mut varset = HashMap::default();
            let mut splitted_line = trimmed_line.split(' ').skip(1);
            while let Some(var_id_str) = splitted_line.next() {
                let value = splitted_line.next().unwrap().parse::<usize>().unwrap();
                let var_id = var_id_str.parse::<usize>().unwrap();
                varset.insert(var_id, vec![value]);
            }
            let evidence = Evidence::new(varset);
            if let Some(current_scenario) = scenario.as_mut() {
                current_scenario.evidence = Some(evidence);
            }
        }
        line.clear();
    }
    if let Some(last_scenario) = scenario {
        scenarios.push(last_scenario)
    }

    Ok(scenarios)
}

pub fn map_scenarios_to_file(filepath: &str, map_scenarios: &[MapScenario]) -> std::io::Result<()> {
    let mut file = File::create(filepath)?;
    for map_scenario in map_scenarios {
        let query_var_ids_str = map_scenario
            .query
            .iter()
            .map(|var| format!["{}", var.id])
            .collect::<Vec<String>>();
        let marginalized_var_ids_str = map_scenario
            .marginalized
            .as_ref()
            .unwrap()
            .iter()
            .map(|var| format!["{}", var.id])
            .collect::<Vec<String>>();
        let evidence_str = map_scenario
            .evidence
            .as_ref()
            .unwrap()
            .iter()
            .map(|(var_id, values)| format!["{} {}", var_id, values.first().unwrap()])
            .collect::<Vec<String>>();
        file.write_fmt(format_args!["q {}\n", query_var_ids_str.join(" ")])?;
        file.write_fmt(format_args!["m {}\n", marginalized_var_ids_str.join(" ")])?;
        file.write_fmt(format_args!["e {}\n", evidence_str.join(" ")])?;
    }
    Ok(())
}
