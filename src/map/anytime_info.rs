use crate::evidence::Evidence;
use ordered_float::OrderedFloat;
use std::cmp::Ordering;
use std::time::{Duration, Instant};

#[derive(PartialEq, Eq, Clone)]
pub struct LowerBoundInfo {
    pub evidence: Evidence,
    pub value: OrderedFloat<f64>,
    pub value_ln: OrderedFloat<f64>,
    pub elapsed: Duration,
}

impl LowerBoundInfo {
    pub fn new(value: f64, ev: &Evidence, time: Duration) -> Self {
        LowerBoundInfo {
            evidence: ev.clone(),
            value: OrderedFloat::<f64>(value),
            value_ln: OrderedFloat::<f64>(value.ln()),
            elapsed: time,
        }
    }
}

pub struct AnytimeInfo {
    pub starting_time: Instant,
    pub lowers: Vec<LowerBoundInfo>,
    pub finished: bool,
    best: f64,
}

impl PartialOrd for LowerBoundInfo {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.value_ln.cmp(&other.value_ln))
    }
}

impl Ord for LowerBoundInfo {
    fn cmp(&self, other: &Self) -> Ordering {
        self.value_ln.cmp(&other.value_ln)
    }
}

impl AnytimeInfo {
    pub fn new_lower_bound(&mut self, value: f64, evidence: &Evidence) {
        if value > self.best {
            self.lowers.push(LowerBoundInfo::new(
                value,
                evidence,
                self.starting_time.elapsed(),
            ));
            self.best = value;
        }
    }

    pub fn finish(&mut self) {
        self.finished = true;
    }

    pub fn best_evidence(&self) -> Option<&Evidence> {
        self.lowers.last().map(|bound| &bound.evidence)
    }

    pub fn chart_order(&self) -> Vec<(f64, f64, Duration)> {
        let mut result = self.lowers.clone();
        result.sort_by(|a, b| a.elapsed.partial_cmp(&b.elapsed).unwrap());
        let mut last_result = self.lowers.last().unwrap().clone();
        last_result.elapsed = self.starting_time.elapsed();
        result.push(last_result);
        result
            .into_iter()
            .map(|lower_bound_info| {
                (
                    lower_bound_info.value.into_inner(),
                    lower_bound_info.value_ln.into_inner(),
                    lower_bound_info.elapsed,
                )
            })
            .collect()
    }
}

impl Default for AnytimeInfo {
    fn default() -> Self {
        AnytimeInfo {
            starting_time: Instant::now(),
            lowers: vec![],
            finished: false,
            best: -std::f64::INFINITY,
        }
    }
}
