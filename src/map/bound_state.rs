use super::hybrid_max_product::hybrid_max_product;
use crate::evidence::Evidence;
use crate::spn::SPN;
use std::collections::HashMap;
use std::fmt;

pub type MarginalDict = HashMap<usize, HashMap<usize, f64>>;

pub struct BoundStateInfo {
    pub bound_state: BoundState,
    pub marginal_dict: MarginalDict,
}

#[derive(Clone, Debug)]
pub struct BoundState {
    pub lower: f64,
    pub upper: f64,
    pub evidence: Evidence,
}

impl fmt::Display for BoundState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write![f, "{} - {} - {}", self.lower, self.upper, self.evidence]
    }
}

impl BoundState {
    pub fn default(spn: &SPN, node_id: usize, evidence: &Evidence) -> BoundStateInfo {
        let hybrid_max_product_result = hybrid_max_product(spn, node_id, evidence);
        let lower_bound_value = match &hybrid_max_product_result.evidence {
            Some(ev) => spn.value(ev),
            None => 0f64,
        };
        let upper_bound_value = match hybrid_max_product_result.sum_product_value {
            Some(val) => val,
            None => 0f64,
        };
        let (result_evidence, marginal_dict) = hybrid_max_product_result.extract();
        BoundStateInfo {
            bound_state: BoundState {
                lower: lower_bound_value,
                upper: upper_bound_value,
                evidence: result_evidence.unwrap(),
            },
            marginal_dict,
        }
    }
}
