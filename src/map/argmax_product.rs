use super::map_scenario::MapScenario;
use super::time_checker::TimeChecker;
use crate::evidence::Evidence;
use crate::spn::node::Node;
use crate::spn::SPN;
use crate::variable::Variable;
use std::collections::HashMap;
use std::sync::mpsc::channel;
use std::{thread, time};

pub fn argmax_product_wrapper(
    map_scenario: &MapScenario,
    time_limit_opt: Option<u64>,
) -> Option<Evidence> {
    let spn = &map_scenario.spn;
    match time_limit_opt {
        Some(time_limit) => {
            let (sender, receiver) = channel();
            thread::spawn(move || {
                thread::sleep(time::Duration::from_secs(time_limit));
                sender.send(true).is_ok()
            });
            let time_checker = TimeChecker::new(receiver);
            argmax_product(
                spn,
                &map_scenario.query,
                &map_scenario.marginalized,
                &map_scenario.evidence,
                spn.root_id,
                &mut Some(time_checker),
            )
            .map(|(ev, _)| ev)
        }
        None => argmax_product(
            spn,
            &map_scenario.query,
            &map_scenario.marginalized,
            &map_scenario.evidence,
            spn.root_id,
            &mut None,
        )
        .map(|(ev, _)| ev),
    }
}

fn argmax_product(
    spn: &SPN,
    query: &[Variable],
    marginalized: &Option<Vec<Variable>>,
    evidence: &Option<Evidence>,
    node_id: usize,
    time_checker_opt: &mut Option<TimeChecker>,
) -> Option<(Evidence, f64)> {
    if let Some(time_checker) = time_checker_opt {
        if time_checker.check() {
            return None;
        }
    }
    match &spn.nodes[node_id] {
        Node::Sum(child_ids, _) => {
            let mut child_results = Vec::with_capacity(child_ids.len());
            for child_id in child_ids.iter() {
                match argmax_product(
                    spn,
                    query,
                    marginalized,
                    evidence,
                    *child_id,
                    time_checker_opt,
                ) {
                    Some((ev, _)) => {
                        let value = spn.value_subspn(&ev, node_id);
                        child_results.push((ev, value));
                    }
                    None => {
                        return None;
                    }
                }
            }
            Some(
                child_results
                    .into_iter()
                    .max_by(|(_, value_current), (_, value_to_compare)| {
                        value_current.partial_cmp(&value_to_compare).unwrap()
                    })
                    .unwrap(),
            )
        }
        Node::Product(child_ids) => {
            let mut new_evidence = Evidence::default();
            let mut total_value = 1f64;
            for child_id in child_ids {
                match argmax_product(
                    spn,
                    query,
                    marginalized,
                    evidence,
                    *child_id,
                    time_checker_opt,
                ) {
                    Some((ev, value)) => {
                        new_evidence.merge_mut(&ev);
                        total_value *= value;
                    }
                    None => {
                        return None;
                    }
                }
            }
            Some((new_evidence, total_value))
        }
        Node::Indicator(var_id, assign) => {
            let mut varset = HashMap::default();
            if let Some(ev) = evidence {
                if ev.has(*var_id) {
                    return Some((ev.clone(), spn.value_subspn(&ev, node_id)));
                }
            }
            if let Some(marginal_vars) = marginalized {
                if let Some(variable) = marginal_vars
                    .iter()
                    .find(|marginal_var| marginal_var.id == *var_id)
                {
                    let values = (0..variable.n_categories).collect();
                    varset.insert(*var_id, values);
                    return Some((Evidence::new(varset), 1f64));
                }
            }
            varset.insert(*var_id, vec![*assign]);
            Some((Evidence::new(varset), 1f64))
        }
        _ => panic!["Argmax Product run on unknown Node type"],
    }
}
