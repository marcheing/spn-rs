use super::anytime_info::AnytimeInfo;
use super::heuristics::ordering;
use super::map_scenario::MapScenario;
use super::time_checker::TimeChecker;
use crate::evidence::Evidence;
use crate::spn::SPN;
use crate::variable::Variable;
use std::collections::HashMap;
use std::sync::mpsc::channel;
use std::{thread, time};

pub type CheckingFunction = dyn Fn(
    &SPN,
    Evidence,
    f64,
    &Option<Vec<Variable>>,
) -> (Evidence, Option<HashMap<usize, Vec<f64>>>);

pub type BranchingHeuristic = dyn Fn(
    &SPN,
    &Evidence,
    &Option<Vec<Variable>>,
    &Option<HashMap<usize, Vec<f64>>>,
) -> Option<(usize, Vec<usize>)>;

pub fn max_search_with_marginal_checking(
    map_scenario: &MapScenario,
    time_limit_opt: Option<u64>,
    initial_evidence_opt: Option<Evidence>,
) -> Evidence {
    max_search(
        map_scenario,
        &marginal_checking,
        initial_evidence_opt,
        time_limit_opt,
    )
}

pub fn max_search_with_forward_checking(
    map_scenario: &MapScenario,
    time_limit_opt: Option<u64>,
    initial_evidence_opt: Option<Evidence>,
) -> Evidence {
    max_search(
        map_scenario,
        &forward_checking,
        initial_evidence_opt,
        time_limit_opt,
    )
}

pub fn max_search(
    map_scenario: &MapScenario,
    checking_function: &CheckingFunction,
    initial_evidence_opt: Option<Evidence>,
    time_limit_opt: Option<u64>,
) -> Evidence {
    let mut anytime_info = AnytimeInfo::default();
    let spn = &map_scenario.spn;
    let evidence_option = &map_scenario.evidence;
    let marginalized_option = &map_scenario.marginalized;
    let mut partial_evidences = spn.complete_evidence();

    let mut initial_evidence = match initial_evidence_opt {
        Some(ev) => ev,
        None => {
            let mut ev = Evidence::default();
            for variable in &spn.variables {
                ev.insert(variable.id, vec![0]);
            }
            ev
        }
    };
    if let Some(evidence) = evidence_option {
        for (var_id, values) in evidence.iter() {
            partial_evidences[var_id] = values.clone();
            initial_evidence[var_id] = values.clone();
        }
    }
    if let Some(marginalized) = marginalized_option {
        for var in marginalized {
            initial_evidence[&var.id] = (0..var.n_categories).collect();
        }
    }
    let initial_value = spn.value(&initial_evidence);
    anytime_info.new_lower_bound(initial_value, &initial_evidence);
    let mut anytime_info_param = Some(anytime_info);
    match time_limit_opt {
        Some(time_limit) => {
            let (sender, receiver) = channel();
            thread::spawn(move || {
                thread::sleep(time::Duration::from_secs(time_limit));
                sender.send(true).is_ok()
            });
            let time_checker = TimeChecker::new(receiver);
            search(
                spn,
                &partial_evidences,
                initial_evidence,
                initial_value,
                checking_function,
                &mut anytime_info_param,
                marginalized_option,
                &mut Some(time_checker),
                &ordering,
                &None,
            );
        }
        None => {
            search(
                spn,
                &partial_evidences,
                initial_evidence,
                initial_value,
                checking_function,
                &mut anytime_info_param,
                marginalized_option,
                &mut None,
                &ordering,
                &None,
            );
        }
    }
    anytime_info_param.unwrap().best_evidence().unwrap().clone()
}

pub fn max_search_for_chart(
    map_scenario: &MapScenario,
    checking_function: &CheckingFunction,
    time_limit_opt: Option<u64>,
    branching_heuristic: &BranchingHeuristic,
) -> AnytimeInfo {
    let mut anytime_info = AnytimeInfo::default();
    let spn = &map_scenario.spn;
    let evidence_option = &map_scenario.evidence;
    let marginalized_option = &map_scenario.marginalized;
    let mut partial_evidences = spn.complete_evidence();

    let mut initial_evidence = Evidence::default();
    for variable in &spn.variables {
        initial_evidence.insert(variable.id, vec![0]);
    }
    if let Some(evidence) = evidence_option {
        for (var_id, values) in evidence.iter() {
            partial_evidences[var_id] = values.clone();
            initial_evidence[var_id] = values.clone();
        }
    }
    if let Some(marginalized) = marginalized_option {
        for var in marginalized {
            initial_evidence[&var.id] = (0..var.n_categories).collect();
        }
    }
    let initial_value = spn.value(&initial_evidence);
    anytime_info.new_lower_bound(initial_value, &initial_evidence);
    let mut anytime_info_param = Some(anytime_info);
    match time_limit_opt {
        Some(time_limit) => {
            let (sender, receiver) = channel();
            thread::spawn(move || {
                thread::sleep(time::Duration::from_secs(time_limit));
                sender.send(true).is_ok()
            });
            let time_checker = TimeChecker::new(receiver);
            search(
                spn,
                &partial_evidences,
                initial_evidence,
                initial_value,
                checking_function,
                &mut anytime_info_param,
                marginalized_option,
                &mut Some(time_checker),
                branching_heuristic,
                &None,
            );
        }
        None => {
            search(
                spn,
                &partial_evidences,
                initial_evidence,
                initial_value,
                checking_function,
                &mut anytime_info_param,
                marginalized_option,
                &mut None,
                branching_heuristic,
                &None,
            );
        }
    }
    anytime_info_param.unwrap()
}

fn search(
    spn: &SPN,
    partial_evidences: &Evidence,
    mut best_evidence: Evidence,
    mut best_value: f64,
    checking_function: &CheckingFunction,
    anytime_info_option: &mut Option<AnytimeInfo>,
    marginalized_variables: &Option<Vec<Variable>>,
    maybe_checker: &mut Option<TimeChecker>,
    branching_heuristic: &BranchingHeuristic,
    derivatives_opt: &Option<HashMap<usize, Vec<f64>>>,
) -> Evidence {
    if let Some(checker) = maybe_checker {
        if checker.check() {
            if let Some(anytime_info) = anytime_info_option {
                anytime_info.new_lower_bound(best_value, &best_evidence);
            }
            return best_evidence;
        }
    }
    if let Some(marginal_vars) = marginalized_variables {
        if spn
            .scope()
            .iter()
            .filter(|var_id| !marginal_vars.iter().any(|var| var.id == **var_id))
            .all(|var_id| partial_evidences[var_id].len() == 1)
        {
            return partial_evidences.clone();
        }
    }
    if partial_evidences.all_set() {
        return partial_evidences.clone();
    }
    let branching_result = branching_heuristic(
        spn,
        partial_evidences,
        marginalized_variables,
        derivatives_opt,
    );
    if branching_result.is_none() {
        //println!["This shouldn't happen. Branching result returned none. Here are the evidences: {:?}\nHere are the marginals: {:?}", partial_evidences, marginalized_variables];
        return best_evidence;
    }
    let (chosen_variable, chosen_values) = branching_result.unwrap();
    for possible_value in chosen_values {
        if maybe_checker
            .as_mut()
            .map_or(false, |checker| checker.check())
        {
            break;
        }
        let mut evidences_to_consider = partial_evidences.clone();
        evidences_to_consider.insert(chosen_variable, vec![possible_value]);
        let (pruned_evidences, new_derivatives_opt) = checking_function(
            spn,
            evidences_to_consider,
            best_value,
            marginalized_variables,
        );
        if maybe_checker
            .as_mut()
            .map_or(false, |checker| checker.check())
        {
            break;
        }
        if !pruned_evidences.is_empty() {
            let returned_evidence = search(
                spn,
                &pruned_evidences,
                best_evidence.clone(),
                best_value,
                checking_function,
                anytime_info_option,
                marginalized_variables,
                maybe_checker,
                branching_heuristic,
                &new_derivatives_opt,
            );
            if maybe_checker
                .as_mut()
                .map_or(false, |checker| checker.check())
            {
                break;
            }
            if returned_evidence != best_evidence {
                best_evidence = returned_evidence.clone();
                best_value = spn.value(&best_evidence);
                if let Some(anytime_info) = anytime_info_option {
                    anytime_info.new_lower_bound(best_value, &best_evidence);
                }
            }
        }
    }
    best_evidence
}

pub fn marginal_checking(
    spn: &SPN,
    partial_evidences: Evidence,
    best_value: f64,
    _: &Option<Vec<Variable>>,
) -> (Evidence, Option<HashMap<usize, Vec<f64>>>) {
    if spn.value(&partial_evidences) > best_value {
        (partial_evidences, None)
    } else {
        (Evidence::default(), None)
    }
}

pub fn is_close(x1: f64, x2: f64) -> bool {
    (x1 - x2).abs() < std::f64::EPSILON
}

pub fn forward_checking(
    spn: &SPN,
    mut partial_evidences: Evidence,
    best_value: f64,
    marginalized_variables_option: &Option<Vec<Variable>>,
) -> (Evidence, Option<HashMap<usize, Vec<f64>>>) {
    let mut evidences_changed = true;
    let mut derivatives = None;
    while evidences_changed {
        evidences_changed = false;
        derivatives = Some(spn.derivative_of_assignment(&partial_evidences));
        let mut new_evidences = partial_evidences.clone();
        for (var_id, possible_assignments) in partial_evidences.iter() {
            if let Some(marginalized_variables) = marginalized_variables_option {
                if marginalized_variables
                    .iter()
                    .any(|marginal_var| marginal_var.id == *var_id)
                {
                    continue;
                }
            }
            for assignment in possible_assignments {
                let derivative_value = derivatives.as_ref().unwrap()[var_id][*assignment];
                if !is_close(best_value, derivative_value) && best_value > derivative_value {
                    if new_evidences[var_id].len() == 1 {
                        return (Evidence::default(), derivatives);
                    }
                    let value_to_remove_index =
                        new_evidences[var_id].binary_search(assignment).unwrap();
                    new_evidences[var_id].remove(value_to_remove_index);
                    evidences_changed = true;
                }
            }
        }
        partial_evidences = new_evidences;
    }
    (partial_evidences, derivatives)
}
