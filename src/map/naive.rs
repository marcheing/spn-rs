use crate::evidence::Evidence;
use crate::spn::SPN;
use crate::utils::anytime::*;
use std::collections::HashMap;
use std::f64;

enum PermutationStatus {
    NOTFINISHED,
    FINISHED,
}

struct PermutationGenerator {
    all_values: Vec<Vec<usize>>,
    indexes: Vec<usize>,
    sizes: Vec<usize>,
    status: PermutationStatus,
}

impl PermutationGenerator {
    fn new(values: Vec<Vec<usize>>) -> Self {
        let indexes = vec![0; values.len()];
        let sizes = values.iter().map(|vec| vec.len()).collect();
        PermutationGenerator {
            all_values: values,
            indexes,
            sizes,
            status: PermutationStatus::NOTFINISHED,
        }
    }

    fn increment(&mut self) {
        let mut current_index = 0;
        let mut current_value;
        while current_index < self.indexes.len() {
            current_value = self.indexes[current_index];
            if current_value + 1 == self.sizes[current_index] {
                self.indexes[current_index] = 0;
                current_index += 1;
                continue;
            } else {
                self.indexes[current_index] += 1;
                return;
            }
        }
        self.status = PermutationStatus::FINISHED;
    }

    fn current(&self) -> Vec<usize> {
        self.indexes
            .iter()
            .zip(self.all_values.iter())
            .map(|(index, values)| values[*index])
            .collect()
    }
}

impl Iterator for PermutationGenerator {
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.status {
            PermutationStatus::FINISHED => None,
            PermutationStatus::NOTFINISHED => {
                let item = self.current();
                self.increment();
                Some(item)
            }
        }
    }
}

pub fn naive(spn: &SPN) -> Evidence {
    let total: usize = spn
        .complete_evidence()
        .values()
        .map(|values_vec| values_vec.len())
        .product();
    let mut current = 1;
    let variable_values = spn.variables.iter().map(|var| var.all_values()).collect();
    let permutation_generator = PermutationGenerator::new(variable_values);
    let mut best_value = -f64::INFINITY;
    let mut best_evidence = Evidence::default();
    for permutation in permutation_generator {
        let mut varset = HashMap::default();
        for var in &spn.variables {
            let mut values = Vec::default();
            values.push(permutation[var.id]);
            varset.insert(var.id, values);
        }
        let evidence = Evidence::new(varset);
        let value = spn.value(&evidence);
        if value > best_value {
            best_value = value;
            best_evidence = evidence;
        }
        current += 1;
        println!["{:?}/{:?}", current, total];
    }
    best_evidence
}

pub fn anytime_naive(spn: &SPN, mut anytime_env: AnytimeEnv) {
    let variable_values = spn.variables.iter().map(|var| var.all_values()).collect();
    let permutation_generator = PermutationGenerator::new(variable_values);
    let mut best_value = -f64::INFINITY;
    let mut best_evidence = Evidence::default();
    anytime_env.update_best_result((best_evidence, best_value, 1f64));
    let mut total_value = 0f64;
    for permutation in permutation_generator {
        if anytime_env.check_timeout() {
            return;
        }
        let mut varset = HashMap::default();
        for var in &spn.variables {
            let mut values = Vec::default();
            values.push(permutation[var.id]);
            varset.insert(var.id, values);
        }
        let evidence = Evidence::new(varset);
        let value = spn.value(&evidence);
        total_value += value;
        assert![total_value - 1f64 <= std::f64::EPSILON];
        if value > best_value {
            best_value = value;
            best_evidence = evidence;
            anytime_env.update_best_result((best_evidence, best_value, 1f64));
        }
    }
    println!("Finished naive");
}
