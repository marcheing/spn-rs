use super::anytime_info::AnytimeInfo;
use super::map_scenario::MapScenario;
use super::time_checker::TimeChecker;
use crate::evidence::Evidence;
use crate::spn::SPN;
use crate::variable::Variable;
use std::sync::mpsc::channel;
use std::{thread, time};

pub fn bare_local_search_wrapper(map_scenario: &MapScenario) -> Evidence {
    local_search_wrapper(map_scenario, None, None)
}

pub fn local_search_wrapper(
    map_scenario: &MapScenario,
    initial_evidence: Option<Evidence>,
    time_limit_opt: Option<u64>,
) -> Evidence {
    assert![map_scenario.is_valid()];
    let evidence = match initial_evidence {
        Some(ev) => ev,
        None => {
            let mut evidence = match &map_scenario.evidence {
                Some(ev) => ev.clone(),
                None => Evidence::default(),
            };
            if let Some(marginalized) = &map_scenario.marginalized {
                for variable in marginalized {
                    evidence.insert(variable.id, (0..variable.n_categories).collect());
                }
            }
            for variable in &map_scenario.query {
                evidence.insert(variable.id, vec![0]);
            }
            evidence
        }
    };
    match time_limit_opt {
        Some(time_limit) => {
            let (sender, receiver) = channel();
            thread::spawn(move || {
                thread::sleep(time::Duration::from_secs(time_limit));
                sender.send(true).is_ok()
            });
            let time_checker = TimeChecker::new(receiver);
            local_search(
                &map_scenario.spn,
                evidence,
                &map_scenario.query,
                &mut Some(time_checker),
            )
            .best_evidence()
            .unwrap()
            .clone()
        }
        None => local_search(&map_scenario.spn, evidence, &map_scenario.query, &mut None)
            .best_evidence()
            .unwrap()
            .clone(),
    }
}

pub fn local_search(
    spn: &SPN,
    evidence: Evidence,
    query: &[Variable],
    time_checker_opt: &mut Option<TimeChecker>,
) -> AnytimeInfo {
    let mut best_value = spn.value(&evidence);
    let mut anytime_info = AnytimeInfo::default();
    anytime_info.new_lower_bound(best_value, &evidence);
    let mut changed = true;
    let mut best_evidence = evidence;

    while changed {
        if let Some(time_checker) = time_checker_opt {
            if time_checker.check() {
                break;
            }
        }
        changed = false;
        for variable in query {
            for value in 0..variable.n_categories {
                let mut new_evidence = best_evidence.clone();
                new_evidence.insert(variable.id, vec![value]);
                let new_value = spn.value(&new_evidence);
                if new_value > best_value {
                    changed = true;
                    best_value = new_value;
                    best_evidence = new_evidence;
                    anytime_info.new_lower_bound(best_value, &best_evidence);
                }
            }
        }
    }
    anytime_info.new_lower_bound(best_value, &best_evidence);
    anytime_info.finish();
    anytime_info
}
