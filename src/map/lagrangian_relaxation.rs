use super::bound_state::BoundState;
use crate::evidence::Evidence;
use crate::spn::node::Node;
use crate::spn::SPN;
use std::collections::HashMap;
use std::f64;

type LambdaMap = HashMap<usize, HashMap<usize, f64>>;

const DEFAULT_ITERATIONS: usize = 1;

pub struct LRDiag<'a, 'b, 'c> {
    spn: &'a SPN,
    node_id: usize,
    evidences: &'b Evidence,
    pub bound_state: BoundState,
    values: Vec<f64>,
    max_iterations: usize,
    lambdas: Vec<&'c LambdaMap>,
}

impl<'a, 'b, 'c> LRDiag<'a, 'b, 'c> {
    pub fn new(
        spn: &'a SPN,
        node_id: usize,
        evidences: &'b Evidence,
        bound_state: Option<BoundState>,
        max_iterations: Option<usize>,
        lambdas: Option<Vec<&'c LambdaMap>>,
    ) -> Self {
        let new_bound_state = match bound_state {
            Some(state) => state,
            None => BoundState::default(spn, node_id, evidences).bound_state,
        };
        let new_max_iterations = match max_iterations {
            Some(n) => n,
            None => DEFAULT_ITERATIONS,
        };
        let new_lambdas = match lambdas {
            Some(lambs) => lambs,
            None => vec![],
        };
        LRDiag {
            spn,
            node_id,
            evidences,
            bound_state: new_bound_state,
            values: vec![],
            max_iterations: new_max_iterations,
            lambdas: new_lambdas,
        }
    }

    pub fn lagrangian_relaxation(&mut self) -> Evidence {
        let scope = self.spn.scope_subspn(self.node_id);
        if scope.len() == 1 {
            return self.lagrangian_relaxation_scope_one();
        }
        match &self.spn.nodes[self.node_id] {
            Node::Product(children_ids) => self.lagrangian_relaxation_product(children_ids),
            Node::Sum(children_ids, weights) => self.dual_decomp(children_ids, weights),
            _ => panic![
                "Error trying to call Lagrangian Relaxation on an Indicator node without scope 1: {:?}", scope
            ],
        }
    }

    fn lagrangian_relaxation_scope_one(&mut self) -> Evidence {
        let variable_id = *self.spn.scope_subspn(self.node_id).iter().next().unwrap();
        let mut best_value = -f64::INFINITY;
        let mut best_evidence = Evidence::default();
        for value in &self.evidences[&variable_id] {
            let lambda_sum: f64 = self
                .lambdas
                .iter()
                .map(|lamb| lamb[&variable_id][value])
                .sum();
            let mut new_varset = HashMap::default();
            let mut new_set = Vec::default();
            new_set.push(*value);
            new_varset.insert(variable_id, new_set);
            let ev = Evidence::new(new_varset);
            let new_value = self.spn.value(&ev) * (-lambda_sum).exp();
            if new_value > best_value {
                best_value = new_value;
                best_evidence = ev;
            }
        }
        best_evidence
    }

    fn lagrangian_relaxation_product(&mut self, children_ids: &[usize]) -> Evidence {
        let mut final_evidence = Evidence::default();
        for child_id in children_ids {
            let child_scope = self.spn.scope_subspn(*child_id);
            let mut lambdas = vec![];
            let mut new_evidence = Evidence::default();
            for lamb in &self.lambdas {
                for var_id in lamb.keys() {
                    if child_scope.contains(var_id) {
                        lambdas.push(*lamb);
                    }
                }
            }
            for var_id in child_scope {
                new_evidence.insert_set(*var_id, self.evidences[&var_id].clone());
            }
            let subevidence = LRDiag::new(
                self.spn,
                *child_id,
                &new_evidence,
                None,
                Some(self.max_iterations),
                Some(lambdas),
            )
            .lagrangian_relaxation();
            final_evidence.merge_mut(&subevidence);
        }
        final_evidence
    }

    fn dual_decomp(&mut self, children_ids: &[usize], weights: &[f64]) -> Evidence {
        let mut iteration = 0;
        let mut lamb = get_vector_in_dict_form(&self.spn, self.node_id, &self.evidences);

        loop {
            iteration += 1;

            let child_maximizations: Vec<(usize, Evidence)> = children_ids
                .iter()
                .map(|child_id| {
                    let mut new_lambs = self.lambdas.clone();
                    new_lambs.push(&lamb[child_id]);
                    let mut lr_diag = LRDiag::new(
                        self.spn,
                        *child_id,
                        self.evidences,
                        None,
                        Some(self.max_iterations),
                        Some(new_lambs),
                    );
                    (*child_id, lr_diag.lagrangian_relaxation())
                })
                .collect();
            let child_evidences_list = child_maximizations.iter().map(|(_, evidence)| evidence);

            let (evidence_maximization, max_value) =
                self.lambda_maximization(weights, &child_maximizations, &lamb);
            let maybe_new_lower = self.spn.value(&evidence_maximization);

            if maybe_new_lower > self.bound_state.lower {
                self.bound_state.evidence = evidence_maximization.clone();
                self.bound_state.lower = maybe_new_lower;
            }

            let mut all_evidences = vec![&evidence_maximization];
            for ev in child_evidences_list {
                all_evidences.push(&ev);
            }

            if self.node_id == self.spn.root_id {
                if max_value < self.bound_state.upper {
                    self.bound_state.upper = max_value;
                }
                self.values.push(max_value);
            }

            if iteration >= self.max_iterations {
                return self.bound_state.evidence.clone();
            }

            let first_evidence = all_evidences[0];

            if all_evidences.iter().all(|ev| *ev == first_evidence) {
                if self.node_id == self.spn.root_id {
                    self.bound_state.evidence = evidence_maximization.clone();
                    self.bound_state.lower = self.spn.value(&self.bound_state.evidence);
                }
                return evidence_maximization;
            }

            let mut grad = get_vector_in_dict_form(&self.spn, self.node_id, &self.evidences);
            let mut norm_factor = 0f64;

            for var_id in self.evidences.keys() {
                for (child_id, current_child_evidence) in child_maximizations.iter() {
                    let assignment_child = current_child_evidence[var_id].get(0).unwrap();
                    let assignment_individual = evidence_maximization[var_id].get(0).unwrap();

                    if assignment_child == assignment_individual {
                        continue;
                    }

                    (*grad
                        .get_mut(&child_id)
                        .unwrap()
                        .get_mut(var_id)
                        .unwrap()
                        .get_mut(&assignment_child)
                        .unwrap()) = -max_value;
                    (*grad
                        .get_mut(&child_id)
                        .unwrap()
                        .get_mut(var_id)
                        .unwrap()
                        .get_mut(&assignment_individual)
                        .unwrap()) = max_value;

                    norm_factor += 2f64 * max_value.powi(2);
                }
            }

            let rate =
                2f64 * ((self.bound_state.upper * 1.05) - self.bound_state.lower) / norm_factor;

            for var_id in self.evidences.keys() {
                for (child_id, current_child_evidence) in &child_maximizations {
                    let assignment_child = current_child_evidence[var_id].get(0).unwrap();
                    let assignment_individual = evidence_maximization[var_id].get(0).unwrap();

                    *lamb
                        .get_mut(&child_id)
                        .unwrap()
                        .get_mut(var_id)
                        .unwrap()
                        .get_mut(&assignment_child)
                        .unwrap() -= rate * grad[&child_id][var_id][&assignment_child];
                    *lamb
                        .get_mut(&child_id)
                        .unwrap()
                        .get_mut(var_id)
                        .unwrap()
                        .get_mut(&assignment_individual)
                        .unwrap() -= rate * grad[&child_id][var_id][&assignment_individual];
                }
            }
        }
    }

    fn lambda_maximization(
        &mut self,
        weights: &[f64],
        child_maximizations: &[(usize, Evidence)],
        lamb: &HashMap<usize, LambdaMap>,
    ) -> (Evidence, f64) {
        let mut maybe_best_evidence = None;
        let mut best_value = -f64::INFINITY;
        for (_, subevidence) in child_maximizations {
            let mut total = 0f64;
            for ((child_id, child_evidence), weight) in
                child_maximizations.iter().zip(weights.iter())
            {
                let mut sum_of_lambdas = 0f64;
                for var_id in self.evidences.keys() {
                    sum_of_lambdas += lamb[child_id][var_id][&subevidence[var_id].get(0).unwrap()];
                    sum_of_lambdas -=
                        lamb[child_id][var_id][&child_evidence[var_id].get(0).unwrap()];
                }
                total += weight
                    * sum_of_lambdas.exp()
                    * self.spn.value_subspn(&child_evidence, *child_id);
            }
            if total > best_value {
                maybe_best_evidence = Some(subevidence.clone());
                best_value = total;
            }
        }
        if let Some(best_evidence) = maybe_best_evidence {
            (best_evidence, best_value)
        } else {
            panic!["Lambda Maximization didn't find a best evidence"];
        }
    }
}

fn get_vector_in_dict_form(
    spn: &SPN,
    node_id: usize,
    evidence: &Evidence,
) -> HashMap<usize, LambdaMap> {
    let mut map = HashMap::default();
    let empty_vec = vec![];
    let children_ids = match &spn.nodes[node_id] {
        Node::Indicator(_, _) => &empty_vec,
        Node::Sum(children_ids, _) => children_ids,
        Node::Product(children_ids) => children_ids,
        _ => panic![],
    };
    for child_id in children_ids {
        map.insert(*child_id, HashMap::default());
        for var_id in evidence.keys() {
            (*map.get_mut(child_id).unwrap()).insert(*var_id, HashMap::default());
            for value in &evidence[var_id] {
                (*map.get_mut(child_id).unwrap().get_mut(var_id).unwrap()).insert(*value, 0f64);
            }
        }
    }
    map
}

pub fn lagrangian_relaxation_wrapper(spn: &SPN) -> Evidence {
    LRDiag::new(spn, spn.root_id, &spn.complete_evidence(), None, None, None)
        .lagrangian_relaxation()
}
