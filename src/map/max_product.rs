use super::map_scenario::MapScenario;
use super::time_checker::TimeChecker;
use crate::evidence::Evidence;
use crate::spn::node::Node;
use crate::spn::SPN;
use crate::variable::Variable;
use std::collections::HashMap;
use std::sync::mpsc::channel;
use std::{thread, time};

pub fn max_product_wrapper(
    map_scenario: &MapScenario,
    time_limit_opt: Option<u64>,
) -> Option<Evidence> {
    let mut time_checker = match time_limit_opt {
        Some(time_limit) => {
            let (sender, receiver) = channel();
            thread::spawn(move || {
                thread::sleep(time::Duration::from_secs(time_limit));
                sender.send(true).is_ok()
            });
            Some(TimeChecker::new(receiver))
        }
        None => None,
    };
    max_product(
        &map_scenario.spn,
        &map_scenario.query,
        &map_scenario.marginalized,
        &map_scenario.evidence,
        map_scenario.spn.root_id,
        &mut time_checker,
    )
    .map(|(ev, _)| ev)
}

pub fn max_product(
    spn: &SPN,
    query: &[Variable],
    marginalized: &Option<Vec<Variable>>,
    evidence: &Option<Evidence>,
    node_id: usize,
    time_checker: &mut Option<TimeChecker>,
) -> Option<(Evidence, f64)> {
    let root = &spn.nodes[node_id];
    match root {
        Node::Indicator(var_id, assign) => {
            let mut varset = HashMap::default();
            if let Some(ev) = evidence {
                if ev.has(*var_id) {
                    return Some((ev.clone(), spn.value_subspn(&ev, node_id)));
                }
            }
            if let Some(marginal_vars) = marginalized {
                if let Some(variable) = marginal_vars
                    .iter()
                    .find(|marginal_var| marginal_var.id == *var_id)
                {
                    let values = (0..variable.n_categories).collect();
                    varset.insert(*var_id, values);
                    return Some((Evidence::new(varset), 1f64));
                }
            }
            varset.insert(*var_id, vec![*assign]);
            Some((Evidence::new(varset), 1f64))
        }
        Node::Product(children_ids) => {
            let mut new_evidence = Evidence::default();
            let mut total_value = 1f64;
            for child_id in children_ids {
                match max_product(spn, query, marginalized, evidence, *child_id, time_checker) {
                    Some((ev, value)) => {
                        new_evidence.merge_mut(&ev);
                        total_value *= value;
                    }
                    None => {
                        return None;
                    }
                }
            }
            Some((new_evidence, total_value))
        }
        Node::Sum(children_ids, weights) => {
            let mut child_max_products = Vec::with_capacity(children_ids.len());
            for child_id in children_ids {
                match max_product(spn, query, marginalized, evidence, *child_id, time_checker) {
                    Some(map_result) => {
                        child_max_products.push(map_result);
                    }
                    None => {
                        return None;
                    }
                }
            }
            Some(
                child_max_products
                    .into_iter()
                    .zip(weights)
                    .map(|((map_result, value), weight)| (map_result, value * weight))
                    .max_by(|x, y| x.1.partial_cmp(&y.1).unwrap())
                    .unwrap(),
            )
        }
        _ => panic![],
    }
}
