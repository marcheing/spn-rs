#[derive(PartialEq, Eq, Hash, Clone, Debug, Default)]
pub struct Variable {
    pub id: usize,
    pub n_categories: usize,
}

impl Variable {
    pub fn new(id: usize, n_categories: usize) -> Self {
        Variable { id, n_categories }
    }

    pub fn all_values(&self) -> Vec<usize> {
        (0..self.n_categories).collect()
    }
}
