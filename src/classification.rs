use crate::evidence::Evidence;
use crate::spn::SPN;

pub fn classification_test(spn: &SPN, test_dataset: &[Vec<usize>]) -> usize {
    let mut hits = 0;
    let variable_for_classification = spn.variables.last().unwrap();
    for data_instance in test_dataset {
        let data_instance_correct_value = data_instance[variable_for_classification.id];
        let spn_chosen_value = (0..variable_for_classification.n_categories)
            .map(|chosen_category| {
                let mut new_data_instance = data_instance.clone();
                *new_data_instance.last_mut().unwrap() = chosen_category;
                let mut evidence = Evidence::default();
                for (data_index, data_value) in new_data_instance.iter().enumerate() {
                    evidence.insert(data_index, vec![*data_value]);
                }
                (chosen_category, spn.value(&evidence))
            })
            .max_by(|x, y| x.1.partial_cmp(&y.1).unwrap())
            .unwrap()
            .0;
        if data_instance_correct_value == spn_chosen_value {
            hits += 1;
        }
    }

    hits
}
