use crate::spn::node::Node;
use crate::spn::SPN;
use crate::variable::Variable;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

const BUFF_SZ: usize = 1000;

fn resize_vec_if_needed<T>(vec: &mut Vec<T>, required_index: usize) {
    let capacity = vec.capacity();
    if capacity < required_index {
        vec.reserve(required_index - capacity);
    }
}

pub fn to_file(path: &Path, spn: &SPN) -> std::io::Result<()> {
    let mut file = File::create(path)?;
    for (node_index, node) in spn.nodes.iter().enumerate() {
        match node {
            Node::Indicator(var_id, assignment) => {
                file.write_fmt(format_args![
                    "indicator {} {} {}\n",
                    node_index, var_id, assignment
                ])?;
            }
            Node::Product(child_ids) => {
                file.write_fmt(format_args!["* {}", node_index])?;
                for child_id in child_ids {
                    file.write_fmt(format_args![" {}", child_id])?;
                }
                file.write_all(b"\n")?;
            }
            Node::Sum(child_ids, weights) => {
                file.write_fmt(format_args!["+ {}", node_index])?;
                for (child_id, weight) in child_ids.iter().zip(weights.iter()) {
                    file.write_fmt(format_args![" {} {}", child_id, weight])?;
                }
                file.write_all(b"\n")?;
            }
            _ => panic![],
        }
    }
    Ok(())
}

pub fn from_file(path: &Path) -> std::io::Result<SPN> {
    let mut file = File::open(path)?;
    let mut file_string = String::new();
    file.read_to_string(&mut file_string)?;

    let lines = file_string.split('\n');

    let mut variables = Vec::with_capacity(BUFF_SZ);
    let mut root_id = 0;
    let mut node_vec: Vec<Node> = Vec::with_capacity(BUFF_SZ);
    let mut var_id_to_vec_index: HashMap<usize, usize> = HashMap::default();
    let mut node_id_to_vec_index: HashMap<usize, usize> = HashMap::default();
    for line in lines {
        if line.is_empty() {
            continue;
        }
        let line_elems: Vec<&str> = line.trim().split(' ').collect();
        let node_type: &str = line_elems[0];
        let the_id: usize = match line_elems[1].parse::<i64>() {
            Ok(number) => number as usize,
            Err(e) => panic!("{}", e),
        };
        resize_vec_if_needed(&mut node_vec, the_id);
        node_vec.push(match node_type {
            "l" | "indicator" => {
                let var_id = line_elems[2].parse::<i64>().unwrap() as usize;
                let assignment = line_elems[3].parse::<i64>().unwrap() as usize;
                match var_id_to_vec_index.get(&var_id) {
                    Some(var_index) => {
                        let variable: &mut Variable = variables.get_mut(*var_index).unwrap();
                        if assignment + 1 > variable.n_categories {
                            variable.n_categories += 1;
                        }
                    }
                    None => {
                        resize_vec_if_needed(&mut variables, var_id);
                        variables.push(Variable::new(var_id, assignment + 1));
                        let index = variables.len() - 1;
                        var_id_to_vec_index.insert(var_id, index);
                    }
                };
                Node::Indicator(var_id, assignment)
            }
            "+" => {
                let iterator = line_elems.iter().skip(2);
                let children = iterator
                    .clone()
                    .step_by(2)
                    .map(|x| x.parse::<i64>().unwrap() as usize)
                    .map(|node_id| node_id_to_vec_index[&node_id])
                    .collect();
                let weights = iterator
                    .skip(1)
                    .step_by(2)
                    .map(|x| x.parse::<f64>().unwrap())
                    .collect();
                Node::Sum(children, weights)
            }
            "*" => {
                let iterator = line_elems.iter().skip(2);
                let mut children = Vec::default();
                for node_id in iterator {
                    children
                        .push(node_id_to_vec_index[&(node_id.parse::<i64>().unwrap() as usize)]);
                }
                Node::Product(children)
            }
            _ => panic!("Unknown node type {}", node_type),
        });
        let node_index = node_vec.len() - 1;
        node_id_to_vec_index.insert(the_id, node_index);
        root_id = the_id;
    }
    variables.sort_by_key(|variable| variable.id);
    Ok(SPN::new(
        node_vec,
        variables,
        node_id_to_vec_index[&root_id],
    ))
}
