use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt;

use super::node::Node;
use crate::evidence::Evidence;
use crate::mis::MIS;
use crate::variable::Variable;

#[derive(Debug, Clone, PartialEq)]
pub struct SPN {
    pub nodes: Vec<Node>,
    pub variables: Vec<Variable>,
    pub root_id: usize,
    scopes: Vec<HashSet<usize>>,
}

fn fix_scopes(nodes: &[Node]) -> Vec<HashSet<usize>> {
    let mut scopes: Vec<HashSet<usize>> = Vec::with_capacity(nodes.len());
    scopes.resize(nodes.len(), HashSet::default());
    for (node_id, node) in nodes.iter().enumerate() {
        match node {
            Node::Indicator(var_id, _) => {
                scopes[node_id] = [*var_id].iter().cloned().collect();
            }
            Node::Product(child_ids) => {
                let mut set: HashSet<usize> = HashSet::default();
                let subscopes = child_ids.iter().map(|child_id| scopes[*child_id].clone());
                for subscope in subscopes {
                    set.extend(&subscope);
                }
                scopes[node_id] = set;
            }
            Node::Sum(child_ids, _) => {
                scopes[node_id] = scopes[*child_ids.first().unwrap()].clone();
            }
            Node::Constant(_) => scopes[node_id] = HashSet::default(),
        }
    }
    scopes
}

impl SPN {
    pub fn new(nodes: Vec<Node>, variables: Vec<Variable>, root_id: usize) -> Self {
        let scopes = fix_scopes(&nodes);
        SPN {
            nodes,
            variables,
            root_id,
            scopes,
        }
    }

    pub fn normalize(&mut self) {
        for mut node in &mut self.nodes {
            if let Node::Sum(_, weights) = &mut node {
                let weight_sum: f64 = weights.iter().sum();
                for weight in weights.iter_mut() {
                    *weight /= weight_sum;
                }
            }
        }
    }

    pub fn sum_node_ids(&self) -> Vec<usize> {
        let mut sum_node_ids = vec![];
        for node_id in self.topological_order() {
            if let Node::Sum(_, _) = self.nodes[node_id] {
                sum_node_ids.push(node_id);
            }
        }
        sum_node_ids
    }

    pub fn sum_nodes(&self) -> Vec<(usize, (Vec<usize>, Vec<f64>))> {
        let mut sum_nodes = vec![];
        for node_id in self.topological_order() {
            if let Node::Sum(child_ids, weights) = &self.nodes[node_id] {
                sum_nodes.push((node_id, (child_ids.clone(), weights.clone())));
            }
        }
        sum_nodes
    }

    pub fn locally_normalize(&mut self) {
        let mut alphas: HashMap<usize, f64> = HashMap::default();
        let mut parents: HashMap<usize, Vec<usize>> = HashMap::default();
        let topological_order = self.topological_order();
        for node_id in topological_order.iter() {
            if let Node::Sum(child_ids, _) = &self.nodes[*node_id] {
                for child_id in child_ids {
                    if parents.contains_key(&child_id) {
                        parents.get_mut(&child_id).unwrap().push(*node_id);
                    } else {
                        parents.insert(*child_id, vec![*node_id]);
                    }
                }
            } else if let Node::Product(child_ids) = &self.nodes[*node_id] {
                alphas.insert(*node_id, 1f64);
                for child_id in child_ids {
                    if parents.contains_key(&child_id) {
                        parents.get_mut(&child_id).unwrap().push(*node_id);
                    } else {
                        parents.insert(*child_id, vec![*node_id]);
                    }
                }
            }
        }
        for node_id in topological_order {
            let alpha = if let Node::Sum(_, weights) = &mut self.nodes[node_id] {
                let alpha: f64 = weights.iter().sum();
                for weight in weights.iter_mut() {
                    *weight /= alpha
                }
                alpha
            } else if let Node::Product(_) = self.nodes[node_id] {
                let alpha = alphas[&node_id];
                alphas.insert(node_id, 1f64);
                alpha
            } else {
                1f64
            };
            if parents.contains_key(&node_id) {
                for parent_id in &parents[&node_id] {
                    if let Node::Sum(parent_child_ids, parent_weights) = &mut self.nodes[*parent_id]
                    {
                        for (parent_child_id, parent_weight) in
                            parent_child_ids.iter().zip(parent_weights.iter_mut())
                        {
                            if *parent_child_id == node_id {
                                *parent_weight *= alpha;
                            }
                        }
                    } else if let Node::Product(_) = self.nodes[*parent_id] {
                        *alphas.get_mut(&parent_id).unwrap() *= alpha;
                    }
                }
            }
        }
    }

    pub fn weight_projection(&mut self, smoothing: f64) {
        let all_marginalized_values = self.log_eval(&self.all_marginalized());
        for node_id in self.topological_order() {
            if let Node::Sum(child_ids, weights) = &mut self.nodes[node_id] {
                let max_logp = child_ids
                    .iter()
                    .map(|child_id| all_marginalized_values[*child_id])
                    .fold(f64::NAN, f64::max);
                let ssz: f64 = child_ids
                    .iter()
                    .zip(weights.iter())
                    .map(|(child_id, weight)| {
                        weight * (all_marginalized_values[*child_id] - max_logp).exp() + smoothing
                    })
                    .sum();

                for (child_id, weight) in child_ids.iter().zip(weights.iter_mut()) {
                    let sz =
                        *weight * (all_marginalized_values[*child_id] - max_logp).exp() + smoothing;
                    *weight = sz / ssz;
                }
            }
        }
    }

    pub fn all_marginalized(&self) -> Evidence {
        let mut evidence = Evidence::default();
        for var in &self.variables {
            evidence.insert(var.id, (0..var.n_categories).collect());
        }
        evidence
    }

    pub fn dist_nodes(&self) -> impl Iterator<Item = usize> + '_ {
        self.nodes
            .iter()
            .enumerate()
            .filter(move |(node_id, node)| match node {
                Node::Sum(_, _) => self.scope_subspn(*node_id).len() == 1,
                _ => false,
            })
            .map(|(node_id, _)| node_id)
    }

    pub fn minimize(self) -> SPN {
        let mut new_nodes = vec![];
        let mut old_index_to_new_index: HashMap<usize, usize> = HashMap::default();
        let mut indicator_to_indicator_index: HashMap<(usize, usize), usize> = HashMap::default();
        for (index, node) in self.nodes.into_iter().enumerate() {
            match node {
                Node::Indicator(var_id, assignment) => {
                    match indicator_to_indicator_index.get(&(var_id, assignment)) {
                        Some(indicator_index) => {
                            old_index_to_new_index.insert(index, *indicator_index);
                        }
                        None => {
                            let indicator_index = new_nodes.len();
                            indicator_to_indicator_index
                                .insert((var_id, assignment), indicator_index);
                            old_index_to_new_index.insert(index, indicator_index);
                            new_nodes.push(Node::Indicator(var_id, assignment));
                        }
                    };
                }
                Node::Sum(children, weights) => {
                    let new_children = children
                        .into_iter()
                        .map(|old_index| old_index_to_new_index[&old_index])
                        .collect();
                    old_index_to_new_index.insert(index, new_nodes.len());
                    new_nodes.push(Node::Sum(new_children, weights));
                }
                Node::Product(children) => {
                    let new_children = children
                        .into_iter()
                        .map(|old_index| old_index_to_new_index[&old_index])
                        .collect();
                    old_index_to_new_index.insert(index, new_nodes.len());
                    new_nodes.push(Node::Product(new_children));
                }
                _ => panic![],
            }
        }
        SPN::new(
            new_nodes,
            self.variables,
            old_index_to_new_index[&self.root_id],
        )
    }

    pub fn root(&self) -> &Node {
        &self.nodes[self.root_id]
    }

    fn edges_recursive(&self, current_node_id: usize, edges: &mut usize) {
        match &self.nodes[current_node_id] {
            Node::Product(child_ids) => {
                *edges += child_ids.len();
                for child_id_ref in child_ids {
                    self.edges_recursive(*child_id_ref, edges);
                }
            }
            Node::Sum(child_ids, _) => {
                *edges += child_ids.len();
                for child_id_ref in child_ids {
                    self.edges_recursive(*child_id_ref, edges);
                }
            }
            _ => {}
        }
    }

    pub fn edges(&self) -> usize {
        self.edges_subspn(self.root_id)
    }

    pub fn edges_subspn(&self, node_id: usize) -> usize {
        let mut edges = 0;
        self.edges_recursive(node_id, &mut edges);
        edges
    }

    fn counts_recursive(&self, current_node_id: usize, counts: &mut HashMap<&str, usize>) {
        match &self.nodes[current_node_id] {
            Node::Product(child_ids) => {
                *counts.get_mut("product").unwrap() += 1;
                for child_id_ref in child_ids {
                    self.counts_recursive(*child_id_ref, counts);
                }
            }
            Node::Sum(child_ids, _) => {
                *counts.get_mut("sum").unwrap() += 1;
                for child_id_ref in child_ids {
                    self.counts_recursive(*child_id_ref, counts);
                }
            }
            Node::Indicator(_, _) => {
                *counts.get_mut("indicator").unwrap() += 1;
            }
            _ => {}
        }
    }

    pub fn counts(&self) -> HashMap<&str, usize> {
        self.counts_subspn(self.root_id)
    }

    pub fn counts_subspn(&self, node_id: usize) -> HashMap<&str, usize> {
        let mut counts: HashMap<&str, usize> = HashMap::default();
        counts.insert("sum", 0);
        counts.insert("product", 0);
        counts.insert("indicator", 0);
        self.counts_recursive(node_id, &mut counts);
        counts
    }

    fn height_recursive(
        &self,
        current_node_id: usize,
        current_height: usize,
        heights: &mut Vec<usize>,
    ) {
        heights[current_node_id] = current_height;
        match &self.nodes[current_node_id] {
            Node::Product(child_ids) => {
                for child_id_ref in child_ids {
                    self.height_recursive(*child_id_ref, current_height + 1, heights);
                }
            }
            Node::Sum(child_ids, _) => {
                for child_id_ref in child_ids {
                    self.height_recursive(*child_id_ref, current_height + 1, heights);
                }
            }
            _ => {}
        }
    }

    pub fn heights(&self) -> Vec<usize> {
        let mut heights = vec![0; self.nodes.len()];
        self.height_recursive(self.root_id, 1, &mut heights);
        let max_height = *heights.iter().max().unwrap();
        for height_ref in heights.iter_mut() {
            *height_ref = max_height - (*height_ref - 1);
        }
        heights
    }

    pub fn height(&self) -> usize {
        self.height_subspn(self.root_id)
    }

    pub fn height_subspn(&self, node_id: usize) -> usize {
        self.heights()[node_id]
    }

    pub fn value(&self, evidence: &Evidence) -> f64 {
        self.value_subspn(evidence, self.root_id)
    }

    pub fn log_value(&self, evidence: &Evidence) -> f64 {
        self.log_value_subspn(evidence, self.root_id)
    }

    pub fn value_subspn(&self, evidence: &Evidence, subspn_root_id: usize) -> f64 {
        self.nodes[subspn_root_id].value(evidence, &self.nodes)
    }

    pub fn log_value_subspn(&self, evidence: &Evidence, subspn_root_id: usize) -> f64 {
        assert![subspn_root_id < self.nodes.len()];
        self.nodes[subspn_root_id].log_value(evidence, &self.nodes)
    }

    pub fn scope(&self) -> &HashSet<usize> {
        &self.scopes[self.root_id]
    }

    pub fn scope_subspn(&self, node_id: usize) -> &HashSet<usize> {
        &self.scopes[node_id]
    }

    pub fn complete_evidence(&self) -> Evidence {
        let mut varset = Evidence::default();
        self.variables
            .iter()
            .for_each(|var| varset.insert_set(var.id, (0..var.n_categories).collect()));
        varset
    }

    pub fn topological_order(&self) -> Vec<usize> {
        let mut order = Vec::default();
        let mut node_queue = VecDeque::default();
        let mut parents: HashMap<usize, usize> = HashMap::default();
        parents.insert(self.root_id, 0);
        node_queue.push_back(self.root_id);
        while !node_queue.is_empty() {
            let node_id = node_queue.pop_front().unwrap();
            match &self.nodes[node_id] {
                Node::Sum(child_ids, _) => {
                    for child_id in child_ids {
                        if parents.contains_key(child_id) {
                            let current_value = parents[child_id];
                            parents.insert(*child_id, current_value + 1);
                        } else {
                            parents.insert(*child_id, 1);
                        }
                        node_queue.push_back(*child_id);
                    }
                }
                Node::Product(child_ids) => {
                    for child_id in child_ids {
                        if parents.contains_key(child_id) {
                            let current_value = parents[child_id];
                            parents.insert(*child_id, current_value + 1);
                        } else {
                            parents.insert(*child_id, 1);
                        }
                        node_queue.push_back(*child_id);
                    }
                }
                _ => {}
            }
        }
        node_queue.push_back(self.root_id);
        while !node_queue.is_empty() {
            let node_id = node_queue.pop_front().unwrap();
            order.push(node_id);
            match &self.nodes[node_id] {
                Node::Sum(child_ids, _) => {
                    for child_id in child_ids {
                        let current_value = parents[child_id];
                        parents.insert(*child_id, current_value - 1);
                        if parents[child_id] == 0 {
                            node_queue.push_back(*child_id);
                        }
                    }
                }
                Node::Product(child_ids) => {
                    for child_id in child_ids {
                        let current_value = parents[child_id];
                        parents.insert(*child_id, current_value - 1);
                        if parents[child_id] == 0 {
                            node_queue.push_back(*child_id);
                        }
                    }
                }
                _ => {}
            }
        }
        order
    }

    pub fn eval(&self, evidence: &Evidence) -> Vec<f64> {
        self.nodes
            .iter()
            .enumerate()
            .map(|(node_id, _)| self.value_subspn(evidence, node_id))
            .collect()
    }

    pub fn log_eval(&self, evidence: &Evidence) -> Vec<f64> {
        self.nodes
            .iter()
            .enumerate()
            .map(|(node_id, _)| self.log_value_subspn(evidence, node_id))
            .collect()
    }

    pub fn derivatives(&self, evidence: &Evidence, eval_cache_opt: Option<Vec<f64>>) -> Vec<f64> {
        let values = match eval_cache_opt {
            Some(eval_cache) => eval_cache,
            None => self.eval(evidence),
        };
        let topological_order = self.topological_order();
        let mut ders = vec![0f64; topological_order.len()];
        ders[self.root_id] = 1f64;
        for node_id in topological_order {
            let node_derivative = ders[node_id];
            match &self.nodes[node_id] {
                Node::Sum(child_ids, weights) => {
                    for (child_id, weight) in child_ids.iter().zip(weights) {
                        ders[*child_id] += node_derivative * weight;
                    }
                }
                Node::Product(child_ids) => {
                    let children_values = child_ids.iter().map(|child_id| values[*child_id]);
                    let zero_counter = children_values
                        .clone()
                        .filter(|value| *value == 0f64)
                        .count();
                    for child_id in child_ids {
                        let child_value = values[*child_id];
                        let other = if zero_counter == 0 {
                            values[node_id] / child_value
                        } else if zero_counter == 1 {
                            if child_value == 0f64 {
                                children_values.clone().fold(1f64, |acc, x| acc * x)
                            } else {
                                0f64
                            }
                        } else {
                            0f64
                        };
                        ders[*child_id] += node_derivative * other;
                    }
                }
                _ => {}
            }
        }
        ders
    }

    pub fn derivative_of_assignment(&self, evidence: &Evidence) -> HashMap<usize, Vec<f64>> {
        let ders = self.derivatives(evidence, None);
        let mut leaf_derivs = HashMap::default();
        for var in &self.variables {
            let vector = vec![0f64; var.n_categories];
            leaf_derivs.insert(var.id, vector);
        }
        for node_id in self.topological_order() {
            if let Node::Indicator(var_id, assign) = &self.nodes[node_id] {
                leaf_derivs.get_mut(var_id).unwrap()[*assign] += ders[node_id];
            }
        }
        leaf_derivs
    }

    pub fn binarize(&mut self) {
        let mut nodes_to_add = Vec::default();
        let mut nodes_to_change: Vec<(usize, Node)> = Vec::default();
        let mut next_node_id = self.nodes.len();
        for (node_id, node) in self.nodes.iter().enumerate() {
            match node {
                Node::Sum(child_ids, weights) => {
                    let n_children = child_ids.len();
                    if n_children > 2 {
                        let new_node = if n_children % 2 == 0 {
                            let half = n_children / 2;
                            let (left_children, right_children) = child_ids.split_at(half);
                            let (left_weights, right_weights) = weights.split_at(half);
                            let sum_left_weights = left_weights.iter().sum();
                            let sum_right_weights = right_weights.iter().sum();
                            let left_node = Node::Sum(
                                left_children.into(),
                                left_weights.iter().map(|x| x / sum_left_weights).collect(),
                            );
                            let right_node = Node::Sum(
                                right_children.into(),
                                right_weights
                                    .iter()
                                    .map(|x| x / sum_right_weights)
                                    .collect(),
                            );
                            nodes_to_add.push(left_node);
                            let left_id = next_node_id;
                            next_node_id += 1;
                            nodes_to_add.push(right_node);
                            let right_id = next_node_id;
                            next_node_id += 1;
                            Node::Sum(
                                vec![left_id, right_id],
                                vec![sum_left_weights, sum_right_weights],
                            )
                        } else {
                            let (last_child, other_children) = child_ids.split_last().unwrap();
                            let (last_child_weight, other_weights) = weights.split_last().unwrap();
                            let sum_other_weights = other_weights.iter().sum();
                            let right_node = Node::Sum(other_children.into(), other_weights.into());
                            nodes_to_add.push(right_node);
                            let other_id = next_node_id;
                            next_node_id += 1;
                            Node::Sum(
                                vec![*last_child, other_id],
                                vec![*last_child_weight, sum_other_weights],
                            )
                        };
                        nodes_to_change.push((node_id, new_node));
                    }
                }
                _ => {
                    continue;
                }
            }
        }
        nodes_to_add
            .into_iter()
            .for_each(|node| self.nodes.push(node));
        nodes_to_change
            .into_iter()
            .for_each(|(node_id, node)| self.nodes[node_id] = node);
    }
}

impl fmt::Display for SPN {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut string = String::default();
        let mut node_queue = Vec::default();
        let mut visited = Vec::default();
        node_queue.push((self.root(), self.root_id));
        while !node_queue.is_empty() {
            let (node, id) = node_queue.pop().unwrap();
            match node {
                Node::Indicator(var_id, assign) => {
                    string.push_str(&format!["[L {:?}: {:?} = {:?}]\n", id, var_id, assign]);
                }
                Node::Product(child_ids) => {
                    string.push_str(&format!["[* {:?} => {:?}]\n", id, child_ids]);
                    child_ids.iter().for_each(|x| {
                        if !visited.contains(x) {
                            node_queue.push((&self.nodes[*x], *x));
                        }
                    });
                }
                Node::Sum(child_ids, weights) => {
                    string.push_str(&format![
                        "[+ {:?} => {:?} x {:?}]\n",
                        id, weights, child_ids
                    ]);
                    child_ids.iter().for_each(|x| {
                        if !visited.contains(x) {
                            node_queue.push((&self.nodes[*x], *x));
                        }
                    });
                }
                Node::Constant(value) => {
                    string.push_str(&format!["[C {:?}: {:?}]\n", id, value]);
                }
            }
            visited.push(id);
        }
        write!(f, "{}", string)
    }
}

impl From<MIS> for SPN {
    fn from(mis: MIS) -> Self {
        let mut nodes = vec![];
        let mut neighbors: HashMap<usize, Vec<usize>> = HashMap::default();
        for i in 0..mis.n_vertices {
            neighbors.insert(i, Vec::default());
            for possible_neighbor in 0..mis.n_vertices {
                if mis.data[i][possible_neighbor] {
                    neighbors.get_mut(&i).unwrap().push(possible_neighbor);
                }
            }
        }

        let mut product_nodes = vec![];
        let mut weights = vec![];
        let mut variables = vec![];
        let mut counts_for_c = vec![0; mis.n_vertices];
        let mut number_of_neighbors = vec![0; mis.n_vertices];

        for i in 0..mis.n_vertices {
            number_of_neighbors[i] = neighbors[&i].len();
            counts_for_c[i] = 2u64.pow((mis.n_vertices - number_of_neighbors[i] - 1) as u32);

            let mut child_ids = vec![];

            for j in 0..mis.n_vertices {
                let prob = if i == j {
                    1f64
                } else if mis.data[i][j] {
                    0f64
                } else {
                    0.5
                };
                let variable = Variable {
                    id: j,
                    n_categories: 2,
                };
                variables.push(variable);
                let indicator1 = Node::Indicator(j, 0);
                let id_indicator1 = nodes.len();
                nodes.push(indicator1);
                let indicator2 = Node::Indicator(j, 1);
                let id_indicator2 = nodes.len();
                nodes.push(indicator2);
                let bernoulli =
                    Node::Sum(vec![id_indicator1, id_indicator2], vec![1f64 - prob, prob]);
                let id_bernoulli = nodes.len();
                nodes.push(bernoulli);
                child_ids.push(id_bernoulli);
            }
            let product_node_id = nodes.len();
            nodes.push(Node::Product(child_ids));
            product_nodes.push(product_node_id);
        }

        let norm_coeff: u64 = counts_for_c.iter().sum();
        for n_neighbors in number_of_neighbors.iter().take(mis.n_vertices) {
            let weight =
                (2u64.pow((mis.n_vertices - n_neighbors - 1) as u32)) as f64 / norm_coeff as f64;
            weights.push(weight);
        }
        let root_id = nodes.len();
        nodes.push(Node::Sum(product_nodes, weights));
        let scopes = fix_scopes(&nodes);

        SPN {
            nodes,
            variables,
            root_id,
            scopes,
        }
    }
}
