use crate::evidence::Evidence;
use std::f64;

#[derive(Debug, Clone, PartialEq)]
pub enum Node {
    Indicator(usize, usize),   // Variable ID, Assignment
    Sum(Vec<usize>, Vec<f64>), // Children, Weights
    Product(Vec<usize>),       // Children
    Constant(f64),             // Constant Nodes are created by some Map Algorithms
}

impl Node {
    pub fn value(&self, evidence: &Evidence, nodes: &[Node]) -> f64 {
        match self {
            Node::Indicator(var_id, assign) => match evidence.get(*var_id) {
                Some(values) => {
                    if values.contains(assign) {
                        1f64
                    } else {
                        0f64
                    }
                }
                None => 0f64,
            },
            Node::Sum(child_ids, weights) => child_ids
                .iter()
                .zip(weights.iter())
                .map(|(child_id, weight)| *weight * nodes[*child_id].value(evidence, nodes))
                .sum(),
            Node::Product(child_ids) => child_ids
                .iter()
                .map(|child_id| nodes[*child_id].value(evidence, nodes))
                .product(),
            Node::Constant(value) => *value,
        }
    }

    pub fn log_value(&self, evidence: &Evidence, nodes: &[Node]) -> f64 {
        match self {
            Node::Indicator(var_id, assign) => match evidence.get(*var_id) {
                Some(values) => {
                    if values.contains(assign) {
                        0f64
                    } else {
                        f64::NEG_INFINITY
                    }
                }
                None => f64::NEG_INFINITY,
            },
            Node::Sum(child_ids, weights) => child_ids
                .iter()
                .zip(weights.iter())
                .map(|(child_id, weight)| {
                    (weight.ln() + nodes[*child_id].log_value(evidence, nodes)).exp()
                })
                .sum::<f64>()
                .ln(),
            Node::Product(child_ids) => child_ids
                .iter()
                .map(|child_id| nodes[*child_id].log_value(evidence, nodes))
                .sum(),
            Node::Constant(value) => value.ln(),
        }
    }
}
