#[derive(Debug, Clone, Default)]
pub struct Dataset {
    pub raw_data: Vec<Vec<usize>>,
}

#[derive(Debug, Clone)]
pub struct RowSlice<'a> {
    dataset: &'a Dataset,
    indexes: Vec<usize>,
}

#[derive(Debug, Clone)]
pub struct ColumnSlice<'a> {
    dataset: &'a Dataset,
    indexes: Vec<usize>,
}

#[derive(Debug, Clone)]
pub struct DataSlice<'a> {
    dataset: &'a Dataset,
    row_indexes: Vec<usize>,
    column_indexes: Vec<usize>,
}

impl<'a> DataSlice<'a> {
    pub fn new(dataset: &'a Dataset, row_indexes: Vec<usize>, column_indexes: Vec<usize>) -> Self {
        DataSlice {
            dataset,
            row_indexes,
            column_indexes,
        }
    }
}

impl Dataset {
    pub fn rows(&self, indexes: Option<&[usize]>) -> impl Iterator<Item = &Vec<usize>> {
        let selected_indexes = match indexes {
            Some(indexes) => indexes.to_vec(),
            None => (0..self.raw_data.len()).map(|x| x).collect(),
        };
        self.raw_data
            .iter()
            .enumerate()
            .filter(move |(index, row)| selected_indexes.contains(index))
            .map(|(index, row)| row)
    }

    pub fn iter(
        &self,
        row_indexes: Vec<usize>,
        column_indexes: Vec<usize>,
    ) -> impl Iterator<Item = Vec<usize>> + '_ {
        self.raw_data
            .iter()
            .enumerate()
            .filter(move |(row_index, row)| row_indexes.contains(row_index))
            .map(move |(row_index, row)| {
                row.iter()
                    .enumerate()
                    .filter(|(column_index, data_point)| column_indexes.contains(column_index))
                    .map(|(_, data_point)| *data_point)
                    .collect()
            })
    }
}

impl<'a> RowSlice<'a> {
    pub fn iter(self) -> impl Iterator<Item = &'a Vec<usize>> {
        self.dataset
            .raw_data
            .iter()
            .enumerate()
            .filter(move |(index, row)| self.indexes.contains(index))
            .map(|(index, row)| row)
    }
}

impl<'a> ColumnSlice<'a> {
    pub fn iter(self) -> impl Iterator<Item = Vec<&'a usize>> {
        let indexes = self.indexes;
        self.dataset.raw_data.iter().map(move |row| {
            row.iter()
                .enumerate()
                .filter(|(index, number)| indexes.contains(index))
                .map(|(index, number)| number)
                .collect()
        })
    }
}
