use crate::variable::Variable;
use rand::seq::SliceRandom;
use std::fs::File;
use std::io::prelude::*;

pub fn load_data(filename: &str) -> std::io::Result<(Vec<Vec<usize>>, Vec<Variable>)> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut variables = vec![];
    let mut data: Vec<Vec<usize>> = vec![];

    for line in contents.split('\n') {
        if line.starts_with("var") {
            let splitted_line: Vec<&str> = line.split(' ').collect();
            let var_id = splitted_line[1].parse::<usize>().unwrap();
            let n_categories = splitted_line[2].parse::<usize>().unwrap();
            variables.push(Variable::new(var_id, n_categories));
        } else if !line.trim().is_empty() {
            data.push(
                line.split(|c| c == ',' || c == ' ')
                    .map(|datapoint_str| datapoint_str.parse::<usize>().unwrap())
                    .collect(),
            );
        }
    }
    variables.sort_by_cached_key(|var| var.id);
    Ok((data, variables))
}

pub fn split_data(
    filename: &str,
    train_filename: &str,
    test_filename: &str,
    train_proportion: f64,
) -> std::io::Result<()> {
    let (data, variables) = load_data(filename)?;

    let data_indexes: Vec<usize> = (0..data.len()).collect();
    let chosen_train_indexes: Vec<usize> = data_indexes
        .choose_multiple(
            &mut rand::thread_rng(),
            ((data.len() as f64) * train_proportion) as usize,
        )
        .cloned()
        .collect();

    let mut training_data = vec![];
    let mut test_data = vec![];

    for index in data_indexes {
        if chosen_train_indexes.contains(&index) {
            training_data.push(data[index].clone());
        } else {
            test_data.push(data[index].clone());
        }
    }

    let mut train_file = File::create(train_filename)?;
    let mut test_file = File::create(test_filename)?;
    for variable in variables {
        let var_string = format!["var {} {}\n", variable.id, variable.n_categories];
        let var_string_as_bytes = var_string.as_bytes();
        train_file.write_all(var_string_as_bytes)?;
        test_file.write_all(var_string_as_bytes)?;
    }
    for data_line in training_data {
        let mut string_data_line = data_line
            .into_iter()
            .map(|number| format!["{}", number])
            .collect::<Vec<String>>()
            .join(",");
        string_data_line.push('\n');
        train_file.write_all(string_data_line.as_bytes())?;
    }
    for data_line in test_data {
        let mut string_data_line = data_line
            .into_iter()
            .map(|number| format!["{}", number])
            .collect::<Vec<String>>()
            .join(",");
        string_data_line.push('\n');
        test_file.write_all(string_data_line.as_bytes())?;
    }

    Ok(())
}
