extern crate crossbeam;
extern crate itertools;
extern crate ordered_float;
extern crate rand;
extern crate rgsl;

pub mod actions;
pub mod classification;
pub mod data;
pub mod evidence;
pub mod independence;
pub mod kmedoid;
pub mod learn;
pub mod map;
pub mod mis;
pub mod spn;
pub mod utils;
pub mod variable;
