use crate::independence::graph::IndependencyGraph;
use crate::kmedoid::kmedoid;
use crate::spn::node::Node;
use crate::spn::SPN;
use crate::variable::Variable;
use rand::Rng;

fn merge_nodes(mut current_nodes: Vec<Node>, new_nodes: Vec<Node>) -> Vec<Node> {
    let next_node_index = current_nodes.len();
    for node in new_nodes {
        match node {
            Node::Indicator(_, _) => {
                current_nodes.push(node);
            }
            Node::Sum(mut child_ids, weights) => {
                child_ids
                    .iter_mut()
                    .for_each(|child_id| *child_id += next_node_index);
                current_nodes.push(Node::Sum(child_ids, weights));
            }
            Node::Product(mut child_ids) => {
                child_ids
                    .iter_mut()
                    .for_each(|child_id| *child_id += next_node_index);
                current_nodes.push(Node::Product(child_ids));
            }
            _ => {}
        }
    }
    current_nodes
}

fn create_multinomial(var: &Variable, data: &[usize]) -> Vec<Node> {
    println!["Creating new leaf..."];
    let mut counts = vec![0; var.n_categories];
    for data_sample in data {
        counts[*data_sample] += 1;
    }
    let mut sum = 0f64;
    let mut probabilities: Vec<f64> = counts
        .iter()
        .map(|c| {
            sum += 1.0 + (*c as f64);
            (1 + *c) as f64
        })
        .collect();

    probabilities.iter_mut().for_each(|p| *p /= sum);

    let mut nodes = vec![];
    let mut weights = vec![];
    for (category, probability) in (0..var.n_categories).zip(probabilities) {
        nodes.push(Node::Indicator(var.id, category));
        weights.push(probability);
    }
    //println!["{:?}", weights];
    //assert![weights.iter().sum::<f64>() - 1f64 <= std::f64::EPSILON];
    nodes.push(Node::Sum((0..nodes.len()).collect(), weights));
    nodes
}

fn fully_factorized_form(
    var_data_pairs: impl Iterator<Item = (Variable, Vec<usize>)>,
) -> Vec<Node> {
    let mut all_nodes = vec![];
    let mut child_ids = vec![];
    for (var, data) in var_data_pairs {
        all_nodes = merge_nodes(all_nodes, create_multinomial(&var, &data));
        child_ids.push(all_nodes.len() - 1);
    }

    all_nodes.push(Node::Product(child_ids));
    all_nodes
}

pub fn gens<R: Rng + ?Sized>(
    scope: &[Variable],
    data: &[Vec<usize>],
    kclusters: usize,
    pval: f64,
    rng: &mut R,
    row_indexes_opt: Option<Vec<usize>>,
    depth: usize,
) -> SPN {
    assert!(!data.is_empty());
    let row_indexes = row_indexes_opt.unwrap_or_else(|| (0..data.len()).collect());
    assert!(kclusters > 0);
    println![
        "Called gens with dataset size {:?} and scope {:?}",
        row_indexes.len(),
        scope.len()
    ];
    let data_iter = row_indexes.iter().map(|index| &data[*index]);
    for data_instance in data_iter.clone() {
        for var in scope.iter() {
            assert![data_instance[var.id] < var.n_categories];
        }
    }

    // Dataset with 1 variable
    if scope.len() == 1 {
        let variable = scope.first().unwrap();
        let relevant_data: Vec<usize> = data_iter
            .clone()
            .map(|data_instance| data_instance[variable.id])
            .collect();
        let nodes = create_multinomial(variable, &relevant_data);
        let root_id = nodes.len() - 1;
        return SPN::new(nodes, scope.to_vec(), root_id);
    }

    // Maximum depth
    if depth >= 30 {
        println!["Maximum depth. Returning fully factorized form."];
        let relevant_data = scope.iter().map(|var| {
            (
                var.clone(),
                data_iter
                    .clone()
                    .map(|data_instance| data_instance[var.id])
                    .collect::<Vec<usize>>(),
            )
        });
        let nodes = fully_factorized_form(relevant_data);
        let root_id = nodes.len() - 1;
        return SPN::new(nodes, scope.to_vec(), root_id);
    }

    println!["Creating vardatas for independence test"];
    let var_datas = scope
        .iter()
        .map(|variable| {
            let mut training_data = vec![];
            for data_line in data_iter.clone() {
                training_data.push(data_line[variable.id]);
            }
            (variable.clone(), training_data)
        })
        .collect();

    println!["Creating new independence graph"];
    let independence_graph = IndependencyGraph::new(var_datas, pval);

    if independence_graph.kset.len() > 1 {
        println!["Found independency. Separating independent sets..."];
        let kset = &independence_graph.kset;

        let children = kset.iter().map(|subset| {
            let the_new_scope: Vec<Variable> = subset
                .iter()
                .map(|set_member| {
                    scope
                        .iter()
                        .find(|variable| variable.id == *set_member)
                        .unwrap()
                        .clone()
                })
                .collect();

            gens(
                &the_new_scope,
                data,
                kclusters,
                pval,
                rng,
                Some(row_indexes.clone()),
                depth + 1,
            )
        });
        let mut all_nodes = vec![];
        let mut all_variables = vec![];
        let mut product_children = vec![];
        for child in children {
            all_nodes = merge_nodes(all_nodes, child.nodes);
            product_children.push(all_nodes.len() - 1);
            all_variables.extend(child.variables);
        }
        let root_id = all_nodes.len();
        let product_node = Node::Product(product_children);
        all_nodes.push(product_node);
        return SPN::new(all_nodes, all_variables, root_id);
    }

    println!["No independence found. Preparing for clustering..."];

    println!["Data: {}", row_indexes.len()];
    if row_indexes.len() < kclusters {
        let relevant_data = scope.iter().map(|var| {
            (
                var.clone(),
                data_iter
                    .clone()
                    .map(|data_instance| data_instance[var.id])
                    .collect::<Vec<usize>>(),
            )
        });
        let nodes = fully_factorized_form(relevant_data);
        let root_id = nodes.len() - 1;
        return SPN::new(nodes, scope.to_vec(), root_id);
    }
    let data_for_clustering: Vec<Vec<usize>> = data_iter
        .clone()
        .map(|data_instance| scope.iter().map(|var| data_instance[var.id]).collect())
        .collect();
    let clusters = kmedoid(kclusters, &data_for_clustering, rng);

    let k = clusters.len();
    if k == 1 {
        let relevant_data = scope.iter().map(|var| {
            (
                var.clone(),
                data_iter
                    .clone()
                    .map(|data_instance| data_instance[var.id])
                    .collect::<Vec<usize>>(),
            )
        });
        let nodes = fully_factorized_form(relevant_data);
        let root_id = nodes.len() - 1;
        return SPN::new(nodes, scope.to_vec(), root_id);
    }

    println!["Reformatting clusters to appropriate format and creating sum node..."];

    let mut all_nodes = vec![];
    let mut sum_children = vec![];
    let mut weights = vec![];

    for cluster in clusters {
        let ni = cluster.all_points.len();
        println!["Cluster has {} elements", ni];
        let data_indexes = cluster
            .all_points
            .iter()
            .map(|cluster_point| row_indexes[*cluster_point])
            .collect();

        println!["Created new sum node child. Recursing..."];
        let child = gens(
            scope,
            data,
            kclusters,
            pval,
            rng,
            Some(data_indexes),
            depth + 1,
        );
        let weight = (ni as f64) / (row_indexes.len() as f64);
        all_nodes = merge_nodes(all_nodes, child.nodes);
        sum_children.push(all_nodes.len() - 1);
        weights.push(weight);
    }
    assert![weights.iter().sum::<f64>() - 1f64 <= std::f64::EPSILON];
    let root_id = all_nodes.len();
    all_nodes.push(Node::Sum(sum_children, weights));
    SPN::new(all_nodes, scope.to_vec(), root_id)
}
