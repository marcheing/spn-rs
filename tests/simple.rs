use spn_rs::evidence::Evidence;
use spn_rs::spn::io::file::from_file;
use spn_rs::spn::node::Node;
use spn_rs::spn::SPN;
use spn_rs::variable::Variable;
use std::collections::HashMap;
use std::path::Path;
mod common;

fn f64_equality_test(x: f64, y: f64) {
    assert!((x - y).abs() < std::f64::EPSILON);
}

#[test]
fn indicator() {
    let var = common::var_example();
    let node = Node::Indicator(var.id, 0);

    let mut varset1 = HashMap::new();
    varset1.insert(var.id, vec![0]);
    let evidence1 = Evidence::new(varset1);

    let mut varset2 = HashMap::new();
    varset2.insert(var.id, vec![1]);
    let evidence2 = Evidence::new(varset2);

    let nodes = vec![node];
    let spn = SPN::new(nodes, vec![var], 0);

    f64_equality_test(spn.value(&evidence1), 1f64);
    f64_equality_test(spn.value(&evidence2), 0f64);
}

#[test]
fn product() {
    let var1 = Variable {
        id: 0,
        n_categories: 2,
    };
    let var2 = Variable {
        id: 1,
        n_categories: 2,
    };

    let ind1 = Node::Indicator(var1.id, 0);
    let ind2 = Node::Indicator(var2.id, 1);

    let prod = Node::Product(vec![0, 1]);

    let mut varset1 = HashMap::new();
    varset1.insert(var1.id, vec![0]);
    varset1.insert(var2.id, vec![1]);
    let evidence1 = Evidence::new(varset1);

    let mut varset2 = HashMap::new();
    varset2.insert(var1.id, vec![0]);
    varset2.insert(var2.id, vec![0]);
    let evidence2 = Evidence::new(varset2);

    let mut varset3 = HashMap::new();
    varset3.insert(var1.id, vec![1]);
    varset3.insert(var2.id, vec![0]);
    let evidence3 = Evidence::new(varset3);

    let mut varset4 = HashMap::new();
    varset4.insert(var1.id, vec![1]);
    varset4.insert(var2.id, vec![1]);
    let evidence4 = Evidence::new(varset4);

    let nodes = vec![ind1, ind2, prod];
    let spn = SPN::new(nodes, vec![var1, var2], 2);

    f64_equality_test(spn.value(&evidence1), 1f64);
    f64_equality_test(spn.value(&evidence2), 0f64);
    f64_equality_test(spn.value(&evidence3), 0f64);
    f64_equality_test(spn.value(&evidence4), 0f64);
}

#[test]
fn sum() {
    let var1 = Variable {
        id: 0,
        n_categories: 2,
    };
    let var2 = Variable {
        id: 1,
        n_categories: 2,
    };

    let ind1 = Node::Indicator(var1.id, 0);
    let ind2 = Node::Indicator(var2.id, 1);

    let sum = Node::Sum(vec![0, 1], vec![0.3, 0.7]);

    let mut varset1 = HashMap::new();
    varset1.insert(var1.id, vec![0]);
    varset1.insert(var2.id, vec![1]);
    let evidence1 = Evidence::new(varset1);

    let mut varset2 = HashMap::new();
    varset2.insert(var1.id, vec![0]);
    varset2.insert(var2.id, vec![0]);
    let evidence2 = Evidence::new(varset2);

    let mut varset3 = HashMap::new();
    varset3.insert(var1.id, vec![1]);
    varset3.insert(var2.id, vec![0]);
    let evidence3 = Evidence::new(varset3);

    let mut varset4 = HashMap::new();
    varset4.insert(var1.id, vec![1]);
    varset4.insert(var2.id, vec![1]);
    let evidence4 = Evidence::new(varset4);

    let nodes = vec![ind1, ind2, sum];
    let spn = SPN::new(nodes, vec![var1, var2], 2);

    f64_equality_test(spn.value(&evidence1), 1f64);
    f64_equality_test(spn.value(&evidence2), 0.3);
    f64_equality_test(spn.value(&evidence3), 0f64);
    f64_equality_test(spn.value(&evidence4), 0.7);
}

#[test]
fn counts() {
    let spn = from_file(Path::new("learned-spns/example.spn")).unwrap();
    let counts = spn.counts();
    let height = spn.height();
    assert_eq![counts["sum"], 5];
    assert_eq![counts["product"], 2];
    assert_eq![counts["indicator"], 8];
    assert_eq![height, 4];
}

#[test]
fn assignments() {
    let spn = common::spn_example();
    for (index, variable) in spn.variables.iter().enumerate() {
        assert_eq![index, variable.id];
    }
    for node in &spn.nodes {
        if let Node::Indicator(var_id, assign) = node {
            let variable = &spn.variables[*var_id];
            assert![*assign < variable.n_categories];
        }
    }
    let complete_evidence = spn.complete_evidence();
    spn.derivative_of_assignment(&complete_evidence);
}
