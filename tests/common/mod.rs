use spn_rs::spn::node::*;
use spn_rs::spn::SPN;
use spn_rs::variable::Variable;

pub fn var_example() -> Variable {
    Variable {
        id: 0,
        n_categories: 2,
    }
}

pub fn spn_example() -> SPN {
    let variables = vec![
        Variable {
            id: 0,
            n_categories: 2,
        },
        Variable {
            id: 1,
            n_categories: 2,
        },
    ];
    let indicator_0_0 = Node::Indicator(0, 0);
    let indicator_0_1 = Node::Indicator(0, 1);
    let indicator_1_0 = Node::Indicator(1, 0);
    let indicator_1_1 = Node::Indicator(1, 1);
    let sum_1 = Node::Sum(vec![0, 1], vec![0.6, 0.4]);
    let sum_2 = Node::Sum(vec![2, 3], vec![0.1, 0.9]);
    let sum_3 = Node::Sum(vec![0, 1], vec![0.2, 0.8]);
    let sum_4 = Node::Sum(vec![2, 3], vec![0.6, 0.4]);
    let prod_1 = Node::Product(vec![4, 5]);
    let prod_2 = Node::Product(vec![6, 7]);
    let root = Node::Sum(vec![8, 9], vec![0.5, 0.5]);
    let nodes = vec![
        indicator_0_0,
        indicator_0_1,
        indicator_1_0,
        indicator_1_1,
        sum_1,
        sum_2,
        sum_3,
        sum_4,
        prod_1,
        prod_2,
        root,
    ];
    let root_id = nodes.len() - 1;
    SPN::new(nodes, variables, root_id)
}
