use spn_rs::map::map_scenario::MapScenario;
use spn_rs::map::max_search::*;
mod common;

#[test]
fn max_search_forward() {
    let spn = common::spn_example();
    let query_vars = spn.variables.clone();
    let map_scenario = MapScenario::new(&spn, query_vars, None, None);
    let evidence_returned = max_search_with_forward_checking(&map_scenario);
    assert_eq![evidence_returned[&0], vec![1]];
    assert_eq![evidence_returned[&1], vec![1]];
}
