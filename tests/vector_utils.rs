use spn_rs::utils::vector::{powerset, remove_item};

#[test]
fn test_powerset() {
    let vec = vec![0, 1, 2];
    let powerset = powerset(&vec);

    assert!(powerset.contains(&vec![]));
    assert!(powerset.contains(&vec![0]));
    assert!(powerset.contains(&vec![1]));
    assert!(powerset.contains(&vec![2]));
    assert!(powerset.contains(&vec![0, 1]));
    assert!(powerset.contains(&vec![0, 2]));
    assert!(powerset.contains(&vec![1, 2]));
    assert!(powerset.contains(&vec));
}

#[test]
fn test_remove_item() {
    let mut vec = vec![10, 20, 30];
    remove_item(&mut vec, 20);
    assert_eq!(vec, vec![10, 30]);
}
