use std::path::Path;

use spn_rs::spn::io::file::from_file;
use spn_rs::spn::node::Node;
use spn_rs::spn::SPN;

#[test]
fn test_binarization_simple() {
    perform_both_tests(from_file(Path::new("learned-spns/example.spn")).unwrap());
}

fn perform_both_tests(mut spn: SPN) {
    let original = spn.clone();
    spn.binarize();
    check_nodes(&spn);
    compare_values(&spn, &original);
}

fn compare_values(spn1: &SPN, spn2: &SPN) {
    for evidence in spn1.complete_evidence().all_subevidences() {
        assert!(spn1.value(&evidence) - spn2.value(&evidence) <= std::f64::EPSILON);
    }
}

fn check_nodes(spn: &SPN) {
    for node in &spn.nodes {
        if let Node::Sum(child_ids, weights) = node {
            assert![child_ids.len() <= 2];
            assert![weights.len() <= 2];
        }
    }
}
