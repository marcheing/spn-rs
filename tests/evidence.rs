use std::collections::HashMap;

use spn_rs::evidence::Evidence;

#[test]
fn test_merge() {
    let mut varset1 = HashMap::new();
    let mut varset2 = HashMap::new();

    varset1.insert(0, vec![0, 1]);
    varset1.insert(1, vec![1]);
    varset2.insert(1, vec![0, 1]);
    varset2.insert(0, vec![2]);

    let ev1 = Evidence::new(varset1);
    let ev2 = Evidence::new(varset2);
    let merged = ev1.merge(&ev2);

    let vec_result_1: Vec<usize> = vec![0, 1, 2];
    let vec_result_2: Vec<usize> = vec![0, 1];

    for value in vec_result_1 {
        assert![merged[&0].contains(&value)];
    }
    for value in vec_result_2 {
        assert![merged[&1].contains(&value)];
    }
}

#[test]
fn test_merge_disjunct() {
    let mut varset1 = HashMap::new();
    let mut varset2 = HashMap::new();

    varset1.insert(0, vec![0, 1]);
    varset2.insert(1, vec![0, 1, 2]);

    let ev1 = Evidence::new(varset1);
    let ev2 = Evidence::new(varset2);
    let merged = ev1.merge(&ev2);

    let vec_result_1: Vec<usize> = vec![0, 1, 2];
    let vec_result_2: Vec<usize> = vec![0, 1];

    assert_eq!(merged[&0], vec_result_2);
    assert_eq!(merged[&1], vec_result_1);
}
#[test]
fn test_univariate_subevidences() {
    let mut varset = HashMap::new();

    varset.insert(0, vec![0, 1]);
    varset.insert(1, vec![0, 1, 2]);

    let ev1 = Evidence::new(varset);
    let subevidences = ev1.univariate_subevidences();

    let mut expected_1 = Evidence::default();
    expected_1.insert(0, vec![0]);
    expected_1.insert(1, vec![0]);

    let mut expected_2 = Evidence::default();
    expected_2.insert(0, vec![1]);
    expected_2.insert(1, vec![0]);

    let mut expected_3 = Evidence::default();
    expected_3.insert(0, vec![0]);
    expected_3.insert(1, vec![1]);

    let mut expected_4 = Evidence::default();
    expected_4.insert(0, vec![1]);
    expected_4.insert(1, vec![1]);

    let mut expected_5 = Evidence::default();
    expected_5.insert(0, vec![0]);
    expected_5.insert(1, vec![2]);

    let mut expected_6 = Evidence::default();
    expected_6.insert(0, vec![1]);
    expected_6.insert(1, vec![2]);

    assert!(subevidences.contains(&expected_1));
    assert!(subevidences.contains(&expected_2));
    assert!(subevidences.contains(&expected_3));
    assert!(subevidences.contains(&expected_4));
    assert!(subevidences.contains(&expected_5));
    assert!(subevidences.contains(&expected_6));
    assert_eq!(subevidences.len(), 6);
}

#[test]
fn test_all_subevidences() {
    let mut varset = HashMap::new();

    varset.insert(0, vec![0, 1]);
    varset.insert(1, vec![0, 1, 2]);

    let ev1 = Evidence::new(varset);
    let subevidences: Vec<Evidence> = ev1.all_subevidences().collect();

    let mut expected_1 = Evidence::default();
    expected_1.insert(0, vec![0]);
    expected_1.insert(1, vec![0]);

    let mut expected_2 = Evidence::default();
    expected_2.insert(0, vec![1]);
    expected_2.insert(1, vec![0]);

    let mut expected_3 = Evidence::default();
    expected_3.insert(0, vec![0]);
    expected_3.insert(1, vec![1]);

    let mut expected_4 = Evidence::default();
    expected_4.insert(0, vec![1]);
    expected_4.insert(1, vec![1]);

    let mut expected_5 = Evidence::default();
    expected_5.insert(0, vec![0]);
    expected_5.insert(1, vec![2]);

    let mut expected_6 = Evidence::default();
    expected_6.insert(0, vec![1]);
    expected_6.insert(1, vec![2]);

    let mut expected_7 = Evidence::default();
    expected_7.insert(0, vec![0]);

    let mut expected_8 = Evidence::default();
    expected_8.insert(0, vec![1]);

    let mut expected_9 = Evidence::default();
    expected_9.insert(0, vec![0, 1]);

    let mut expected_10 = Evidence::default();
    expected_10.insert(1, vec![0]);

    let mut expected_11 = Evidence::default();
    expected_11.insert(1, vec![1]);

    let mut expected_12 = Evidence::default();
    expected_12.insert(1, vec![2]);

    let mut expected_13 = Evidence::default();
    expected_13.insert(1, vec![0, 1]);

    let mut expected_14 = Evidence::default();
    expected_14.insert(1, vec![1, 2]);

    let mut expected_15 = Evidence::default();
    expected_15.insert(1, vec![0, 2]);

    let mut expected_16 = Evidence::default();
    expected_16.insert(1, vec![0, 1, 2]);

    let expected_17 = Evidence::default();

    assert!(subevidences.contains(&expected_1));
    assert!(subevidences.contains(&expected_2));
    assert!(subevidences.contains(&expected_3));
    assert!(subevidences.contains(&expected_4));
    assert!(subevidences.contains(&expected_5));
    assert!(subevidences.contains(&expected_6));
    assert!(subevidences.contains(&expected_7));
    assert!(subevidences.contains(&expected_8));
    assert!(subevidences.contains(&expected_9));
    assert!(subevidences.contains(&expected_10));
    assert!(subevidences.contains(&expected_11));
    assert!(subevidences.contains(&expected_12));
    assert!(subevidences.contains(&expected_13));
    assert!(subevidences.contains(&expected_14));
    assert!(subevidences.contains(&expected_15));
    assert!(subevidences.contains(&expected_16));
    assert!(subevidences.contains(&expected_17));
}
